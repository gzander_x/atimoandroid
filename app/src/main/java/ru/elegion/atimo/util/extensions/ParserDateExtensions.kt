package ru.elegion.atimo.util.extensions

import android.annotation.SuppressLint
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*


@SuppressLint("ConstantLocale")
private val serverFormat = SimpleDateFormat("yyyy-MM-dd",Locale.getDefault())
@SuppressLint("ConstantLocale")
private val uiFormat = SimpleDateFormat("dd.MM.yyyy",Locale.getDefault())
fun String.changedDateToDisplay(): String{
    return try {
        uiFormat.format(serverFormat.parse(this)?:Date())
    }catch (ex: Exception){
        Timber.e(ex.message)
        this
    }
}
fun String.changedDateToServer(): String{
    return serverFormat.format(uiFormat.parse(this)?:Date())
}