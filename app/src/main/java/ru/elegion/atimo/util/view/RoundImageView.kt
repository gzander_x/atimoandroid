package ru.elegion.atimo.util.view

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Canvas
import android.graphics.Path
import android.graphics.RectF
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView
import ru.elegion.atimo.R

class RoundImageView : AppCompatImageView {
    var typedArray: TypedArray? = null
    private var radius = 0.0f
    private var path: Path? = null
    private var rect: RectF? = null


    constructor(context: Context?) : super(context!!) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        typedArray = context.obtainStyledAttributes(attrs, R.styleable.RoundImageView)
        init()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {
        typedArray = context.obtainStyledAttributes(attrs, R.styleable.RoundImageView)
        init()
    }

    private fun init() {
        path = Path()
    }

    override fun onDraw(canvas: Canvas) {
        rect = RectF(0f, 0f, this.width.toFloat(), this.height.toFloat())
        radius = typedArray!!.getDimension(R.styleable.RoundImageView_rect_radius, 10f)
        path!!.addRoundRect(rect!!, radius, radius, Path.Direction.CW)
        canvas.clipPath(path!!)
        super.onDraw(canvas)
    }
}
