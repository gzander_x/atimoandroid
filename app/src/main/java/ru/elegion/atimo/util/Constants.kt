package ru.elegion.atimo.util

object Constants {

    const val SMS_RU_STATUS_CALL_OK = "OK"
    const val SMS_RU_STATUS_CALL_ERROR = "ERROR"

    const val STATUS_NOT_REG = 404

    //region alert bundle
    const val ALERT_TITLE = "alert_title"
    const val ALERT_MSG = "alert_msg"
    const val ALERT_POS_TEXT = "alert_pos_text"
    const val ALERT_BTN_TYPE = "alert_btn_type"
    const val ALERT_ICON = "alert_icon"
    const val ALERT_BTN_TYPE_POS_NEG = 0
    const val ALERT_BTN_TYPE_POS = 1
    const val ALERT_BTN_TYPE_NEG = 2
    const val ALERT_REQUEST_KEY = "key_for_parent_fragment"
    const val ALERT_IMG_SRC = "alert_image_resource"
    const val ALERT_TEXT_COLOR_BUTTON = "alert_text_color_button"
    const val ALERT_COLOR_BUTTON = "ALERT_COLOR_BUTTON"
    const val ALERT_TEXT_COLOR_TITTLE = "alert_text_color_tittle"
    //endregion

    //region bullshit
    const val TRUE_STRING = "True"
    const val FALSE_STRING = "False"
    //endregion

    //region filter passed dialog
    const val BUNDLE_DIALOG_PASSED_TEXT_BUTTONS = "BUNDLE_DIALOG_PASSED_TEXT_BUTTONS"
    const val BUNDLE_DIALOG_PASSED_VALUE = "BUNDLE_DIALOG_PASSED_VALUE"
    const val BUNDLE_DIALOG_PASSED_STATE = "BUNDLE_DIALOG_PASSED_STATE"
    //endregion

    // Ключ, который служит для возвращения во фрагмент, если переход по клику на меню в тулбаре
    const val BUNDLE_MENU_VALUE_KEY_REQUEST = "BUNDLE_MENU_VALUE_KEY_REQUEST"

    //region type open
    const val TYPE_ADD = 0
    const val TYPE_CHANGE = 1
    //endregion

    //region action photo
    const val TYPE_TAKE_PHOTO = 1
    const val TYPE_FROM_GALLERY = 2
    //endregion

    //region shared preferences
    const val SHARED_PREFERENCES = "shared_preferences"
    const val ENCRYPTED_SHARED_PREFERENCES = "encrypted_shared_preferences"

    const val KEY_ACCESS_TOKEN = "access_token"

    const val KEY_NOTIFICATION_SETTINGS = "notification_settings"
    const val filedDelimiter = "?"
    const val entityDelimiter = ";"

    const val KEY_FIRST_NAME = "first_name"
    const val KEY_SECOND_NAME = "second_name"
    const val KEY_LAST_NAME = "last_name"
    const val KEY_PHONE = "phone"
    const val KEY_USER_ROLE = "user_role"
    const val KEY_EMAIL = "email"
    const val KEY_COMPANY_ID = "company_id"
    //endregion

    //region network
    const val HEADER_AUTH_KEY = "Authorization"
    //endregion

    //region internal settings
    const val SEARCH_DELAY_MILLIS = 300L
    const val DEFAULT_PAGING_SIZE = 15
    //endregion

    //region map
    const val KEY_POINT = "point"
    //endregion

    //region user type
    const val USER_TYPE_INVALID = -1
    const val USER_TYPE_ORGANIZATION = 1
    const val USER_TYPE_DRIVER = 2
    //endregion
}
