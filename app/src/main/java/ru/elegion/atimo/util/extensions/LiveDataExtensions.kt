package ru.elegion.atimo.util.extensions

import androidx.lifecycle.MutableLiveData

fun <T> MutableLiveData<T>.update(block: (oldValue: T) -> T) {
    value = value?.let { block(it) }
}