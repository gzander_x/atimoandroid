package ru.elegion.atimo.util

import android.os.Bundle
import androidx.annotation.ColorRes
import androidx.annotation.StringRes

class AlertDialogBundleBuilder private constructor(bundle: Bundle){

    private val dialogBundle = bundle

    fun getBundle() = dialogBundle

    class Builder(){
        private val dialogAlert = AlertDialogBundleBuilder(Bundle())

        fun setTitle(@StringRes title: Int): Builder{
            dialogAlert.dialogBundle.putInt(Constants.ALERT_TITLE, title)
            return this
        }
        fun setTittleColorText(@ColorRes color: Int): Builder{
            dialogAlert.dialogBundle.putInt(Constants.ALERT_TEXT_COLOR_TITTLE, color)
            return this
        }
        fun setMessage(msg: String): Builder{
            dialogAlert.dialogBundle.putString(Constants.ALERT_MSG, msg)
            return this
        }
        fun setBtnColorText(@ColorRes color: Int): Builder{
            dialogAlert.dialogBundle.putInt(Constants.ALERT_TEXT_COLOR_BUTTON, color)
            return this
        }
        fun setRequestKey(): Builder{
            dialogAlert.dialogBundle.putString(Constants.ALERT_REQUEST_KEY, Constants.ALERT_REQUEST_KEY)
            return this
        }
        fun build(): AlertDialogBundleBuilder{
            return dialogAlert
        }
    }
}