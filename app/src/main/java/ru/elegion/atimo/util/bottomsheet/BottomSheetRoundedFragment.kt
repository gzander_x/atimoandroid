package ru.elegion.atimo.util.bottomsheet

import android.os.Bundle
import android.os.Handler
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import ru.elegion.atimo.R
import ru.elegion.atimo.ui.MainActivity
import ru.elegion.atimo.util.BundleBuilder
import ru.elegion.atimo.util.Constants

abstract class BaseBottomSheetRoundedFragment<V : ViewBinding> : BottomSheetDialogFragment() {

    companion object {
        private const val KEY_REQUEST_RETRY = "RetryActionBottomSheet"
    }

    private var actionMessageDialog: (() -> Unit)? = null

    override fun getTheme(): Int = R.style.BaseBottomSheetDialog

    override fun onCreateDialog(savedInstanceState: Bundle?) =
        BottomSheetDialog(requireContext(), theme)

    protected lateinit var binding: V
    abstract fun initBinding(inflater: LayoutInflater): V
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = initBinding(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        initialize()

        initClicks()
    }

    abstract fun initialize()

    protected open fun initClicks() {
    }

    protected fun setAboveKeyboard() {
        dialog?.setOnShowListener {
            Handler().postDelayed({
                val bottomSheet: FrameLayout? =
                    dialog?.findViewById(com.google.android.material.R.id.design_bottom_sheet)
                bottomSheet?.let {
                    val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
                }
            }, 0)
        }
    }

    fun setMaxHeight(height: Double) {
        val dm = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(dm)
        val currentHeight = (dm.heightPixels * height).toInt()
        dialog?.setOnShowListener {
            val bottomSheetDialog = dialog as BottomSheetDialog
            val bottomSheetInternal: View? =
                bottomSheetDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet)
            bottomSheetInternal?.apply {
                val behavior = BottomSheetBehavior.from(this)
                if (currentHeight != 0) {
                    behavior.peekHeight = currentHeight
                    this.layoutParams.height = currentHeight
                }
                behavior.skipCollapsed = false
            }
        }
    }

    fun closeSheet() = this.dismiss()

    fun showLoader(isShow: Boolean) {
        val activity = requireActivity() as? MainActivity
        activity?.let {
            if (isShow) {
                activity.showLoadingDialog()
            } else {
                activity.hideLoadingDialog()
            }
        }
    }

    fun retryDialogMessage(action: () -> Unit) {
        showMessageDialog(
            action,
            BundleBuilder.getAlertBundle(
                getString(R.string.message_dialog_tittle_error),
                getString(R.string.retry_message),
                getString(R.string.retry),
                Constants.ALERT_BTN_TYPE_POS_NEG,
                R.drawable.ic_error,
                KEY_REQUEST_RETRY
            ),
            KEY_REQUEST_RETRY
        )
    }

    /**
     * action:(()->Unit)? - действие которое произойдет если будет нажата кнопка
     * bundle: Bundle - набор параметров для построение диалога.
     *         P/S см. BundleBuilder что нужно передавать для построения MessageDialog
     *         P/S чтобы action заработал необходимо передать в bundle в параметр ALERT_REQUEST_KEY такое же значение что и в key
     * key: String - ключ по которому parentFragmentManager примет результат от MessageDialog
     **/
    fun showMessageDialog(action: (() -> Unit)? = null, bundle: Bundle, key: String? = null) {
        actionMessageDialog = action
        findNavController().navigate(
            R.id.messageDialog, bundle
        )
        if (key != null) {
            setListenerDialog(key)
        }
    }

    /**
     * Принимает сигнал от MessageDialog и выполяется действие, которое мы передали
     **/
    private fun setListenerDialog(key: String) {
        parentFragmentManager.setFragmentResultListener(
            key,
            this
        ) { _, _ ->
            actionMessageDialog?.invoke()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        actionMessageDialog = null
    }
}
