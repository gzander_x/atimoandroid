package ru.elegion.atimo.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.MediaStore
import android.text.TextUtils
import androidx.activity.result.contract.ActivityResultContract
import androidx.core.content.FileProvider
import androidx.core.net.toUri
import ru.elegion.atimo.BuildConfig
import timber.log.Timber
import java.io.File
import java.security.SecureRandom
import java.text.SimpleDateFormat
import java.util.*

class TakePhoto : ActivityResultContract<Unit, Uri?>() {

    var uri: Uri? = null

    override fun createIntent(context: Context, input: Unit): Intent {
        val takePhotoIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val photo = generatePicturePath(context.applicationContext, "")
        uri = photo?.toUri()
        photo?.let {
            val content = FileProvider.getUriForFile(
                context, "${BuildConfig.APPLICATION_ID}.provider", it
            )
            takePhotoIntent.apply {
                putExtra(
                    MediaStore.EXTRA_OUTPUT,
                    content
                )
                addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
                addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            }
        }
        return takePhotoIntent
    }

    override fun parseResult(resultCode: Int, intent: Intent?): Uri? {
        return if (resultCode == Activity.RESULT_OK) intent?.data?:uri else null
    }

    private fun generatePicturePath(applicationContext: Context?, ext: String?): File? {
        try {
            val storageDir: File =
                applicationContext?.getExternalFilesDir(null)!!
            val date = Date()
            date.time =
                System.currentTimeMillis() + SecureRandom().nextInt(
                    1000
                ) + 1
            val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss_SSS", Locale.US).format(date)
            return File(
                storageDir,
                "IMG_" + timeStamp + "." + if (TextUtils.isEmpty(ext)) "jpg" else ext
            )
        } catch (e: Exception) {
            Timber.e("generatePicturePath ${e.message}")
        }
        return null
    }
}