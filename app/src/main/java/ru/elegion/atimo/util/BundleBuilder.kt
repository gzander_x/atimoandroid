package ru.elegion.atimo.util

import android.os.Bundle
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes

class BundleBuilder {

    companion object {
        fun getAlertBundle(
            title: String,
            msg: String,
            posText: String = "",
            btnType: Int = Constants.ALERT_BTN_TYPE_NEG
        ) = Bundle().apply {
            putString(Constants.ALERT_TITLE, title)
            putString(Constants.ALERT_MSG, msg)
            putString(Constants.ALERT_POS_TEXT, posText)
            putInt(Constants.ALERT_BTN_TYPE, btnType)
        }
        fun getAlertBundle(
            title: String,
            msg: String,
            posText: String = "",
            btnType: Int = Constants.ALERT_BTN_TYPE_NEG,
            @DrawableRes srcImage: Int,
            requestKey: String,
            @ColorRes colorBtn: Int = 0,
            @ColorRes textColorBtn: Int = 0
        ) = Bundle().apply {
            putString(Constants.ALERT_TITLE, title)
            putString(Constants.ALERT_MSG, msg)
            putString(Constants.ALERT_POS_TEXT, posText)
            putInt(Constants.ALERT_BTN_TYPE, btnType)
            putInt(Constants.ALERT_IMG_SRC, srcImage)
            putString(Constants.ALERT_REQUEST_KEY, requestKey)
            if (colorBtn > 0 ) putInt(Constants.ALERT_COLOR_BUTTON, colorBtn)
            if (textColorBtn > 0 ) putInt(Constants.ALERT_TEXT_COLOR_BUTTON, textColorBtn)
        }
        fun getAlertBundle(
            title: String,
            msg: String,
            posText: String = "",
            btnType: Int = Constants.ALERT_BTN_TYPE_NEG,
            @DrawableRes srcImage: Int,
        ) = Bundle().apply {
            putString(Constants.ALERT_TITLE, title)
            putString(Constants.ALERT_MSG, msg)
            putString(Constants.ALERT_POS_TEXT, posText)
            putInt(Constants.ALERT_BTN_TYPE, btnType)
            putInt(Constants.ALERT_IMG_SRC, srcImage)
        }
    }
}