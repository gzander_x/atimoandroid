package ru.elegion.atimo.util

import android.os.Bundle
import androidx.annotation.ArrayRes

class FilterPassedDialogBundleBuilder private constructor(bundle: Bundle){

    private val dialogBundle = bundle

    fun getBundle() = dialogBundle

    class Builder(){
        private val dialogBuilder = FilterPassedDialogBundleBuilder(Bundle())

        fun setButtonsText(@ArrayRes text: Int): Builder{
            dialogBuilder.dialogBundle.putInt(Constants.BUNDLE_DIALOG_PASSED_TEXT_BUTTONS, text)
            return this
        }
        fun setState(passed: Boolean?): Builder{
            dialogBuilder.dialogBundle.putSerializable(Constants.BUNDLE_DIALOG_PASSED_STATE, passed)
            return this
        }
        fun build(): FilterPassedDialogBundleBuilder{
            return dialogBuilder
        }
    }
}