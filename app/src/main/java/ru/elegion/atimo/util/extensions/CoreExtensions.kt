package ru.elegion.atimo.util.extensions

import ru.elegion.atimo.util.Constants.FALSE_STRING
import ru.elegion.atimo.util.Constants.TRUE_STRING

fun Boolean?.toSting(): String?{
    return when(this){
        true -> TRUE_STRING
        false -> FALSE_STRING
        else -> null
    }
}