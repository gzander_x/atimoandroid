package ru.elegion.atimo.repository.notifications

import dagger.hilt.components.SingletonComponent
import it.czerwinski.android.hilt.annotations.BoundTo
import ru.elegion.atimo.data.models.notifications.Notification
import ru.elegion.atimo.data.models.notifications.NotificationRes
import ru.elegion.atimo.data.network.AtimoApiService
import ru.elegion.atimo.util.NetworkUtils
import ru.elegion.atimo.util.Resource
import javax.inject.Inject

@BoundTo(supertype = NotificationsRepository::class, component = SingletonComponent::class)
class NotificationsRepositoryImpl @Inject constructor(
    private val api: AtimoApiService,
    private val networkUtils: NetworkUtils
): NotificationsRepository {
    override suspend fun getNotifications(): Resource<NotificationRes> {
        return Resource.Success(getSamples())
        //TODO mot yet implements
//        return networkUtils.getResponse {
//
//        }
    }
    private fun getSamples() = NotificationRes(
        true,
        listOf(
            Notification(
                tittle = "ATIMO",
                message = "С другой стороны реализация намеченных плановых заданий требуют от нас анализа дальнейших направлений развития.",
                date = "11 апреля",
                status = 1
            ),
            Notification(
                status = 0,
                message = "С другой стороны реализация намеченных плановых заданий требуют от нас анализа дальнейших направлений развития.",
                date = "19 января"
            )
        )
    )
}