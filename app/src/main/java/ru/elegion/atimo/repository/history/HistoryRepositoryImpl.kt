package ru.elegion.atimo.repository.history

import dagger.hilt.components.SingletonComponent
import it.czerwinski.android.hilt.annotations.BoundTo
import ru.elegion.atimo.data.models.history.HistoryItem
import ru.elegion.atimo.data.models.history.HistoryRes
import ru.elegion.atimo.data.network.AtimoApiService
import ru.elegion.atimo.util.NetworkUtils
import ru.elegion.atimo.util.Resource
import javax.inject.Inject

@BoundTo(supertype = HistoryRepository::class, component = SingletonComponent::class)
class HistoryRepositoryImpl @Inject constructor(
    private val api: AtimoApiService,
    private val networkUtils: NetworkUtils
    ): HistoryRepository {
    override suspend fun getHistoryInfo(): Resource<HistoryRes> {
        return Resource.Success(getSamples())
//        return networkUtils.getResponse {
//            api.getHistoryInfo()
//        }
    }

    private fun getSamples() = HistoryRes(
        success = true,
        techInspections = HistoryItem(cntAll = 400, cntPassed = 50, cntNotPassed = 350),
        medInspections = HistoryItem(cntAll = 300, cntPassed = 270, cntNotPassed = 30),
        waybills = HistoryItem(cntAll = 220)
    )
}