package ru.elegion.atimo.repository.company

import dagger.hilt.components.SingletonComponent
import it.czerwinski.android.hilt.annotations.BoundTo
import ru.elegion.atimo.data.models.company.*
import ru.elegion.atimo.data.models.drivers.Driver
import ru.elegion.atimo.data.models.vehicles.Vehicle
import ru.elegion.atimo.data.network.AtimoApiService
import ru.elegion.atimo.repository.user.UserRepository
import ru.elegion.atimo.util.NetworkUtils
import ru.elegion.atimo.util.Resource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@BoundTo(supertype =  CompanyRepository::class, component = SingletonComponent::class)
class CompanyRepositoryImpl @Inject constructor(
    private val networkUtils: NetworkUtils,
    private val api: AtimoApiService,
    private val userRepository: UserRepository
    ): CompanyRepository {

    private var companyInfo: CompanyInfoReq? = null

    private var drivers: MutableList<Driver>? = null


    private var cars: List<Vehicle>? = null

    override var isBlockedDrivers: Boolean = false
    override var isBlockedCars: Boolean = false

    override fun getCompany(): CompanyInfoReq? = companyInfo

    override suspend fun updateCompanyInfo(company: CompanyInfoReq): Resource<CompanyCreateModifyRes> {
        val response = networkUtils.getResponse {
            api.createModifyCompany(CompanyReq(company))
        }
        if (response is Resource.Success){
            companyInfo = company.copy()
            val user = userRepository.currentUser.copy()
            user.fromCompany(company)
            userRepository.currentUser = user

        }
        return response

    }

    override fun setCars(cars: List<Vehicle>) {
        isBlockedCars = cars.firstOrNull{ it.vehicleStatus } != null
        this.cars = cars
    }

    override fun serDrivers(drivers: List<Driver>) {
        isBlockedDrivers = drivers.firstOrNull { it.driverStatus == 1 } != null
        this.drivers = drivers.toMutableList()
    }

    override fun getCars(): List<Vehicle> {
        return cars ?: emptyList()
    }

    override fun getDrivers(): List<Driver> {
        return drivers ?: emptyList()
    }

    override suspend fun gerBalance(): CompanyBalance {
        //TODO заменить на API сервера
        return CompanyBalance(0)
    }

    override fun addDriver(driver: Driver) {
        drivers?.add(driver)
    }


}