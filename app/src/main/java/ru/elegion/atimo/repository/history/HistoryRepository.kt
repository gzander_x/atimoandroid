package ru.elegion.atimo.repository.history

import ru.elegion.atimo.data.models.history.HistoryRes
import ru.elegion.atimo.util.Resource

interface HistoryRepository {
    suspend fun getHistoryInfo(): Resource<HistoryRes>
}