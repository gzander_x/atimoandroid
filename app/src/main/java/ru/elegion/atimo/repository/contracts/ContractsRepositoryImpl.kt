package ru.elegion.atimo.repository.contracts

import dagger.hilt.components.SingletonComponent
import it.czerwinski.android.hilt.annotations.BoundTo
import ru.elegion.atimo.data.models.contracts.Contract
import javax.inject.Inject

@BoundTo(supertype =  ContractsRepository::class, component = SingletonComponent::class)
class ContractsRepositoryImpl @Inject constructor() : ContractsRepository {
    override suspend fun getContacts(): List<Contract> {
        return getEmptySamples()
    }

    private fun getEmptySamples() = emptyList<Contract>()

    private fun getSamples() = listOf(
        Contract(
            numDoc = "ПМО №129309104151",
            dateDoc = "26.01.22",
            formatFile = "PDF",
            size = "123 кб"
        )
    )

}