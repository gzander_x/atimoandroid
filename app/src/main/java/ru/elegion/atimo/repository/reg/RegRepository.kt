package ru.elegion.atimo.repository.reg

import ru.elegion.atimo.data.models.company.CompanyCreateModifyRes
import ru.elegion.atimo.data.models.company.CompanyInfoReq
import ru.elegion.atimo.util.Resource

interface RegRepository {
    suspend fun getOrganizations(): List<String>
    suspend fun createModifyCompany(company: CompanyInfoReq): Resource<CompanyCreateModifyRes>
}