package ru.elegion.atimo.repository.company

import ru.elegion.atimo.data.models.company.CompanyBalance
import ru.elegion.atimo.data.models.company.CompanyCreateModifyRes
import ru.elegion.atimo.data.models.company.CompanyInfoReq
import ru.elegion.atimo.data.models.drivers.Driver
import ru.elegion.atimo.data.models.vehicles.Vehicle
import ru.elegion.atimo.util.Resource

interface CompanyRepository {

    var isBlockedDrivers: Boolean
    var isBlockedCars: Boolean

    fun getCompany(): CompanyInfoReq?
    suspend fun updateCompanyInfo(company: CompanyInfoReq): Resource<CompanyCreateModifyRes>
    fun setCars(cars: List<Vehicle>)
    fun serDrivers(drivers: List<Driver>)
    fun getCars(): List<Vehicle>
    fun getDrivers(): List<Driver>
    suspend fun gerBalance(): CompanyBalance
    fun addDriver(driver: Driver)
}
