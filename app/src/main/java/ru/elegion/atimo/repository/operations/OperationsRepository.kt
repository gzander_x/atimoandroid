package ru.elegion.atimo.repository.operations

import ru.elegion.atimo.data.models.operations.Operation

interface OperationsRepository {

    suspend fun getOperation(): List<Operation>

}