package ru.elegion.atimo.repository.test

import dagger.hilt.components.SingletonComponent
import it.czerwinski.android.hilt.annotations.BoundTo
import ru.elegion.atimo.data.models.Test
import javax.inject.Inject

@BoundTo(supertype = TestRepository::class, component = SingletonComponent::class)
class RepositoryImpl @Inject constructor() : TestRepository {
    override suspend fun getTest(): Test = Test("Test Fragment")
}