package ru.elegion.atimo.repository.contracts

import ru.elegion.atimo.data.models.contracts.Contract

interface ContractsRepository {

    suspend fun getContacts(): List<Contract>

}