package ru.elegion.atimo.repository.terminals

import dagger.hilt.components.SingletonComponent
import it.czerwinski.android.hilt.annotations.BoundTo
import ru.elegion.atimo.data.models.points.Point
import ru.elegion.atimo.data.models.points.PointsRes
import ru.elegion.atimo.data.models.waybills.WaybillsRes
import ru.elegion.atimo.data.network.AtimoApiService
import ru.elegion.atimo.repository.user.UserAuthStorage
import ru.elegion.atimo.util.NetworkUtils
import ru.elegion.atimo.util.Resource
import javax.inject.Inject

@BoundTo(supertype = TerminalsRepository::class, component = SingletonComponent::class)
class TerminalsRepositoryImpl @Inject constructor(
    private val networkUtils: NetworkUtils,
    private val api: AtimoApiService,
    private val userAuthStorage: UserAuthStorage
) : TerminalsRepository {

    override suspend fun getTerminals(): Resource<PointsRes> {
        return networkUtils.getResponse(request = {
            api.getTerminals(userAuthStorage.currentUser.phone)
        })
        //  return getSamples()
    }

    override suspend fun getTestTerminals(): List<Point> {
        return getSamples()
    }

    override suspend fun getWaybills(): Resource<WaybillsRes> {
        return networkUtils.getResponse(request = {
            api.getWaybills(
                phone = userAuthStorage.currentUser.phone,
                passed = "true",
                perPage = 5
            )
        })
    }

    private fun getSamples() = listOf(
        Point(
            name = "данные",
            latitude = "55.763242",
            longitude = "37.620003",
            work_time_begin = "10:00",
            work_time_end = "19:00",
            mo = true,
            to = false,
            status = 1
        ),
        Point(
            name = "тест2",
            latitude = "55.751904",
            longitude = "37.632626",
            work_time_begin = "09:00",
            work_time_end = "20:00",
            mo = false,
            to = true,
            status = 2
        ),
        Point(
            name = "тест3",
            latitude = "52.264401",
            longitude = "76.985592",
            work_time_begin = "09:00",
            work_time_end = "20:00",
            mo = false,
            to = true,
            status = 1
        ),
        Point(
            name = "Название пункта",
            latitude = "52.264401",
            longitude = "76.985592",
            work_time_begin = "09:00",
            work_time_end = "20:00",
            mo = true,
            to = true,
            status = 1
        ),
        Point(
            name = "угол",
            latitude = "52.264401",
            longitude = "76.985592",
            work_time_begin = "09:00",
            work_time_end = "20:00",
            mo = true,
            to = false,
            status = 1
        )

    )
}
