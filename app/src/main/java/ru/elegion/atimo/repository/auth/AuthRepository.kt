package ru.elegion.atimo.repository.auth

import ru.elegion.atimo.data.network.model.SmsRuCallRes
import ru.elegion.atimo.data.network.model.SmsRuSmsRes
import ru.elegion.atimo.util.Resource

interface AuthRepository {
    suspend fun sendCall(phoneNumber: String): Resource<SmsRuCallRes>
    suspend fun sendSms(phoneNumber: String, msg: String): Resource<SmsRuSmsRes>
}