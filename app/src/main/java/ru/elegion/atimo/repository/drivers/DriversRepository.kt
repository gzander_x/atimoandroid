package ru.elegion.atimo.repository.drivers

import androidx.lifecycle.LiveData
import androidx.paging.PagingData
import ru.elegion.atimo.data.models.BaseAdapterListModel
import ru.elegion.atimo.data.models.drivers.Driver
import ru.elegion.atimo.data.models.drivers.DriversRes
import ru.elegion.atimo.data.models.vehicles.InspectionParams
import ru.elegion.atimo.data.models.waybills.WaybillsRes
import ru.elegion.atimo.data.network.base.BaseParamsRequest
import ru.elegion.atimo.util.Resource

interface DriversRepository {

    val isBlockedDrivers: Boolean

    suspend fun getDriversNetwork(): Resource<DriversRes>
    fun getDriversCash(): List<Driver>
    fun getDriversCashWithFilter(isBlocked: Boolean?): List<Driver>
    suspend fun createModifyDriver(driver: Driver): Resource<Unit>
    fun addDriver(driver: Driver)
    fun medInspectionsStream(filterProvider: () -> InspectionParams): LiveData<PagingData<BaseAdapterListModel>>
    fun waybillsStream(params: BaseParamsRequest): LiveData<PagingData<BaseAdapterListModel>>
}