package ru.elegion.atimo.repository.operations

import dagger.hilt.components.SingletonComponent
import it.czerwinski.android.hilt.annotations.BoundTo
import ru.elegion.atimo.data.models.operations.Operation
import javax.inject.Inject

@BoundTo(supertype =  OperationsRepository::class, component = SingletonComponent::class)
class OperationsRepositoryImpl @Inject constructor() : OperationsRepository {
    override suspend fun getOperation(): List<Operation> {
        return emptyList()
    }

    private fun getSamples() = listOf(
        Operation(
            numDoc = "12354",
            dateDoc = "26.01.2022 15:21",
            countViews = 100,
            sum = 5020.00
        ),
        Operation(
            numDoc = "7777",
            dateDoc = "26.01.2022 15:21",
            countViews = 77,
            sum = 7777.77
        ),
        Operation(
            numDoc = "99999",
            dateDoc = "26.01.2022 15:21",
            countViews = 99,
            sum = 999.00
        ),
        Operation(
            numDoc = "50897",
            dateDoc = "26.01.2022 15:21",
            countViews = 5,
            sum = 520.52
        ),

    )

}