package ru.elegion.atimo.repository.reg

import dagger.hilt.components.SingletonComponent
import it.czerwinski.android.hilt.annotations.BoundTo
import ru.elegion.atimo.data.models.company.*
import ru.elegion.atimo.data.network.AtimoApiService
import ru.elegion.atimo.repository.user.UserRepository
import ru.elegion.atimo.util.NetworkUtils
import ru.elegion.atimo.util.Resource
import javax.inject.Inject

@BoundTo(supertype = RegRepository::class, component = SingletonComponent::class)
class RegRepositoryImpl @Inject constructor(
    private val networkUtils: NetworkUtils,
    private val api: AtimoApiService,
    private val userRepository: UserRepository
) : RegRepository {

    override suspend fun getOrganizations(): List<String> = listOf("ООО", "ИП", "Самозанятый")

    override suspend fun createModifyCompany(company: CompanyInfoReq): Resource<CompanyCreateModifyRes> {
        var responseCreateCompany = networkUtils.getResponse {
            api.createModifyCompany(CompanyReq(company))
        }
        if (responseCreateCompany is Resource.Success){
            val responseUser = userRepository.getUser(company.phone)
            if (responseUser is Resource.Error){
                responseCreateCompany = Resource.Error(error = responseUser.error)
            }
        }
        return responseCreateCompany
    }
}