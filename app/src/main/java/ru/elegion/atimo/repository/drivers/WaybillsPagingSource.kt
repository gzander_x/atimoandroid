package ru.elegion.atimo.repository.drivers

import androidx.paging.PagingSource
import androidx.paging.PagingState
import retrofit2.HttpException
import ru.elegion.atimo.data.models.BaseAdapterListModel
import ru.elegion.atimo.data.models.waybills.Waybill
import ru.elegion.atimo.data.network.AtimoApiService
import ru.elegion.atimo.data.network.base.BaseParamsRequest
import ru.elegion.atimo.util.extensions.toSting

class WaybillsPagingSource(
    private val api: AtimoApiService,
    private val filters: BaseParamsRequest
) : PagingSource<Int, BaseAdapterListModel>(){
    override fun getRefreshKey(state: PagingState<Int, BaseAdapterListModel>): Int? {
        return state.anchorPosition?.let {
            state.closestPageToPosition(it)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(it)?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, BaseAdapterListModel> {
        return try {

            val pageNumber = params.key ?: 1
            val passed = if(filters.passed != null) !filters.passed!! else null

            val response = api.getWaybills(
                phone = filters.phone,
                passed = passed.toSting(),
                dateBegin = filters.dateBegin,
                dateEnd = filters.dateEnd,
                page = pageNumber,
                perPage = params.loadSize
            )

            if (response.isSuccessful) {
                val data = checkNotNull(response.body()?.waybillsList?.map(Waybill::toAdapterModel))
                val prevKey = (pageNumber - 1).takeIf { pageNumber > 1 }
                val nextKey = (pageNumber + 1).takeIf { data.isNotEmpty() }
                LoadResult.Page(
                    data = data,
                    prevKey = prevKey,
                    nextKey = nextKey
                )
            }else{
                LoadResult.Error(HttpException(response))
            }

        }catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

}