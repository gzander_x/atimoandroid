package ru.elegion.atimo.repository.drivers

import androidx.lifecycle.LiveData
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.liveData
import dagger.hilt.components.SingletonComponent
import it.czerwinski.android.hilt.annotations.BoundTo
import retrofit2.HttpException
import ru.elegion.atimo.data.models.BaseAdapterListModel
import ru.elegion.atimo.data.models.drivers.Driver
import ru.elegion.atimo.data.models.drivers.DriverCreateReq
import ru.elegion.atimo.data.models.drivers.DriverMo
import ru.elegion.atimo.data.models.drivers.DriversRes
import ru.elegion.atimo.data.models.vehicles.InspectionParams
import ru.elegion.atimo.data.models.waybills.WaybillsRes
import ru.elegion.atimo.data.network.AtimoApiService
import ru.elegion.atimo.data.network.base.BaseParamsRequest
import ru.elegion.atimo.repository.company.CompanyRepository
import ru.elegion.atimo.repository.user.UserAuthStorage
import ru.elegion.atimo.ui.adapters.base.PageKeyedPagingSource
import ru.elegion.atimo.util.Constants
import ru.elegion.atimo.util.NetworkUtils
import ru.elegion.atimo.util.Resource
import ru.elegion.atimo.util.extensions.toSting
import javax.inject.Inject

@BoundTo(supertype = DriversRepository::class, component = SingletonComponent::class)
class DriversRepositoryImpl @Inject constructor(
    private val companyRepository: CompanyRepository,
    private val networkUtils: NetworkUtils,
    private val api: AtimoApiService,
    private val userAuthStorage: UserAuthStorage
) : DriversRepository {
    override val isBlockedDrivers: Boolean
        get() = companyRepository.isBlockedDrivers

    override suspend fun getDriversNetwork(): Resource<DriversRes> {
        return networkUtils.getResponse {
            api.getDrivers(userAuthStorage.currentUser.phone)
        }
    }

    override fun getDriversCash(): List<Driver> {
        return companyRepository.getDrivers()
    }

    override fun getDriversCashWithFilter(isBlocked: Boolean?): List<Driver> {
        return if (isBlocked != null) {
            getDriversCash().filter {
                it.driverStatus == if (isBlocked) 0 else 1
            }
        } else {
            getDriversCash()
        }
    }

    override suspend fun createModifyDriver(driver: Driver): Resource<Unit> {
        return networkUtils.getResponse {
            api.createModifyDriver(DriverCreateReq(driver.toCreateModify()))
        }
    }

    override fun addDriver(driver: Driver) {
        companyRepository.addDriver(driver)
    }


    override fun waybillsStream(params: BaseParamsRequest): LiveData<PagingData<BaseAdapterListModel>> {
        return Pager(
            config = PagingConfig(
                pageSize = Constants.DEFAULT_PAGING_SIZE,
                enablePlaceholders = false,
                initialLoadSize = Constants.DEFAULT_PAGING_SIZE * 2
            ),
            pagingSourceFactory = {
                WaybillsPagingSource(api, params)
            }
        ).liveData
    }

    override fun medInspectionsStream(filterProvider: () -> InspectionParams): LiveData<PagingData<BaseAdapterListModel>> {
        return Pager(
            config = PagingConfig(
                pageSize = Constants.DEFAULT_PAGING_SIZE,
                enablePlaceholders = false,
                initialLoadSize = Constants.DEFAULT_PAGING_SIZE * 2
            ),
            pagingSourceFactory = {
                val filter = filterProvider()
                PageKeyedPagingSource { page, perPage ->
                    val response = api.getMedInspections(
                        telephone = filter.telephone,
                        passed = filter.passed.toSting(),
                        dateBegin = filter.dateBegin,
                        dateEnd = filter.dateEnd,
                        page = page,
                        perPage = perPage
                    )

                    if (!response.isSuccessful) throw HttpException(response)

                    checkNotNull(response.body()?.medicalCheckupList?.map(DriverMo::toAdapterModel))
                }
            }
        ).liveData
    }
}