package ru.elegion.atimo.repository.user

import ru.elegion.atimo.data.models.notifications.NotificationType
import ru.elegion.atimo.data.models.setting.PushUpdateRes
import ru.elegion.atimo.data.models.auth.User
import ru.elegion.atimo.data.models.auth.UserRes
import ru.elegion.atimo.util.Resource

interface UserRepository {

    var currentUser: User
    var pushSettings: List<Pair<NotificationType, Boolean>>

    val isUserValid: Boolean

    suspend fun getUser(phoneNumber: String): Resource<UserRes>

    suspend fun updatePushSettings(settings: Map<String, Boolean>): Resource<PushUpdateRes>

    fun logOut()
}