package ru.elegion.atimo.repository.cars

import androidx.lifecycle.LiveData
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.liveData
import dagger.hilt.components.SingletonComponent
import it.czerwinski.android.hilt.annotations.BoundTo
import retrofit2.HttpException
import ru.elegion.atimo.data.models.BaseAdapterListModel
import ru.elegion.atimo.data.models.vehicles.*
import ru.elegion.atimo.data.network.AtimoApiService
import ru.elegion.atimo.repository.company.CompanyRepository
import ru.elegion.atimo.repository.user.UserAuthStorage
import ru.elegion.atimo.ui.adapters.base.PageKeyedPagingSource
import ru.elegion.atimo.util.Constants
import ru.elegion.atimo.util.NetworkUtils
import ru.elegion.atimo.util.Resource
import ru.elegion.atimo.util.extensions.toSting
import timber.log.Timber
import javax.inject.Inject

@BoundTo(supertype = CarsRepository::class, component = SingletonComponent::class)
class CarsRepositoryImpl @Inject constructor(
    private val companyRepository: CompanyRepository,
    private val networkUtils: NetworkUtils,
    private val api: AtimoApiService,
    private val userAuthStorage: UserAuthStorage
) : CarsRepository {
    override val isBlockedCars: Boolean
        get() = companyRepository.isBlockedCars

    override suspend fun getCarsNetwork() = networkUtils.getResponse {
        api.getVehicles(userAuthStorage.currentUser.phone)
    }

    override fun getCarsCash(): List<Vehicle> {
        return companyRepository.getCars()
    }

    override fun getCarsCashWithFilter(isBlocked: Boolean?): List<Vehicle> {
        return if (isBlocked != null) {
            getCarsCash().filter {
                it.vehicleStatus == !isBlocked
            }
        } else {
            getCarsCash()
        }
    }

    override suspend fun createModifyVehicle(car: Vehicle): Resource<Unit> {
        return networkUtils.getResponse {
            api.createModifyVehicle(VehicleUpdateReq(VehicleServer.fromModel(car)))
        }
    }


    override fun techInspectionsStream(filterProvider: () -> InspectionParams): LiveData<PagingData<BaseAdapterListModel>> {
        Timber.d("techInspectionsStream")
        return Pager(
            config = PagingConfig(
                pageSize = Constants.DEFAULT_PAGING_SIZE,
                enablePlaceholders = false,
                initialLoadSize = Constants.DEFAULT_PAGING_SIZE * 2
            ),
            pagingSourceFactory = {
                val filter = filterProvider()
                PageKeyedPagingSource { page, perPage ->
                    val response = api.getTechInspections(
                        telephone = filter.telephone,
                        passed = filter.passed.toSting(),
                        dateBegin = filter.dateBegin,
                        dateEnd = filter.dateEnd,
                        page = page,
                        perPage = perPage,
                        grz = filter.grz?:""
                    )

                    if (!response.isSuccessful) throw HttpException(response)

                    checkNotNull(response.body()?.technicalCheckupList?.map(TechInspectionRes::toAdapterModel))
                }
            }
        ).liveData
    }
}