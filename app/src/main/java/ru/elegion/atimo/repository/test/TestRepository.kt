package ru.elegion.atimo.repository.test

import ru.elegion.atimo.data.models.Test

interface TestRepository {
    suspend fun getTest(): Test
}