package ru.elegion.atimo.repository.notifications

import ru.elegion.atimo.data.models.notifications.Notification
import ru.elegion.atimo.data.models.notifications.NotificationRes
import ru.elegion.atimo.util.Resource

interface NotificationsRepository {
    suspend fun getNotifications(): Resource<NotificationRes>
}