package ru.elegion.atimo.repository.auth

import dagger.hilt.components.SingletonComponent
import it.czerwinski.android.hilt.annotations.BoundTo
import ru.elegion.atimo.R
import ru.elegion.atimo.data.network.AuthApiService
import ru.elegion.atimo.data.network.model.SmsRuCallRes
import ru.elegion.atimo.data.network.model.SmsRuSmsRes
import ru.elegion.atimo.util.NetworkUtils
import ru.elegion.atimo.util.Resource
import timber.log.Timber
import javax.inject.Inject

@BoundTo(supertype =  AuthRepository::class, component = SingletonComponent::class)
class AuthRepositoryImpl @Inject constructor(
    private val authApi: AuthApiService,
    private val networkUtils: NetworkUtils
): AuthRepository {
    override suspend fun sendCall(phoneNumber: String): Resource<SmsRuCallRes> {
        return networkUtils.getResponse(defaultErrorMessage = R.string.send_call_err){
            authApi.sendCall(phoneNumber = phoneNumber)
        }
    }

    override suspend fun sendSms(phoneNumber: String, msg: String): Resource<SmsRuSmsRes> {
        val response = networkUtils.getResponse(defaultErrorMessage = R.string.send_sms_err){
            authApi.sendSms(to = phoneNumber, message = msg)
        }
        return if(response is Resource.Success){
            Timber.d("Пришло от sms.ru = ${response.data}" )
            val smsInfo = response.data?.sms?.let {
                it[phoneNumber]
            }
            val status = smsInfo?.let {
                it.asJsonObject["status"].asString
            }
            if(status == "OK"){
                response
            }else{
                Timber.e("Ошибка sendSms sms.ru = ${smsInfo?.let { it.asJsonObject["status_text"] }}")
                val message = if(smsInfo != null) smsInfo.asJsonObject["status_text"]?.asString else "status_text = null"
                Resource.Error(R.string.send_sms_err, data = SmsRuSmsRes(status = message!!, statusCode = 0, sms = null))
            }
        }else{
            response
        }

    }
}