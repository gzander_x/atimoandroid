package ru.elegion.atimo.repository.cars

import androidx.lifecycle.LiveData
import androidx.paging.PagingData
import ru.elegion.atimo.data.models.BaseAdapterListModel
import ru.elegion.atimo.data.models.vehicles.InspectionParams
import ru.elegion.atimo.data.models.vehicles.Vehicle
import ru.elegion.atimo.data.models.vehicles.VehiclesRes
import ru.elegion.atimo.util.Resource


interface CarsRepository {

    val isBlockedCars: Boolean

    suspend fun getCarsNetwork(): Resource<VehiclesRes>
    fun getCarsCash(): List<Vehicle>
    fun getCarsCashWithFilter(isBlocked: Boolean?): List<Vehicle>
    suspend fun createModifyVehicle(car: Vehicle): Resource<Unit>

    fun techInspectionsStream(filterProvider: () -> InspectionParams): LiveData<PagingData<BaseAdapterListModel>>
}