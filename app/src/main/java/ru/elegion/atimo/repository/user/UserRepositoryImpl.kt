package ru.elegion.atimo.repository.user

import dagger.hilt.components.SingletonComponent
import it.czerwinski.android.hilt.annotations.BoundTo
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.auth.*
import ru.elegion.atimo.data.models.notifications.NotificationType
import ru.elegion.atimo.data.models.setting.PushUpdateReq
import ru.elegion.atimo.data.models.setting.PushUpdateReqBody
import ru.elegion.atimo.data.models.setting.PushUpdateRes
import ru.elegion.atimo.data.network.AtimoApiService
import ru.elegion.atimo.data.network.model.UnsuccessfulResponseException
import ru.elegion.atimo.util.Constants
import ru.elegion.atimo.util.NetworkUtils
import ru.elegion.atimo.util.Resource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@BoundTo(supertype = UserRepository::class, component = SingletonComponent::class)
class UserRepositoryImpl @Inject constructor(
    private val networkUtils: NetworkUtils,
    private val api: AtimoApiService,
    private val userAuthStorage: UserAuthStorage
) : UserRepository {

    override var currentUser: User
        get() = userAuthStorage.currentUser
        set(value) {
            userAuthStorage.currentUser = value
        }

    override var pushSettings: List<Pair<NotificationType, Boolean>>
        get() = userAuthStorage.currentNotificationSettings.mapNotNull { pair ->
            NotificationType.byKey(pair.first)?.let { Pair(it, pair.second) }
        }
        set(value) {
            userAuthStorage.currentNotificationSettings = value.map {
                Pair(it.first.key, it.second)
            }
        }

    override val isUserValid: Boolean
        get() = userAuthStorage.isUserValid

    override suspend fun getUser(phoneNumber: String): Resource<UserRes> {
        var response = networkUtils.getResponse(defaultErrorMessage = R.string.sign_in_error) {
            api.getUser(phoneNumber)
        }

        if (response is Resource.Success) {
            if(response.data!!.user.companyId.isNotBlank()){
                currentUser = response.data!!.user
                pushSettings = if( response.data!!.pushList.isNotEmpty() ) {
                    response.data!!.pushList.mapNotNull { setting ->
                        NotificationType.byKey(setting.name)?.let {
                            Pair(it, setting.status)
                        }
                    }
                }else{
                    listOf(
                        Pair(NotificationType.BALANCE_UPDATE, true),
                        Pair(NotificationType.DRIVER_BLOCKED, true),
                        Pair(NotificationType.WAYBILL_END, true)
                    )
                }
            }else{
                response = Resource.Error(error = UnsuccessfulResponseException(statusCode = Constants.STATUS_NOT_REG, message = "Пользователь не зарегестрирован"))
            }
        }

        return response
    }

    override suspend fun updatePushSettings(settings: Map<String, Boolean>): Resource<PushUpdateRes> {
        return networkUtils.getResponse(defaultErrorMessage = R.string.settings_error) {
            api.createModifyPush(
                PushUpdateReq(
                    phone = currentUser.phone,
                    pushList = listOf(
                        PushUpdateReqBody(
                            waybillEnd = settings.get("WaybillEnd") ?: true,
                            driverBlocked = settings.get("DriverBlocked") ?: true,
                            balanceUpdate = settings.get("BalanceUpdate") ?: true
                        )
                    )
                )
            )
        }
    }

    override fun logOut() = userAuthStorage.clear()
}