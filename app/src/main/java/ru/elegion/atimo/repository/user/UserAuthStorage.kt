package ru.elegion.atimo.repository.user

import android.content.SharedPreferences
import androidx.core.content.edit
import ru.elegion.atimo.data.models.auth.User
import ru.elegion.atimo.util.Constants
import ru.elegion.atimo.util.Constants.USER_TYPE_INVALID
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class UserAuthStorage @Inject constructor(
    @Named(Constants.ENCRYPTED_SHARED_PREFERENCES) private val sharedPrefs: SharedPreferences
) {

    init {
        readUser()
    }

    private var currentCashUser: User? = null

    private var currentCashNotificationSettings: List<Pair<String, Boolean>>? = null

    var currentUser: User
        get() = currentCashUser ?: readUser() //TODO раскоментить после тестов
        set(value) = writeUser(value)

    var currentNotificationSettings: List<Pair<String, Boolean>>
        get() = readNotificationSettings()
        set(value) = writeNotificationSettings(value)

    val isUserValid: Boolean
        get() = currentUser.userRole != USER_TYPE_INVALID

    fun clear() = sharedPrefs.edit {
        currentCashUser = null
        remove(Constants.KEY_FIRST_NAME)
        remove(Constants.KEY_SECOND_NAME)
        remove(Constants.KEY_LAST_NAME)
        remove(Constants.KEY_PHONE)
        remove(Constants.KEY_USER_ROLE)
        remove(Constants.KEY_NOTIFICATION_SETTINGS)
    }

    private fun readUser() = sharedPrefs.run {
        val user = User(
            firstName = getString(Constants.KEY_FIRST_NAME, "") ?: "",
            secondName = getString(Constants.KEY_SECOND_NAME, "") ?: "",
            lastName = getString(Constants.KEY_LAST_NAME, "") ?: "",
            phone = getString(Constants.KEY_PHONE, "") ?: "",
            userRole = getInt(Constants.KEY_USER_ROLE, USER_TYPE_INVALID),
            email = getString(Constants.KEY_EMAIL, "") ?: "",
            companyId = getString(Constants.KEY_COMPANY_ID, "") ?: ""
        )
        currentCashUser = user
        user
    }

    private fun writeUser(user: User) = sharedPrefs.edit {
        currentCashUser = user
        putString(Constants.KEY_FIRST_NAME, user.firstName)
        putString(Constants.KEY_SECOND_NAME, user.secondName)
        putString(Constants.KEY_LAST_NAME, user.lastName)
        putString(Constants.KEY_COMPANY_ID, user.companyId)
        putString(
            Constants.KEY_PHONE,
            user.phone.takeIf(String::isNotEmpty)?.replace("+","") ?: ""
        )
        putInt(Constants.KEY_USER_ROLE, user.userRole)
        putString(
            Constants.KEY_EMAIL,
            user.email ?: ""
        )
    }

    private fun readNotificationSettings() = sharedPrefs.run {
        val notificationsString = getString(Constants.KEY_NOTIFICATION_SETTINGS, "") ?: ""
        notificationsString.split(Constants.entityDelimiter).map { setting ->
            setting.split(Constants.filedDelimiter).let { Pair(it[0], it[1].toBoolean()) } // unsafe
        }
    }

    private fun writeNotificationSettings(
        settings: List<Pair<String, Boolean>>
    ) = sharedPrefs.edit {
        val settingsStr = settings.map { setting ->
            "${setting.first}${Constants.filedDelimiter}${setting.second}"
        }.joinToString(Constants.entityDelimiter)
        putString(Constants.KEY_NOTIFICATION_SETTINGS, settingsStr)
    }
}