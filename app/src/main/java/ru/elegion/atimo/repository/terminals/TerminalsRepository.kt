package ru.elegion.atimo.repository.terminals

import ru.elegion.atimo.data.models.points.Point
import ru.elegion.atimo.data.models.points.PointsRes
import ru.elegion.atimo.data.models.waybills.WaybillsRes
import ru.elegion.atimo.util.Resource

interface TerminalsRepository {
    suspend fun getTerminals(): Resource<PointsRes>

    suspend fun getTestTerminals(): List<Point>

    suspend fun getWaybills(): Resource<WaybillsRes>
}
