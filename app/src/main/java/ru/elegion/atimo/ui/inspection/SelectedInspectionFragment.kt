package ru.elegion.atimo.ui.inspection

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.vehicles.TechInspectionRes
import ru.elegion.atimo.databinding.FragmentInspectionBinding
import ru.elegion.atimo.ui.adapters.base.BaseListsAdapterWithPaging
import ru.elegion.atimo.ui.base.BaseFragment

@AndroidEntryPoint
class SelectedInspectionFragment : BaseFragment<FragmentInspectionBinding>() {

    private val viewModel: InspectionViewModel by viewModels()
    private val adapter = BaseListsAdapterWithPaging()
        .apply {
            listener = {
                (it?.data as? TechInspectionRes)?.let {
                    navigate(R.id.inspectionDialog, InspectionDialog.prepareBundle(it))
                }
            }
            addLoadStateListener { state ->
                showLoader(state.refresh == LoadState.Loading)

                if (state.refresh is LoadState.Error) {
                    retryDialogMessage(::refresh)
                }
            }
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        setupObservers()
    }

    override fun initBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentInspectionBinding = FragmentInspectionBinding.inflate(layoutInflater)

    private fun initViews() = binding.run {
        viewSelectDate.isVisible = false
        textCounter.isVisible = false
        textCarInfo.text = arguments?.getString(KEY_CAR_NAME)

        listInspection.adapter = adapter
        listInspection.layoutManager = LinearLayoutManager(requireContext())
    }

    private fun setupObservers() = viewModel.run {
        techInspections.observe(viewLifecycleOwner) {
            lifecycleScope.launch { adapter.submitData(it) }
        }
        filterUpdated.observe(viewLifecycleOwner) {
            adapter.refresh()
        }
    }

    companion object {

        private const val KEY_CAR_NAME = "carName"
        const val KEY_CAR_GRZ = "carGrz"

        fun prepareBundle(carDisplayedName: String, carGrz: String) = bundleOf(
            KEY_CAR_NAME to carDisplayedName,
            KEY_CAR_GRZ to carGrz
        )
    }
}