package ru.elegion.atimo.ui.tariffs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.rates.RateUI
import ru.elegion.atimo.databinding.FragmentPromoBinding
import ru.elegion.atimo.ui.base.BaseFragment
import ru.elegion.atimo.util.BundleBuilder
import ru.elegion.atimo.util.Constants

class PromoFragment : BaseFragment<FragmentPromoBinding>() {

    companion object{
        private const val BUNDLE_KEY_REQUEST = "BUNDLE_KEY_REQUEST"
        private const val BUNDLE_PROMO = "BUNDLE_PROMO"
        private const val BUNDLE_RETURN = "BUNDLE_RETURN"
        fun createBundle(keyRequest: String, promo: RateUI, keyReturn: String): Bundle{
            return bundleOf(BUNDLE_KEY_REQUEST to keyRequest, BUNDLE_PROMO to promo, BUNDLE_RETURN to keyReturn)
        }
    }

    override fun initBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentPromoBinding = FragmentPromoBinding.inflate(inflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val keyResult = arguments?.getString(BUNDLE_KEY_REQUEST)
        val promo = arguments?.getParcelable<RateUI>(BUNDLE_PROMO)
        val keyReturn = arguments?.getString(BUNDLE_RETURN)
        binding.run {
            buttonActivate.setOnClickListener {
                if(editPromo.text.toString() == promo?.code && keyResult != null && keyReturn != null){
                    parentFragmentManager.setFragmentResult(
                        keyResult,
                        bundleOf(keyReturn to promo)
                    )
                    showMessageDialog(
                        bundle = BundleBuilder.getAlertBundle(
                            title = getString(R.string.promo_tittle_success),
                            msg = getString(R.string.promo_message_success),
                            btnType = Constants.ALERT_BTN_TYPE_POS,
                            srcImage = R.drawable.ic_checkmark,
                            posText = getString(R.string.close),
                            requestKey = Constants.ALERT_REQUEST_KEY
                        ),
                        key = Constants.ALERT_REQUEST_KEY
                    ) {
                        findNavController().popBackStack()
                    }
                }else {
                    showMessageDialog(
                        bundle = BundleBuilder.getAlertBundle(
                            title = getString(R.string.promo_tittle_not_find),
                            msg = getString(R.string.promo_message_not_find),
                            btnType = Constants.ALERT_BTN_TYPE_NEG,
                            srcImage = R.drawable.ic_error
                        )
                    )
                }
            }
        }
    }

}