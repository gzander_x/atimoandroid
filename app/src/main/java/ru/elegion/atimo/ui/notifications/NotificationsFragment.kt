package ru.elegion.atimo.ui.notifications

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.databinding.FragmentNotificationsBinding
import ru.elegion.atimo.ui.adapters.notifications.NotificationsAdapter
import ru.elegion.atimo.ui.base.BaseFragment
import ru.elegion.atimo.util.Resource

@AndroidEntryPoint
class NotificationsFragment : BaseFragment<FragmentNotificationsBinding>() {

    private lateinit var adapter: NotificationsAdapter
    private val viewModel: NotificationsViewModel by viewModels()

    override fun initBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentNotificationsBinding = FragmentNotificationsBinding.inflate(inflater)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecycler()
        setObservers()
    }

    private fun setObservers() {
        with(viewModel){
            liveDataNotifications.observe(viewLifecycleOwner){
                when(it){
                    is Resource.Loading -> showLoader(true)
                    is Resource.Error -> {
                        showLoader(false)
                        retryDialogMessage {
                            getNotifications()
                        }
                    }
                    is Resource.Success -> {
                        showLoader(false)
                        adapter.setData(it.data?.notifications?: emptyList())
                    }
                 }
            }
        }
    }

    private fun initRecycler() {
        adapter = NotificationsAdapter().apply {
            listener = {
                viewModel.setNotification(it)
                //TODO send to server when notification read
                navigate(R.id.notificationFragment, NotificationFragment.prepareBundle(it))
            }
        }
        binding.run{
            recyclerNotifications.adapter = adapter
            recyclerNotifications.layoutManager = LinearLayoutManager(requireContext())
        }
    }


}