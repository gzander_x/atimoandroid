package ru.elegion.atimo.ui.history

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.elegion.atimo.data.models.history.HistoryRes
import ru.elegion.atimo.repository.history.HistoryRepository
import ru.elegion.atimo.util.Resource
import javax.inject.Inject

@HiltViewModel
class HistoryViewModel @Inject constructor(
    private val historyRepository: HistoryRepository
) : ViewModel() {

    private val _liveDataHistory: MutableLiveData<Resource<HistoryRes>> = MutableLiveData()
    val liveDataHistory: LiveData<Resource<HistoryRes>> = _liveDataHistory

    init{
        getHistoryData()
    }

    fun getHistoryData() {

        viewModelScope.launch {
            _liveDataHistory.postValue(Resource.Loading())
            delay(2000)
            _liveDataHistory.postValue(historyRepository.getHistoryInfo())
        }
    }

}