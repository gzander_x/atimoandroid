package ru.elegion.atimo.ui.dialogs

import android.graphics.PorterDuff
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.databinding.DialogResultBinding
import ru.elegion.atimo.ui.base.BaseDialogFragment
import ru.elegion.atimo.util.Constants

@AndroidEntryPoint
class MessageDialog : BaseDialogFragment<DialogResultBinding>() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setListeners()
        // setupObservers()

        setData()
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?): DialogResultBinding =
        DialogResultBinding.inflate(layoutInflater)

    private fun setListeners() {
        binding.btnAlertNeg.setOnClickListener {
            dismiss()
        }
        binding.btnAlertPos.setOnClickListener {
            val requestKey = arguments?.getString(Constants.ALERT_REQUEST_KEY)
            requestKey?.let { key ->
                parentFragmentManager.setFragmentResult(
                    key,
                    Bundle()
                )
            }
            findNavController().popBackStack()
        }
    }

    private fun setupObservers() {
    }

    private fun setData() {
        binding.run {
            textAlertTitle.text = arguments?.getString(Constants.ALERT_TITLE)
            textAlertMsg.text = arguments?.getString(Constants.ALERT_MSG)
            btnAlertPos.text = arguments?.getString(Constants.ALERT_POS_TEXT)

            when (arguments?.getInt(Constants.ALERT_BTN_TYPE)) {
                Constants.ALERT_BTN_TYPE_POS_NEG -> {
                    btnAlertPos.visibility = View.VISIBLE
                    btnAlertNeg.visibility = View.VISIBLE
                }

                Constants.ALERT_BTN_TYPE_POS -> {
                    btnAlertPos.visibility = View.VISIBLE
                    btnAlertNeg.visibility = View.GONE
                }

                Constants.ALERT_BTN_TYPE_NEG -> {
                    btnAlertPos.visibility = View.GONE
                    btnAlertNeg.visibility = View.VISIBLE
                }
            }
            arguments?.getInt(Constants.ALERT_IMG_SRC)?.let {
                if (it > 0)
                    imageAlert.setImageDrawable(ContextCompat.getDrawable(requireContext(), it))
            }
            arguments?.getInt(Constants.ALERT_COLOR_BUTTON)?.let {
                if (it > 0)
                    btnAlertPos.background.setTint(ContextCompat.getColor(requireContext(), it))
            }
            arguments?.getInt(Constants.ALERT_TEXT_COLOR_BUTTON)?.let {
                if (it > 0)
                    btnAlertPos.setTextColor(ContextCompat.getColor(requireContext(), it))
            }
        }
    }
}
