package ru.elegion.atimo.ui.mo

import android.view.LayoutInflater
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.drivers.DriverMo
import ru.elegion.atimo.databinding.DialogInspectionBinding
import ru.elegion.atimo.util.bottomsheet.BaseBottomSheetRoundedFragment
import ru.elegion.atimo.util.extensions.changedDateToDisplay

@AndroidEntryPoint
class MedInspectionDialog : BaseBottomSheetRoundedFragment<DialogInspectionBinding>() {

    override fun initBinding(inflater: LayoutInflater) = DialogInspectionBinding.inflate(inflater)

    override fun initialize() {
        val mo = arguments?.getParcelable(KEY_MO) as? DriverMo
        mo?.let(::renderScreenData)

        setMaxHeight(1.0)
    }

    override fun initClicks() = binding.run {
        buttonClose.setOnClickListener { dismiss() }
    }

    private fun renderScreenData(data: DriverMo) = binding.run {
        textTitle.setText(R.string.inspection__mo_title)
        textDate.text = data.date?.changedDateToDisplay()
        textStatus.text = data.status
        textOrganization.text = data.medicCompanyName
        textInCharge.text = data.medicFio
        renderReason()
        textCertificate.text = getString(R.string.inspection__certificate_hash, data.medicHash)
        textOwner.text = getString(R.string.inspection__certificate_owner, data.medicFio)
        //TODO not yet implement (скоро должны прийти с сервера)
        textValidity.text = getString(R.string.inspection__certificate_valid_date, "с 06.08.2021 по 06.08.2022")
    }

    private fun renderReason() = binding.run {
        textReason.isVisible = false
        textLabelReason.isVisible = false
        barrierReason.isVisible = false
        dividerReason.isVisible = false
    }

    companion object {

        private const val KEY_MO = "mo"

        fun prepareBundle(mo: DriverMo) = bundleOf(
            KEY_MO to mo
        )
    }
}