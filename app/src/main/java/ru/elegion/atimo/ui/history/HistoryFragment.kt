package ru.elegion.atimo.ui.history

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.history.HistoryRes
import ru.elegion.atimo.databinding.FragmentHistoryBinding
import ru.elegion.atimo.databinding.ItemHistoryBinding
import ru.elegion.atimo.ui.base.BaseFragment
import ru.elegion.atimo.util.Resource

@AndroidEntryPoint
class HistoryFragment : BaseFragment<FragmentHistoryBinding>(){

    private val viewModel: HistoryViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setOnClickListeners()
        setupObservers()
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentHistoryBinding =
        FragmentHistoryBinding.inflate(layoutInflater)

    private fun setOnClickListeners(){
        binding.run{
            viewMedical.root.setOnClickListener {
                navigate(R.id.allMedInspectionFragment)
            }
            viewTechnical.root.setOnClickListener {
                navigate(R.id.allInspectionFragment)
            }
            viewWaybills.root.setOnClickListener {
                navigate(R.id.waybillsAllFragment)
            }
        }
    }

    private fun setupObservers(){
        viewModel.liveDataHistory.observe(viewLifecycleOwner){
            when(it){
                is Resource.Loading -> showLoader(true)
                is Resource.Error -> {
                    showLoader(false)
                    retryDialogMessage { viewModel.getHistoryData() }
                }
                is Resource.Success -> {
                    showLoader(false)
                    it.data?.let { data ->
                        init(data)
                    }
                }
            }

        }
    }

    private fun init(data: HistoryRes) {
        binding.run{
            root.isVisible = true
            initItem(
                viewMedical, getString(R.string.drivers__actions_medical),
                showProgress = data.medInspections.cntAll?:0 > 0,
                count = data.medInspections.cntAll ?: 0,
                countCurrent =  data.medInspections.cntPassed ?: 0,
                countLeft = data.medInspections.cntNotPassed ?: 0
            )
            initItem(
                viewTechnical, getString(R.string.personal_account__dialog__technical),
                showProgress = data.techInspections.cntAll?:0 > 0,
                count = data.techInspections.cntAll ?: 0,
                countCurrent =  data.techInspections.cntPassed ?: 0,
                countLeft = data.techInspections.cntNotPassed ?: 0
            )

            initItem(
                viewWaybills, getString(R.string.drivers__actions_waybills),
                showProgress = false,
                count = data.waybills.cntAll ?: 0
            )
        }
    }

    private fun initItem(
        itemBinding: ItemHistoryBinding,
        title: String,
        showProgress: Boolean = true,
        count: Int = 0,
        countCurrent: Int = 0,
        countLeft: Int = 0
    ){
        itemBinding.run{
            historyTitle.text = title

            if(showProgress){
                historyCountCurrent.text = getString(R.string.history_count_current, countCurrent)
                historyCountLeft.text = getString(R.string.history_count_left, countLeft)
                historyProgress.max = count
                historyProgress.progress = countCurrent
            }else{
                historyProgress.visibility = View.GONE
                historyDataView.visibility = View.GONE
            }

            historyCount.text = getString(R.string.history_count, count)
        }
    }
}