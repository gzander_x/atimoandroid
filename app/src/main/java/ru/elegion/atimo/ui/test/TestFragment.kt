package ru.elegion.atimo.ui.test

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.data.models.Test
import ru.elegion.atimo.databinding.FragmentTestBinding
import ru.elegion.atimo.ui.base.BaseFragment
import ru.elegion.atimo.util.Resource
import timber.log.Timber

@AndroidEntryPoint
class TestFragment : BaseFragment<FragmentTestBinding>(){

    private val viewModel: TestViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //setOnClickListeners()
        setupObservers()

        viewModel.getTestInfo()
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentTestBinding =
        FragmentTestBinding.inflate(layoutInflater)

    private fun setOnClickListeners(){
    }

    private fun setupObservers(){
        viewModel.response.observe(viewLifecycleOwner){
            when(it){
                is Resource.Success -> test(it.data)
                is Resource.Error -> Toast.makeText(requireContext(), it.messages!!, Toast.LENGTH_SHORT).show()
                is Resource.Loading -> Unit /* show progressbar */
            }
        }
    }

    private fun test(data: Test?) {
        Timber.d("$data")
        binding.testTV.text = data?.title
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }
}