package ru.elegion.atimo.ui.cars

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import ru.elegion.atimo.data.models.vehicles.Vehicle
import ru.elegion.atimo.databinding.ItemCarBinding
import ru.elegion.atimo.ui.adapters.base.BaseAdapter

class ListCarsAdapter(private val callback: Callback): BaseAdapter<Vehicle, ItemCarBinding>() {

    override fun getBinding(
        inflater: LayoutInflater,
        parent: ViewGroup,
        viewType: Int
    ): ItemCarBinding {
        return ItemCarBinding.inflate(inflater)
    }

    override fun bindViewHolder(holder: ViewBindingHolder, data: Vehicle) {
        holder.binding.tvCarName.text = data.model
        holder.binding.tvCarNumber.text = data.vehicleGrz
        holder.binding.imgLock.isVisible = data.vehicleStatus
        holder.binding.imgMenu.setOnClickListener {
            callback.onClickMenu(data, holder.adapterPosition)
        }
    }

    interface Callback{
        fun onClickMenu(car: Vehicle, position: Int)
    }
}