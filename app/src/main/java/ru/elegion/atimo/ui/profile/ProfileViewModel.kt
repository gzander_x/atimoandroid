package ru.elegion.atimo.ui.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.elegion.atimo.data.models.company.CompanyCreateModifyRes
import ru.elegion.atimo.data.models.company.CompanyInfoReq
import ru.elegion.atimo.repository.company.CompanyRepository
import ru.elegion.atimo.repository.drivers.DriversRepository
import ru.elegion.atimo.repository.user.UserRepository
import ru.elegion.atimo.util.EventLiveData
import ru.elegion.atimo.util.Resource
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val userRepository: UserRepository,
    private val companyRepository: CompanyRepository,
    private val driversRepository: DriversRepository,
) : ViewModel() {

    private var companyInfoBackUp: CompanyInfoReq? = null

    val navigateToSignIn = EventLiveData<Unit>()
    val isSuccessQuit = EventLiveData<Resource<Unit>>()
    private val _successUpdateCompanyLiveData = MutableLiveData<Resource<CompanyCreateModifyRes>>()
    val successUpdateCompanyLiveData: LiveData<Resource<CompanyCreateModifyRes>> = _successUpdateCompanyLiveData

    fun rollbackCompanyInfo(){
        companyInfoBackUp = null
    }

    fun updateCompanyInfo(company: CompanyInfoReq){
        if(companyInfoBackUp != company){
            Timber.d("Обновляем!!!")
            companyInfoBackUp = company.copy()
            viewModelScope.launch {
                _successUpdateCompanyLiveData.postValue(Resource.Loading())
                _successUpdateCompanyLiveData.postValue(companyRepository.updateCompanyInfo(company))
            }
        }else{
            Timber.d("Не обновляем")
        }
    }

    fun quit(){
        viewModelScope.launch {
            isSuccessQuit.postValue(Resource.Loading())
            isSuccessQuit.postValue(driversRepository.createModifyDriver(userRepository.currentUser.toDriver()))
        }
    }

    fun logOut() {
        userRepository.logOut()
        navigateToSignIn.setValue(Unit)
    }
}