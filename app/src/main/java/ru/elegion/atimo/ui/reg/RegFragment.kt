package ru.elegion.atimo.ui.reg

import android.R.attr.maxLength
import android.os.Bundle
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import androidx.navigation.NavOptions
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.company.CompanyInfoReq
import ru.elegion.atimo.databinding.FragmentRegBinding
import ru.elegion.atimo.ui.base.BaseFragment
import ru.elegion.atimo.util.BundleBuilder
import ru.elegion.atimo.util.Constants
import ru.elegion.atimo.util.Resource

@AndroidEntryPoint
class RegFragment : BaseFragment<FragmentRegBinding>(){

    companion object{
        private const val BUNDLE_PHONE = "BUNDLE_PHONE"
        fun createBundle(phone: String): Bundle = bundleOf(BUNDLE_PHONE to phone)
    }

    private val viewModel: RegViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setListeners()
        setupObservers()

        viewModel.getOrganizations()
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentRegBinding =
        FragmentRegBinding.inflate(layoutInflater)

    private fun setListeners(){
        binding.run {
            menuRegOrg.addTextChangedListener {
                val innLength = if (it?.toString() == "ООО") 10 else 12
                val fArray = arrayOf(LengthFilter(innLength))
                binding.editInn.filters = fArray

                binding.regStepTwo.visibility = View.VISIBLE
            }
            editInn.addTextChangedListener {
                checkButtonSaveActive()
            }
            editEmail.addTextChangedListener {
                checkButtonSaveActive()
            }
            editName.addTextChangedListener {
                checkButtonSaveActive()
            }
            editSurname.addTextChangedListener {
                checkButtonSaveActive()
            }
            editLastName.addTextChangedListener {
                checkButtonSaveActive()
            }
            btnReg.setOnClickListener {
                viewModel.regCompany(getCompanyInfo())
            }
        }
    }

    private fun getCompanyInfo() = CompanyInfoReq(
        companyInn = binding.editInn.text.toString(),
        email = binding.editEmail.text.toString(),
        phone = arguments?.getString(BUNDLE_PHONE, "") ?: "",
        firstName = binding.editName.text.toString(),
        secondName = binding.editSurname.text.toString(),
        lastName = binding.editLastName.text.toString()
    )

    private fun setupObservers(){
        viewModel.run {
            responseOrganization.observe(viewLifecycleOwner){
                when(it){
                    is Resource.Success -> setOrganizationMenu(it.data)
                    is Resource.Error -> Toast.makeText(requireContext(), it.messages!!, Toast.LENGTH_SHORT).show()
                    is Resource.Loading -> Unit /* show progressbar */
                }
            }
            liveDataCompanyReg.observe(viewLifecycleOwner){
                showLoader(it is Resource.Loading)
                when(it){
                    is Resource.Loading -> {}
                    is Resource.Success -> {
                        showMessageDialog(
                            bundle = BundleBuilder.getAlertBundle(
                                title = getString(R.string.alert_reg_success_title),
                                msg = getString(R.string.alert_reg_success_msg),
                                btnType = Constants.ALERT_BTN_TYPE_POS,
                                srcImage = R.drawable.ic_checkmark,
                                posText = getString(R.string.alert_reg_success_pos),
                                requestKey = Constants.ALERT_REQUEST_KEY
                            ),
                            key = Constants.ALERT_REQUEST_KEY
                        ) {
                            viewModel.accountRegistered()
                        }
                    }
                    is Resource.Error -> {
                        retryDialogMessage {
                            viewModel.regCompany(getCompanyInfo())
                        }
                    }
                }
            }
            navigateToMap.observe(viewLifecycleOwner) {
                val navOptions = NavOptions.Builder()
                    .setPopUpTo(R.id.nav_graph, true)
                    .build()
                navigate(R.id.mapFragment, navOption = navOptions)
            }
        }
    }

    private fun setOrganizationMenu(organizations: List<String>?){
        val adapter = ArrayAdapter(requireContext(), android.R.layout.simple_dropdown_item_1line, organizations?: emptyList())
        binding.menuRegOrg.setAdapter(adapter)
    }

    private fun checkButtonSaveActive() = binding.run {
        val inn = editInn.text.toString()
        val email = editEmail.text.toString()

        val checkedList = listOf(inn, email)


        btnReg.isEnabled = !checkedList.any(String::isBlank)
    }
}