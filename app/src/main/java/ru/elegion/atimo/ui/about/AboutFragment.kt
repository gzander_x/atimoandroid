package ru.elegion.atimo.ui.about

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.databinding.FragmentAboutBinding
import ru.elegion.atimo.databinding.FragmentHistoryBinding
import ru.elegion.atimo.databinding.FragmentHowWorkBinding
import ru.elegion.atimo.ui.base.BaseFragment

class AboutFragment : BaseFragment<FragmentAboutBinding>(){



    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentAboutBinding =
        FragmentAboutBinding.inflate(layoutInflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val versionName: String = requireContext().packageManager.getPackageInfo(requireActivity().packageName, 0).versionName
        binding.buildNumberText.text = getString(R.string.create_by, versionName)
    }
}