package ru.elegion.atimo.ui.tariffs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.rates.RateUI
import ru.elegion.atimo.databinding.FragmentTariffsBinding
import ru.elegion.atimo.ui.adapters.TariffsAdapter
import ru.elegion.atimo.ui.base.BaseFragment
import ru.elegion.atimo.util.BundleBuilder
import ru.elegion.atimo.util.Constants
import ru.elegion.atimo.util.extensions.toast

class TariffsFragment : BaseFragment<FragmentTariffsBinding>() {

    companion object {
        private const val KEY_REQUEST_FROM_PROMO = "KEY_REQUEST_FROM_PROMO"
        private const val KEY_RETURN = "KEY_RETURN"
    }

    private val viewModel: TariffsViewModel by viewModels()
    private lateinit var tariffsAdapter: TariffsAdapter

    private var isActivatePromo = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setOnClickListeners()
        setupObservers()
        initRecycler()
    }

    override fun initBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentTariffsBinding =
        FragmentTariffsBinding.inflate(layoutInflater)

    private fun setOnClickListeners() {
        with(binding){
            textPersonalPayment.setOnClickListener {
                navigate(R.id.personalPaymentFragment)
            }
            tariffsPay.setOnClickListener {

            }
           tariffsBill.setOnClickListener {
               if(tariffsAdapter.selectedTariff != null) {
                   viewModel.requestBill()
               }else{
                   toast(requireContext(), R.string.not_selected_tariff)
               }
           }
        }
    }

    private fun setupObservers() {
        parentFragmentManager.setFragmentResultListener(
            KEY_REQUEST_FROM_PROMO,
            this
        ){ _, bundle ->
            isActivatePromo = true
            bundle.getParcelable<RateUI>(KEY_RETURN)?.let {
                tariffsAdapter.setSelected(it)
            }
            binding.textTariffDescription.text = getString(R.string.tariff_promo)
        }
        with(viewModel){
            showSuccessDialogRequestBill.observe(viewLifecycleOwner){
                if (it){
                    showMessageDialog(
                        bundle = BundleBuilder.getAlertBundle(
                            title = getString(R.string.request_bill_tittle),
                            msg = "Smirnov_75@mail.ru",
                            btnType = Constants.ALERT_BTN_TYPE_NEG,
                            srcImage = R.drawable.ic_mail,
                        )
                    )
                }else{
                    retryDialogMessage{
                        requestBill()
                    }
                }

            }
        }
    }

    private fun initRecycler() {
        val items = listOf(
            RateUI(10),
            RateUI(20),
            RateUI(100),
            RateUI(200),
            RateUI(500),
            RateUI(isPromo = true, code = "TEST")
        )
        with(binding) {
            tariffsAdapter = TariffsAdapter().apply {
                listener = {
                    tariffsAdapter.setSelected(it)
                    if (it.isPromo && it.code != null)  {
                        if(!isActivatePromo){
                            navigate(
                                R.id.promoFragment,
                                PromoFragment.createBundle(KEY_REQUEST_FROM_PROMO, it, KEY_RETURN)
                            )
                        }else{
                            binding.textTariffDescription.text = getString(R.string.tariff_promo)
                        }
                    } else{
                        binding.textTariffDescription.text = getString(R.string.tariff_discount)
                    }
                }

                setData(items)
            }
            tariffsList.adapter = tariffsAdapter
        }
    }

}