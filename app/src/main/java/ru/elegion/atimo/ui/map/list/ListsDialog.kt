package ru.elegion.atimo.ui.map.list

import android.os.Bundle
import android.view.LayoutInflater
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.points.Point
import ru.elegion.atimo.databinding.ListsDialogBinding
import ru.elegion.atimo.ui.adapters.ListsAdapter
import ru.elegion.atimo.util.Constants.KEY_POINT
import ru.elegion.atimo.util.Resource
import ru.elegion.atimo.util.bottomsheet.BaseBottomSheetRoundedFragment
import timber.log.Timber

@AndroidEntryPoint
class ListsDialog : BaseBottomSheetRoundedFragment<ListsDialogBinding>() {

    private val viewModel: ListsViewModel by viewModels()
    private lateinit var listsAdapter: ListsAdapter
    override fun initBinding(inflater: LayoutInflater) = ListsDialogBinding.inflate(inflater)

    override fun initialize() {
        setMaxHeight(0.8)
        initRecycler()
        setupObservers()
        binding.apply {
            editPhone.addTextChangedListener {
                viewModel.findTerminal(it.toString())
            }
        }
    }

    override fun initClicks() {
        binding.btnClose.setOnClickListener { closeSheet() }
    }

    private fun setupObservers() {
        viewModel.apply {
            liveDataTerminals.observe(viewLifecycleOwner) {
                when (it) {
                    is Resource.Error -> {}
                    is Resource.Loading -> {}
                    is Resource.Success -> {
                        listsAdapter.setData(it.data?.points!!)
                        Timber.e("${it.data}")
                    }
                }
            }
        }
    }

    private fun initRecycler() {
        with(binding) {
            listsAdapter = ListsAdapter().apply {
                listener = {
                    onPointSelected(it)
                    val bundle = Bundle()
                    bundle.putParcelable(KEY_POINT, it)
                    findNavController().popBackStack()
                    findNavController().navigate(R.id.listDetailDialog, bundle)
                }
            }
            rvLists.adapter = listsAdapter
        }
    }

    private fun onPointSelected(point: Point) {
        val lat = point.latitude.toDoubleOrNull() ?: 0.0
        val long = point.longitude.toDoubleOrNull() ?: 0.0
        setFragmentResult(REQUEST_SEARCH_SELECTION, bundleOf(
            KEY_LAT to lat,
            KEY_LONG to long
        ))
    }

    companion object {
        const val REQUEST_SEARCH_SELECTION = "searchSelectRequestKey"

        const val KEY_LAT = "lat"
        const val KEY_LONG = "long"
    }
}
