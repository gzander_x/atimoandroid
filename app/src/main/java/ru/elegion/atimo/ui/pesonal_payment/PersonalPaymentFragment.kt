package ru.elegion.atimo.ui.pesonal_payment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.personal_payment.PersonalPaymentInfo
import ru.elegion.atimo.databinding.FragmentPersonalPaymentBinding
import ru.elegion.atimo.ui.base.BaseFragment
import ru.elegion.atimo.util.BundleBuilder
import ru.elegion.atimo.util.Constants

class PersonalPaymentFragment: BaseFragment<FragmentPersonalPaymentBinding>(){

    private val viewModel: PersonalPaymentViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        setupObservers()
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentPersonalPaymentBinding =
        FragmentPersonalPaymentBinding.inflate(layoutInflater)

    private fun setListeners(){
        with(binding){
            paymentDrivesCount.addTextChangedListener {
                checkButtonSend()
            }
            paymentCarsCount.addTextChangedListener {
                checkButtonSend()
            }
            paymentSend.setOnClickListener {
                viewModel.sendPersonalPayment(getPaymentInfo())
            }
        }
    }

    private fun getPaymentInfo() = PersonalPaymentInfo(
        countDrivers = binding.paymentDrivesCount.text.toString().toInt(),
        countCars = binding.paymentCarsCount.text.toString().toInt(),
        isNeedTechInspection = binding.switchTo.isChecked,
        isBecomeReleasePoint = binding.switchPoint.isChecked
    )

    private fun setupObservers(){
        with(viewModel){
            showDialogApplicationSend.observe(viewLifecycleOwner){
                if(it){
                    showMessageDialog(
                        bundle = BundleBuilder.getAlertBundle(
                            title = getString(R.string.application_tittle),
                            msg = getString(R.string.application_message),
                            btnType = Constants.ALERT_BTN_TYPE_POS,
                            srcImage = R.drawable.ic_checkmark,
                            posText = getString(R.string.close),
                            requestKey = Constants.ALERT_REQUEST_KEY
                        ),
                        key = Constants.ALERT_REQUEST_KEY
                    ) {
                        findNavController().popBackStack()
                    }
                }else{
                    retryDialogMessage {
                        viewModel.sendPersonalPayment(getPaymentInfo())
                    }
                }
            }
        }
    }

    private fun checkButtonSend(){
        with(binding){
            val countDriver =  paymentDrivesCount.text.toString()
            val countCars = paymentCarsCount.text.toString()
            paymentSend.isEnabled = !listOf(countDriver, countCars).any(String::isBlank)
        }

    }
}