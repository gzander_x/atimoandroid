package ru.elegion.atimo.ui.map

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ru.elegion.atimo.data.models.points.PointsRes
import ru.elegion.atimo.data.models.waybills.WaybillsRes
import ru.elegion.atimo.repository.terminals.TerminalsRepository
import ru.elegion.atimo.util.Resource
import javax.inject.Inject

@HiltViewModel
class MapViewModel @Inject constructor(
    private val terminalsRepository: TerminalsRepository
) : ViewModel() {

    private val _liveDataTerminals: MutableLiveData<Resource<PointsRes>> = MutableLiveData()
    val liveDataTerminals: LiveData<Resource<PointsRes>> = _liveDataTerminals

    private val _liveDataWaybills: MutableLiveData<Resource<WaybillsRes>> = MutableLiveData()
    val liveDataWaybills: LiveData<Resource<WaybillsRes>> = _liveDataWaybills

    init {
        getTerminals()
        getWaybills()
    }

    private fun getTerminals() {
        viewModelScope.launch(Dispatchers.IO) {
            val response = terminalsRepository.getTerminals()
            _liveDataTerminals.postValue(response)
        }
    }

    private fun getWaybills() {
        viewModelScope.launch(Dispatchers.IO) {
            val response = terminalsRepository.getWaybills()
            _liveDataWaybills.postValue(response)
        }
    }
}
