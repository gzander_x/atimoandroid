package ru.elegion.atimo.ui.onboarding

import android.view.LayoutInflater
import android.view.ViewGroup
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.onboarding.OnboardingSegment
import ru.elegion.atimo.databinding.*
import ru.elegion.atimo.ui.base.BaseFragment
import ru.elegion.atimo.ui.base.BaseOnboardingFragment

@AndroidEntryPoint
class SixthOnboardingFragment: BaseOnboardingFragment<FragmentOnboarding6Binding>() {
    override val segment: OnboardingSegment = OnboardingSegment.SIXTH
    override val msgFooter: Int
        get() = R.string.onboarding_screen_6_msg
    override val imgFooter: Int
        get() = R.drawable.ic_qr
    override val actionBtnNext: (() -> Unit)
        get() = ({
            navigate(R.id.seventhOnboardingFragment)
        })
    override fun initBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentOnboarding6Binding = FragmentOnboarding6Binding.inflate(inflater)

    override fun initHeader(): ItemOnboardingHeaderBinding = binding.layoutHeader

    override fun initFooter(): ItemOnboardingFooterBinding = binding.layoutFooter
}