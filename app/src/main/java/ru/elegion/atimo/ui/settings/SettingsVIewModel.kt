package ru.elegion.atimo.ui.settings

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.notifications.NotificationType
import ru.elegion.atimo.data.models.setting.PushUpdateRes
import ru.elegion.atimo.repository.user.UserRepository
import ru.elegion.atimo.util.EventLiveData
import ru.elegion.atimo.util.Resource
import javax.inject.Inject

@HiltViewModel
class SettingsViewModel @Inject constructor(
    private val userRepository: UserRepository
) : ViewModel() {

    private var pushUpdateJob: Job? = null

    private val _displayedPushState = MutableLiveData(userRepository.pushSettings.toMap())
    val displayedPushState: LiveData<Map<NotificationType, Boolean>>
        get() = _displayedPushState

    private val _pushUpdateResource = MutableLiveData<Resource<PushUpdateRes>>()
    val pushUpdateResource: LiveData<Resource<PushUpdateRes>>
        get() = _pushUpdateResource

    val messageEvent = EventLiveData<Int>()

    fun updatePushNotificationsSetting(type: NotificationType) {
        if (pushUpdateJob?.isActive == true) return

        val oldValue = _displayedPushState.value ?: return
        val newValue = oldValue.toMutableMap().apply {
            put(type, !(oldValue.get(type) ?: true)) // по умолчанию пуши включены
        }
        _displayedPushState.value = newValue

        pushUpdateJob = viewModelScope.launch {
            _pushUpdateResource.value = Resource.Loading()
            val result = userRepository.updatePushSettings(
                newValue.map { (key, value) -> key.key to value }.toMap()
            ).also(_pushUpdateResource::setValue)
            when {
                result is Resource.Success && result.data?.success == true -> {
                    userRepository.pushSettings = newValue.toList()
                }
                result is Resource.Error || result.data?.success == false -> {
                    _displayedPushState.value = oldValue
                    messageEvent.setValue(result.messages ?: R.string.error_network_default)
                }
                else -> Unit
            }
        }
    }
}