package ru.elegion.atimo.ui.map

import android.Manifest
import android.graphics.PointF
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.vmadalin.easypermissions.EasyPermissions
import com.vmadalin.easypermissions.dialogs.SettingsDialog
import com.yandex.mapkit.Animation
import com.yandex.mapkit.MapKitFactory
import com.yandex.mapkit.geometry.Point
import com.yandex.mapkit.layers.ObjectEvent
import com.yandex.mapkit.map.*
import com.yandex.mapkit.map.Map
import com.yandex.mapkit.user_location.UserLocationLayer
import com.yandex.mapkit.user_location.UserLocationObjectListener
import com.yandex.mapkit.user_location.UserLocationView
import com.yandex.runtime.image.ImageProvider
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.waybills.Waybill
import ru.elegion.atimo.databinding.MapFragmentBinding
import ru.elegion.atimo.ui.MainActivity
import ru.elegion.atimo.ui.base.BaseFragment
import ru.elegion.atimo.ui.map.list.ListsDialog.Companion.KEY_LAT
import ru.elegion.atimo.ui.map.list.ListsDialog.Companion.KEY_LONG
import ru.elegion.atimo.ui.map.list.ListsDialog.Companion.REQUEST_SEARCH_SELECTION
import ru.elegion.atimo.ui.waybill.WaybillDialog
import ru.elegion.atimo.util.Constants.KEY_POINT
import ru.elegion.atimo.util.Resource
import ru.elegion.atimo.util.extensions.toast

@AndroidEntryPoint
class MapFragment :
    BaseFragment<MapFragmentBinding>(),
    EasyPermissions.PermissionCallbacks,
    ClusterListener,
    ClusterTapListener,
    UserLocationObjectListener,
    CameraListener {

    private val viewModel: MapViewModel by viewModels()

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?) =
        MapFragmentBinding.inflate(layoutInflater)

    // todo change map key to real key
    private var userLocationLayer: UserLocationLayer? = null
    private var routeStartLocation = Point(0.0, 0.0)
    private var followUserLocation = false
    private val data = (1..4).toList()

    private val PLACEMARKS_NUMBER = 2000
    private val CLUSTER_CENTERS: Point = Point(55.751244, 37.618423)
    private var listPointTestNewList: List<PointNew>? = null
    private var listPointNewList: List<ru.elegion.atimo.data.models.points.Point>? = null

    private fun createPoints(count: Int): List<PointNew> {
        val points = ArrayList<PointNew>()

        for (i in 0 until count) {
            val clusterCenter = CLUSTER_CENTERS
            val latitude = clusterCenter.latitude + Math.random() - 0.5
            val longitude = clusterCenter.longitude + Math.random() - 0.5
            points.add(
                PointNew(
                    latitude,
                    longitude,
                    data.random()
                )
            )
        }
        listPointTestNewList = points
        return points
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MapKitFactory.initialize(requireContext())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setListeners()
        binding.mapView.map.move(
            CameraPosition(
                CLUSTER_CENTERS, 10F, 0F, 0F
            )
        )
        setupObservers()

        setFragmentResultListener(REQUEST_SEARCH_SELECTION) { _, bundle ->
            val lat = bundle.getDouble(KEY_LAT)
            val long = bundle.getDouble(KEY_LONG)
            val cameraPosition = CameraPosition(Point(lat, long), 10F, 0F, 0F)
            val animation = Animation(Animation.Type.SMOOTH, 1F)
            binding.mapView.map.move(cameraPosition, animation, null)
        }

//        val clusterizedCollection: ClusterizedPlacemarkCollection =
//            binding.mapView.map.mapObjects.addClusterizedPlacemarkCollection(this)
//        val points = createPoints(PLACEMARKS_NUMBER)
//
//        for (point in points) {
//            val marker = clusterizedCollection.addPlacemark(
//                Point(point.latitude, point.longitude),
//                ImageProvider.fromResource(
//                    requireContext(),
//                    when (point.type) {
//                        1 -> {
//                            R.drawable.pin_to_mo
//                        }
//                        2 -> {
//                            R.drawable.pin_empty
//                        }
//                        3 -> {
//                            R.drawable.pin_to
//                        }
//                        else -> {
//                            R.drawable.pin_empty
//                        }
//                    }
//                ),
//                IconStyle(
//                    PointF(0.5f, 0.5f),
//                    RotationType.ROTATE,
//                    1.0f,
//                    true,
//                    true,
//                    1.0f,
//                    null
//                )
//            )
//            marker.userData = point
//            marker.addTapListener(tabObjectTapListener)
//        }
//
//        clusterizedCollection.clusterPlacemarks(80.0, 15)
    }

    private fun setListeners() {
        binding.btnMenu.setOnClickListener {
            (requireActivity() as? MainActivity)?.openDrawerMenu()
        }
    }

    private fun setupObservers() {
        viewModel.apply {

            val clusterizedCollection: ClusterizedPlacemarkCollection =
                binding.mapView.map.mapObjects.addClusterizedPlacemarkCollection(this@MapFragment)

            liveDataTerminals.observe(viewLifecycleOwner) {
                when (it) {
                    is Resource.Loading -> {}
                    is Resource.Error -> {}
                    is Resource.Success -> {
                        it.data?.let { terminals ->
                            for (point in terminals.points) {
                                val marker = clusterizedCollection.addPlacemark(
                                    Point(point.latitude.toDouble(), point.longitude.toDouble()),
                                    ImageProvider.fromResource(
                                        requireContext(),
                                        if (point.mo && !point.to) {
                                            R.drawable.pin_mo
                                        } else if (point.to && !point.mo) {
                                            R.drawable.pin_to
                                        } else if (point.to && point.mo) {
                                            R.drawable.pin_to_mo
                                        } else {
                                            R.drawable.pin_empty
                                        }

                                    ),
                                    IconStyle(
                                        PointF(0.5f, 0.5f),
                                        RotationType.ROTATE,
                                        1.0f,
                                        true,
                                        true,
                                        1.0f,
                                        null
                                    )
                                )
                                marker.userData = point
                                listPointNewList = listOf(point)
                                marker.addTapListener(tabObjectTapListener)
                            }

                            clusterizedCollection.clusterPlacemarks(80.0, 15)
                        }
                    }
                }
            }

            liveDataWaybills.observe(viewLifecycleOwner) {
                when (it) {
                    is Resource.Error -> {}
                    is Resource.Loading -> {}
                    is Resource.Success -> {
                        waybillLogic(it.data?.waybillsList ?: emptyList())
                    }
                }
            }
        }
    }

    private val tabObjectTapListener = MapObjectTapListener { mapObject, selectedPoint ->
        val bundle = Bundle()
        listPointNewList?.forEach { point ->
            if (point == mapObject.userData) {
                bundle.putParcelable(KEY_POINT, point)
            }
        }
        navigate(R.id.listDetailDialog, bundle)
        true
    }

    private fun waybillLogic(data: List<Waybill>) {
        binding.apply {
            when (data.size) {
                0 -> {
                    btnWaybill.icon =
                        ResourcesCompat.getDrawable(resources, R.drawable.ic_waybill_empty, null)
                    btnWaybill.setOnClickListener { findNavController().navigate(R.id.waybillEmptyDialog) }
                }
                1 -> btnWaybill.setOnClickListener {
                    findNavController().navigate(
                        R.id.waybillDialog,
                        bundleOf(WaybillDialog.BUNDLE_WAYBILL to data.first())
                    )
                }

                else -> btnWaybill.setOnClickListener { findNavController().navigate(R.id.waybillsAllFragment) }
            }
        }
    }

    override fun initClicks() {
        binding.apply {
            btnList.setOnClickListener { findNavController().navigate(R.id.listsDialog) }
            locateBtn.setOnClickListener {
                if (hasLocationPermission()) {
                    if (userLocationLayer != null) {
                        cameraUserPosition()
                    } else {
                        onMapReady()
                    }

                    followUserLocation = false
                } else requestLocationPermission()
            }
        }
    }

    // check if user provide location
    private fun hasLocationPermission() =
        EasyPermissions.hasPermissions(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION)

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    private fun requestLocationPermission() {
        EasyPermissions.requestPermissions(
            this,
            getString(R.string.requestForLocation),
            PERMISSION_LOCATION_REQUEST_CODE,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
    }

    companion object {
        const val PERMISSION_LOCATION_REQUEST_CODE = 123
    }

    override fun onPermissionsDenied(requestCode: Int, perms: List<String>) {
        if (EasyPermissions.somePermissionDenied(this, perms.first())) {
            SettingsDialog.Builder(requireActivity())
                .negativeButtonText("Отмена")
                .positiveButtonText("Ок")
                .build().show()
        } else {
            requestLocationPermission()
        }
    }

    override fun onPermissionsGranted(requestCode: Int, perms: List<String>) {
        toast(requireContext(), "permission granted")
        onMapReady()
    }

    override fun onClusterAdded(cluster: Cluster) {
        cluster.appearance.setIcon(
            TextImageProvider(cluster.size.toString(), requireContext())
        )
        cluster.addClusterTapListener(this)
    }

    override fun onClusterTap(cluster: Cluster): Boolean {
        toast(requireContext(), "clicked")
        return true
    }

    override fun onStop() {
        super.onStop()
        binding.mapView.onStop()
        MapKitFactory.getInstance().onStop()
    }

    override fun onStart() {
        super.onStart()
        binding.mapView.onStart()
        MapKitFactory.getInstance().onStart()
    }

    private fun onMapReady() {
        val mapKit = MapKitFactory.getInstance()
        userLocationLayer = mapKit.createUserLocationLayer(binding.mapView.mapWindow)
        userLocationLayer?.isVisible = true
        userLocationLayer?.isHeadingEnabled = true
        userLocationLayer?.setObjectListener(this)

        binding.mapView.map.addCameraListener(this)

        cameraUserPosition()
    }

    private fun cameraUserPosition() {
        if (userLocationLayer?.cameraPosition() != null) {
            routeStartLocation = userLocationLayer?.cameraPosition()!!.target
            binding.mapView.map.move(
                CameraPosition(routeStartLocation, 16f, 0f, 0f), Animation(Animation.Type.SMOOTH, 1f), null
            )
        } else {
            binding.mapView.map.move(CameraPosition(Point(0.0, 0.0), 16f, 0f, 0f))
        }
    }

    override fun onObjectAdded(userLocationView: UserLocationView) {
        setAnchor()
        userLocationView.pin.setIcon(ImageProvider.fromResource(requireContext(), R.drawable.pin_user))
        userLocationView.arrow.setIcon(ImageProvider.fromResource(requireContext(), R.drawable.pin_user))
    }

    override fun onObjectRemoved(p0: UserLocationView) {}

    override fun onObjectUpdated(p0: UserLocationView, p1: ObjectEvent) {}

    override fun onCameraPositionChanged(
        p0: Map,
        p1: CameraPosition,
        p2: CameraUpdateReason,
        finish: Boolean
    ) {
        if (finish) {
            if (followUserLocation) {
                setAnchor()
            }
        } else {
            if (!followUserLocation) {
                noAnchor()
            }
        }
    }

    private fun setAnchor() {
        userLocationLayer?.setAnchor(
            PointF((binding.mapView.width * 0.5).toFloat(), (binding.mapView.height * 0.5).toFloat()),
            PointF((binding.mapView.width * 0.5).toFloat(), (binding.mapView.height * 0.83).toFloat())
        )

        followUserLocation = false
    }

    private fun noAnchor() {
        userLocationLayer?.resetAnchor()
    }
}
