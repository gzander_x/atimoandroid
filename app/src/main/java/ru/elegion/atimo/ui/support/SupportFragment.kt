package ru.elegion.atimo.ui.support

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import ru.elegion.atimo.R
import ru.elegion.atimo.databinding.FragmentSupportBinding
import ru.elegion.atimo.ui.base.BaseFragment
import ru.elegion.atimo.ui.support.adapter.MenuOption
import ru.elegion.atimo.ui.support.adapter.MenuOptionsAdapter
import ru.elegion.atimo.util.extensions.toastLong

class SupportFragment : BaseFragment<FragmentSupportBinding>() {

    private val supportMenuOptions = listOf(
        MenuOption(
            id = "support_menu_option_call",
            titleRes = R.string.support_option_call,
            iconRes = R.drawable.ic_phone_outline,
            onClickListener = ::dialSupport
        ),
        MenuOption(
            id = "support_menu_option_telegram_chat",
            titleRes = R.string.support_option_telegram_chat,
            iconRes = R.drawable.ic_message_circle_outline,
            onClickListener = ::contactTelegram
        )
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initRecycler()
    }

    override fun initBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentSupportBinding.inflate(inflater, container, false)

    private fun initRecycler() = binding.run {
        recyclerOptions.adapter = MenuOptionsAdapter().apply {
            submitList(supportMenuOptions)
        }
    }

    private fun dialSupport() {
        val phoneNumber = "+79999999999"
        val intent = Intent(Intent.ACTION_DIAL, "tel:$phoneNumber".toUri())
        intent.resolveActivity(requireActivity().packageManager)?.let {
            startActivity(intent)
        }

        // TODO указать номер службы поддержки и убрать тост
        toastLong(requireContext(), "Скоро здесь будет настоящий контакт службы поддержки")
    }

    private fun contactTelegram() {
        val telegramUrl = "https://t.me/BotFather"
        val intent = Intent(Intent.ACTION_VIEW, telegramUrl.toUri())
        intent.resolveActivity(requireActivity().packageManager)?.let {
            startActivity(intent)
        }

        // TODO указать контакт службы поддержки и убрать тост
        toastLong(requireContext(), "Скоро здесь будет настоящий контакт службы поддержки")
    }
}