package ru.elegion.atimo.ui.map.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.points.PointsRes
import ru.elegion.atimo.repository.terminals.TerminalsRepository
import ru.elegion.atimo.util.Resource
import javax.inject.Inject

@HiltViewModel
class ListsViewModel @Inject constructor(
    private val terminalsRepository: TerminalsRepository
) : ViewModel() {

    private var findTerminal: Job? = null
    private var listTerminals: PointsRes? = null

    private val _liveDataTerminals: MutableLiveData<Resource<PointsRes>> = MutableLiveData()
    val liveDataTerminals: LiveData<Resource<PointsRes>> = _liveDataTerminals

    init {
        getTerminals()
    }

    private fun getTerminals() {
        viewModelScope.launch(Dispatchers.IO) {
            val response = terminalsRepository.getTerminals()
            listTerminals = response.data
            _liveDataTerminals.postValue(response)
        }
    }

    fun findTerminal(name: String) {
        if (findTerminal?.isActive == true) {
            findTerminal?.cancel()
        }
        if (name.isNotBlank()) {
            findTerminal = viewModelScope.launch(Dispatchers.IO) {
                delay(100)
                val list = listTerminals?.points?.toMutableList()
                    ?.filter { it.name.contains(name, ignoreCase = true) } ?: listOf()
                val filteredRes = listTerminals?.copy(points = list)
                if (list.isNotEmpty()) {
                    _liveDataTerminals.postValue(Resource.Success(filteredRes))
                } else {
                    _liveDataTerminals.postValue(Resource.Error(R.string.drivers__bad_find))
                    _liveDataTerminals.postValue(Resource.Success(filteredRes))
                }
            }
        } else {
            _liveDataTerminals.value = Resource.Success(listTerminals)
        }
    }
}
