package ru.elegion.atimo.ui.onboarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.onboarding.OnboardingSegment
import ru.elegion.atimo.databinding.FragmentOnboarding1Binding
import ru.elegion.atimo.databinding.ItemOnboardingFooterBinding
import ru.elegion.atimo.databinding.ItemOnboardingHeaderBinding
import ru.elegion.atimo.ui.base.BaseOnboardingFragment

@AndroidEntryPoint
class FirstOnboardingFragment: BaseOnboardingFragment<FragmentOnboarding1Binding>() {

    override val segment: OnboardingSegment = OnboardingSegment.FIRST

    override val msgFooter: Int?
        get() = null
    override val imgFooter: Int?
        get() = null
    override val actionBtnNext: () -> Unit
        get() = ({
            navigate(R.id.secondOnboardingFragment)
        })

    override fun initBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentOnboarding1Binding = FragmentOnboarding1Binding.inflate(inflater)

    override fun initHeader(): ItemOnboardingHeaderBinding = binding.layoutHeader

    override fun initFooter(): ItemOnboardingFooterBinding? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnNext.setOnClickListener {
            actionBtnNext.invoke()
        }
    }

}