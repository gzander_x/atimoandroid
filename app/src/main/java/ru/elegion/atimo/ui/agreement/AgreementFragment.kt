package ru.elegion.atimo.ui.agreement

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.databinding.FragmentAgreementBinding
import ru.elegion.atimo.databinding.FragmentFaqBinding
import ru.elegion.atimo.databinding.FragmentHistoryBinding
import ru.elegion.atimo.databinding.FragmentHowWorkBinding
import ru.elegion.atimo.ui.base.BaseFragment

@AndroidEntryPoint
class AgreementFragment : BaseFragment<FragmentAgreementBinding>(){

    private val viewModel: AgremmentViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //setOnClickListeners()
        //setupObservers()
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentAgreementBinding =
        FragmentAgreementBinding.inflate(layoutInflater)

    private fun setOnClickListeners(){
    }

    private fun setupObservers(){
    }
}