package ru.elegion.atimo.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.points.Point
import ru.elegion.atimo.databinding.ItemListBinding
import ru.elegion.atimo.ui.adapters.base.BaseAdapter

class ListsAdapter : BaseAdapter<Point, ItemListBinding>() {
    override fun getBinding(inflater: LayoutInflater, parent: ViewGroup, viewType: Int) =
        ItemListBinding.inflate(inflater, parent, false)

    override fun bindViewHolder(holder: ViewBindingHolder, data: Point) {
        holder.binding {
            data.apply {
                viewStatus.background = if (mo && !to) {
                    AppCompatResources.getDrawable(root.context, R.drawable.ic_ellipse_purple)
                } else if (to && !mo) {
                    AppCompatResources.getDrawable(root.context, R.drawable.ic_ellipse_blue)
                } else if (to && mo) {
                    AppCompatResources.getDrawable(root.context, R.drawable.ic_eclipse_half_blue_half_purple)
                } else {
                    AppCompatResources.getDrawable(root.context, R.drawable.ic_ellipse_grey)
                }
                // TODO вставить название пунтка с бека
                tvName.text = "Название пункта"
                tvAddress.text = name
                tvWorkTime.text =
                    "${root.context.getString(R.string.daily)} $work_time_begin - $work_time_end"
            }
        }
    }
}
