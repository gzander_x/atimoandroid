package ru.elegion.atimo.ui.base

import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.viewbinding.ViewBinding
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import ru.elegion.atimo.R

abstract class BaseDialogFragment<B : ViewBinding> : BottomSheetDialogFragment() {
    private var _viewBinding: B? = null
    protected val binding get() = checkNotNull(_viewBinding)

    override fun getTheme(): Int = R.style.BaseBottomSheetDialog

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _viewBinding = initBinding(inflater, container)
        initClicks()
        return binding.root
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = BottomSheetDialog(requireContext(), theme)
        dialog.setOnShowListener { setupBottomSheet(it) }
        return dialog
    }

    private fun setupBottomSheet(dialogInterface: DialogInterface) {
        val bottomSheetDialog = dialogInterface as BottomSheetDialog
        val bottomSheet = bottomSheetDialog.findViewById<View>(
            com.google.android.material.R.id.design_bottom_sheet)
            ?: return
        bottomSheet.setBackgroundColor(Color.TRANSPARENT)

        val layoutParams = bottomSheet.layoutParams as CoordinatorLayout.LayoutParams

        layoutParams.setMargins(
            resources.getDimensionPixelSize(R.dimen.dialog_hor_margin),
            0,
            resources.getDimensionPixelSize(R.dimen.dialog_hor_margin),
            resources.getDimensionPixelSize(R.dimen.dialog_bottom_margin)
        )

        bottomSheet.layoutParams = layoutParams
    }

    abstract fun initBinding(inflater: LayoutInflater, container: ViewGroup?): B

    protected open fun initClicks() {
    }

    override fun onDestroy() {
        super.onDestroy()
        _viewBinding = null
    }
}