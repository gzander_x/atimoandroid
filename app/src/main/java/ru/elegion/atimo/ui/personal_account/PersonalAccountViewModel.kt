package ru.elegion.atimo.ui.personal_account

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.company.CompanyBalance
import ru.elegion.atimo.data.models.drivers.DriversRes
import ru.elegion.atimo.data.models.vehicles.VehicleServer
import ru.elegion.atimo.repository.cars.CarsRepository
import ru.elegion.atimo.repository.company.CompanyRepository
import ru.elegion.atimo.repository.drivers.DriversRepository
import ru.elegion.atimo.util.Resource
import ru.elegion.atimo.util.SimpleResource
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class PersonalAccountViewModel @Inject constructor(
    private val companyRepository: CompanyRepository,
    private val carsRepository: CarsRepository,
    private val driversRepository: DriversRepository
) : ViewModel() {


    private val _liveDataLoader: MutableLiveData<SimpleResource> = MutableLiveData()
    val liveDataLoader: LiveData<SimpleResource> = _liveDataLoader

    private val _liveDataBalance: MutableLiveData<Int> = MutableLiveData()
    val liveDataBalance: LiveData<Int> = _liveDataBalance

    private val _liveDataCarsSize: MutableLiveData<Int> = MutableLiveData()
    val liveDataCarsSize: LiveData<Int> = _liveDataCarsSize

    private val _liveDataDriversSize: MutableLiveData<Int> = MutableLiveData()
    val liveDataDriversSize: LiveData<Int> = _liveDataDriversSize

    fun getInfo() {
        viewModelScope.launch(Dispatchers.IO){
            _liveDataLoader.postValue(Resource.Loading())
            val balanceDeferred = async { getBalance() }
            val driversDeferred = async { getDrivers() }
            val carsDeferred = async { getCars() }

            val balance = balanceDeferred.await()
            val drivers = driversDeferred.await()
            val cars = carsDeferred.await()
            if(balance is Resource.Error || drivers is Resource.Error || cars is Resource.Error){
                _liveDataLoader.postValue(Resource.Error(R.string.error_network_default))
            }else{
                companyRepository.serDrivers(drivers.data?.drivers?: emptyList())
                companyRepository.setCars(
                    cars.data?.vehicles?.map(VehicleServer::transform) ?: emptyList()
                )
                _liveDataBalance.postValue(balance.data?.balance ?: 0)
                _liveDataDriversSize.postValue(drivers.data?.drivers?.size ?: 0)
                _liveDataCarsSize.postValue(cars.data?.vehicles?.size ?: 0)
                _liveDataLoader.postValue(Resource.Success(Unit))
            }

        }
    }

    private suspend fun getDrivers(): Resource<DriversRes> {
        return try {
            driversRepository.getDriversNetwork()
        }catch (e: Exception){
            Timber.e("getDrivers = ${e.message}")
            Resource.Error(R.string.error_code_text)
        }
    }

    private suspend fun getCars() = carsRepository.getCarsNetwork()

    private suspend fun getBalance(): Resource<CompanyBalance>{
        return try {
            val response = companyRepository.gerBalance()
            Resource.Success(response)
        }catch (e: Exception){
            Timber.e("getBalance ${e.message}")
            Resource.Error(R.string.error_code_text)
        }
    }

}