package ru.elegion.atimo.ui.onboarding.vm

import android.os.CountDownTimer
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import ru.elegion.atimo.util.EventLiveData
import javax.inject.Inject

@HiltViewModel
class OnboardingViewModel @Inject constructor(): ViewModel() {

    companion object{
        private const val TIMEOUT = 5000L
        private const val ONE_SECOND = 1000L
    }

    val timerValue = EventLiveData<Long>()
    val navigateNextFragment = EventLiveData<Unit>()

    private val timer: CountDownTimer = object : CountDownTimer(TIMEOUT, ONE_SECOND / 10){
        override fun onTick(tick: Long) {
            timerValue.postValue(tick)
        }

        override fun onFinish() {
            navigateNextFragment.postValue(Unit)
        }

    }
    fun starTimer(){
        timer.start()
    }
    fun stopTimer(){
        timer.cancel()
    }

    override fun onCleared() {
        super.onCleared()
        timer.cancel()
    }
}