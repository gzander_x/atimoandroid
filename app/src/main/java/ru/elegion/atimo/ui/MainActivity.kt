package ru.elegion.atimo.ui

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.Window
import android.widget.FrameLayout
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.adapter.DrawerMenuModel
import ru.elegion.atimo.databinding.ActivityMainBinding
import ru.elegion.atimo.repository.user.UserAuthStorage
import ru.elegion.atimo.ui.adapters.DrawerMenuAdapter
import ru.elegion.atimo.util.Constants.USER_TYPE_DRIVER
import ru.elegion.atimo.util.Constants.USER_TYPE_ORGANIZATION
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var userAuthStorage: UserAuthStorage

    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController

    private lateinit var drawerMenuAdapter: DrawerMenuAdapter
    private lateinit var appBarConfiguration: AppBarConfiguration

    private var loadingDialog: Dialog? = null

    private val destinationNoChangeTittle = listOf(
        R.id.messageDialog,
        R.id.dialogAlertFragment,
        R.id.actionsDriverDialog,
        R.id.actionsCarDialog,
        R.id.actionPhotoFragment,
        R.id.filterPassedDialog,
        R.id.inspectionDialog,
        R.id.medInspectionDialog,
        R.id.listsDialog,
        R.id.mapFragment,
        R.id.waybillDialog,
        R.id.listDetailDialog,
        R.id.operationDialog,
        R.id.addDriverFragment,
        R.id.addCarFragment
    )

    private val destinationOnboarding = listOf(
        R.id.firstOnboardingFragment,
        R.id.secondOnboardingFragment,
        R.id.thirdOnboardingFragment,
        R.id.fourthOnboardingFragment,
        R.id.fifthOnboardingFragment,
        R.id.sixthOnboardingFragment,
        R.id.seventhOnboardingFragment,
        R.id.eighthOnboardingFragment
    )

    private val destinationNoToolbar = listOf(
        R.id.splashFragment,
        R.id.loginFragment,
        R.id.regFragment,
        R.id.listsDialog,
        R.id.mapFragment,
        R.id.listDetailDialog,
        R.id.waybillDialog,
        R.id.messageDialog
    )

    private val destinationVisibleBottomMenu = listOf(
        R.id.personalAccountFragment, R.id.mapFragment
    )

    private val destinationLockedDrawer = listOf(
        R.id.splashFragment,
        R.id.loginFragment,
        R.id.codeEnterFragment,
        R.id.regFragment
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.topBar.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.splashFragment,
                R.id.loginFragment,
                R.id.regFragment,
                R.id.personalAccountFragment,
                R.id.mapFragment
            ),
            binding.root
        )
        setupActionBarWithNavController(navController, appBarConfiguration)

        binding.bottomMenu.setupWithNavController(navController)

        navController.addOnDestinationChangedListener { _, destination, _ ->
            if (destination.id !in destinationNoChangeTittle) {
                binding.topBar.toolbarTitle.text = destination.label
            }
            binding.bottomMenu.isVisible = destination.id in destinationVisibleBottomMenu &&
                userAuthStorage.currentUser.userRole == USER_TYPE_ORGANIZATION

            binding.topBar.toolbar.isVisible = destination.id !in destinationNoToolbar && destination.id !in destinationOnboarding
            if(destination.id in destinationOnboarding){
                window.statusBarColor = ContextCompat.getColor(this, R.color.onboarding_status_bar)
            }else{
                window.statusBarColor = ContextCompat.getColor(this, R.color.white)
            }
            if (destination.id == R.id.personalAccountFragment) {
                supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_menu_drawer)
            } else {
                supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_arrow)
            }

            val drawerLockMode = if (destination.id in destinationLockedDrawer) {
                DrawerLayout.LOCK_MODE_LOCKED_CLOSED
            } else {
                DrawerLayout.LOCK_MODE_UNDEFINED
            }
            binding.root.setDrawerLockMode(drawerLockMode)
        }
        initNavigationHeader()
        setListeners()
        initRecycler()
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    fun openDrawerMenu() {
        binding.root.openDrawer(binding.navigationDrawer.navigationLayout)
    }
    fun changeTittle(tittle: String) {
        binding.topBar.toolbarTitle.text = tittle
    }

    fun initNavigationHeader() {
        binding.navigationDrawer.run {
            val user = userAuthStorage.currentUser
            val role = user.userRole
            imgAvatar.isVisible = role == USER_TYPE_DRIVER
            textTittle.text = if (role == USER_TYPE_DRIVER) {
                user.firstName
            } else {
                "" //TODO не приходит с сервера
            }
        }
    }

    private fun setListeners() {
        binding.navigationDrawer.navigationProfile.setOnClickListener {
            binding.root.closeDrawers()
            val user = userAuthStorage.currentUser
            if(user.userRole == USER_TYPE_DRIVER){
                navController.navigate(R.id.profileDriverFragment)
            }else {
                navController.navigate(R.id.profileFragment)
            }

        }
    }

    private fun initRecycler() {

        // todo set all navigation id
        val items = arrayListOf(
            DrawerMenuModel(getString(R.string.drawer_menu_history), R.id.historyFragment),
            DrawerMenuModel(getString(R.string.drawer_menu_favorite), null),
            DrawerMenuModel(getString(R.string.drawer_menu_inf), R.id.infoFragment),
            DrawerMenuModel(getString(R.string.drawer_menu_supp), R.id.supportFragment),
            DrawerMenuModel(getString(R.string.drawer_menu_notifi), R.id.notificationsFragment, 2),
            DrawerMenuModel(getString(R.string.drawer_menu_settings), R.id.settingsFragment)
        )

        with(binding) {
            drawerMenuAdapter = DrawerMenuAdapter().apply {
                listener = {
                    if (it.navigation != null) {
                        navController.navigate(it.navigation)
                        root.closeDrawers()
                    } else {
                        Toast.makeText(binding.getRoot().context, it.title, Toast.LENGTH_SHORT).show()
                    }
                }
                setData(items)
            }
            navigationDrawer.listDrawerMenu.adapter = drawerMenuAdapter
        }
    }

    fun showLoadingDialog() {
        loadingDialog = Dialog(this)

        loadingDialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        loadingDialog?.setCancelable(false)

        val relativeLayout = RelativeLayout(this)
        relativeLayout.layoutParams = RelativeLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT)

        val progressBar = ProgressBar(this, null, android.R.attr.progressBarStyleLarge)
        val progressBg = FrameLayout(this, null)

        progressBg.background = ResourcesCompat.getDrawable(resources, R.drawable.circle_notification, null)
        progressBg.background.setTint(Color.WHITE)

        val params = RelativeLayout.LayoutParams(resources.getDimensionPixelSize(R.dimen.progress_bar), resources.getDimensionPixelSize(R.dimen.progress_bar))
        params.addRule(RelativeLayout.CENTER_IN_PARENT)

        val paramsBg = RelativeLayout.LayoutParams(resources.getDimensionPixelSize(R.dimen.progress_bg), resources.getDimensionPixelSize(R.dimen.progress_bg))
        params.addRule(RelativeLayout.CENTER_IN_PARENT)

        relativeLayout.addView(progressBg, paramsBg)
        relativeLayout.addView(progressBar, params)

        loadingDialog?.window?.setContentView(relativeLayout, relativeLayout.layoutParams)
        loadingDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        loadingDialog?.show()
    }

    fun hideLoadingDialog() {
        loadingDialog?.dismiss()
        loadingDialog = null
    }
}
