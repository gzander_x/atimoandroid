package ru.elegion.atimo.ui.legal

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.databinding.FragmentFaqBinding
import ru.elegion.atimo.databinding.FragmentHistoryBinding
import ru.elegion.atimo.databinding.FragmentHowWorkBinding
import ru.elegion.atimo.databinding.FragmentLegalBinding
import ru.elegion.atimo.ui.base.BaseFragment
import ru.elegion.atimo.util.extensions.toast

@AndroidEntryPoint
class LegalFragment : BaseFragment<FragmentLegalBinding>(){

    private val viewModel: LegalViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setOnClickListeners()
        //setupObservers()
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentLegalBinding =
        FragmentLegalBinding.inflate(layoutInflater)

    private fun setOnClickListeners(){
        binding.agreement.setOnClickListener {
            toast(requireContext(), "В разработке")
            //TODO not yet implements
        }
        binding.privacy.setOnClickListener {
            //TODO not yet implements
            toast(requireContext(), "В разработке")
        }
        binding.contracts.setOnClickListener {
            navigate(R.id.contractsFragment)
        }
    }

    private fun setupObservers(){
    }
}