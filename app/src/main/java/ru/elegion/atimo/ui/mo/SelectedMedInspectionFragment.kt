package ru.elegion.atimo.ui.mo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.drivers.DriverMo
import ru.elegion.atimo.databinding.FragmentInspectionBinding
import ru.elegion.atimo.ui.adapters.base.BaseListsAdapterWithPaging
import ru.elegion.atimo.ui.base.BaseFragment

@AndroidEntryPoint
class SelectedMedInspectionFragment : BaseFragment<FragmentInspectionBinding>() {


    private val viewModel: MedInspectionViewModel by viewModels()
    private val adapter = BaseListsAdapterWithPaging().apply {
        listener = { fromAdapter ->
            (fromAdapter?.data as? DriverMo)?.let {
                navigate(R.id.medInspectionDialog, MedInspectionDialog.prepareBundle(it))
            }
        }
        addLoadStateListener { state ->
            showLoader(state.refresh == LoadState.Loading)

            if (state.refresh is LoadState.Error) {
                retryDialogMessage(::refresh)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        setupObservers()
    }

    override fun initBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentInspectionBinding = FragmentInspectionBinding.inflate(layoutInflater)

    private fun initViews() = binding.run {
        viewSelectDate.isVisible = false
        textCounter.isVisible = false
        textCarInfo.text = arguments?.getString(KEY_DRIVER_NAME)

        listInspection.adapter = adapter
        listInspection.layoutManager = LinearLayoutManager(requireContext())
    }

    private fun setupObservers() = viewModel.run {
        medInspections.observe(viewLifecycleOwner) {
            lifecycleScope.launch { adapter.submitData(it) }
        }
        filterUpdated.observe(viewLifecycleOwner) {
            adapter.refresh()
        }
    }

    companion object {

        private const val KEY_DRIVER_NAME = "driverName"
        const val KEY_DRIVER_PHONE = "driverPhone"

        fun prepareBundle(driverName: String, driverPhone: String) = bundleOf(
            KEY_DRIVER_NAME to driverName,
            KEY_DRIVER_PHONE to driverPhone
        )
    }
}