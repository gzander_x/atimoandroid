package ru.elegion.atimo.ui.cars

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.vehicles.Vehicle
import ru.elegion.atimo.repository.cars.CarsRepository
import ru.elegion.atimo.util.Constants.SEARCH_DELAY_MILLIS
import ru.elegion.atimo.util.EventLiveData
import ru.elegion.atimo.util.Resource
import ru.elegion.atimo.util.toTyped
import javax.inject.Inject

@HiltViewModel
class CarsViewModel @Inject constructor(private val carsRepository: CarsRepository) : ViewModel() {

    private var findJob: Job? = null
    private var listCars: MutableList<Vehicle> = mutableListOf()

    private val _liveDataCarsList: MutableLiveData<Resource<List<Vehicle>>> = MutableLiveData()
    val liveDataCarsList: LiveData<Resource<List<Vehicle>>> = _liveDataCarsList

    private var carBackUp: Vehicle? = null
    private val _selectedCar: MutableLiveData<Vehicle> = MutableLiveData()
    val selectedCar: LiveData<Vehicle> = _selectedCar

    private val _liveDataClickSave: EventLiveData<Resource<Vehicle>> = EventLiveData()
    val liveDataClickSave: LiveData<Resource<Vehicle>> = _liveDataClickSave

    private val _liveDataVehicleLockChanged: EventLiveData<Resource<Vehicle>> = EventLiveData()
    val liveDataVehicleLockChanged: LiveData<Resource<Vehicle>> = _liveDataVehicleLockChanged

    var isVisiblePassed: Boolean = carsRepository.isBlockedCars

    val isStateFilterLiveData = MutableLiveData<Boolean?>()

    val showToastNotSearch = EventLiveData<Int>()
    val showBlockDialog = EventLiveData<Vehicle>()
    val navigateToChanged = EventLiveData<Vehicle>()
    val navigateToHistory = EventLiveData<Vehicle>()

    init {
        getListCars()
    }

    private fun getListCars() {
        val responseCars = carsRepository.getCarsCash()
        listCars = responseCars.toMutableList()
        _liveDataCarsList.value = Resource.Success(responseCars)
    }

    fun getListCatsWithFilter (isBlocked: Boolean?) {
        isStateFilterLiveData.value = isBlocked
        viewModelScope.launch(Dispatchers.IO){
            val responseDrivers = carsRepository.getCarsCashWithFilter(isBlocked)
            listCars = responseDrivers.toMutableList()
            _liveDataCarsList.postValue(Resource.Success(responseDrivers))
        }
    }

    fun findCars(name: String) {
        if (findJob?.isActive == true) {
            findJob?.cancel()
        }
        if (name.isNotBlank()) {
            findJob = viewModelScope.launch {
                delay(SEARCH_DELAY_MILLIS)
                val list = listCars.toMutableList()
                    .filter { (it.model).lowercase().contains(name.lowercase()) }
                if (list.isNotEmpty()) {
                    _liveDataCarsList.postValue(Resource.Success(list))
                } else {
                    showToastNotSearch.postValue(R.string.cars__bad_find)
                    _liveDataCarsList.postValue(Resource.Success(listCars))
                }
            }
        } else {
            _liveDataCarsList.value = Resource.Success(listCars)
        }
    }

    fun setCarMenu(car: Vehicle) {
        carBackUp = Vehicle(car)
        _selectedCar.value = car
    }

    fun rollbackCar(){
        if(carBackUp != null){
            _selectedCar.value?.let {
                val driver = it
                driver.rollback(carBackUp!!)
            }
        }
    }

    fun onClickBlock() = withSelectedCar(showBlockDialog::setValue)

    fun onClickChanged() = withSelectedCar(navigateToChanged::setValue)

    fun onClickHistory() = withSelectedCar(navigateToHistory::setValue)

    fun onClickSave(car: Vehicle) {
        viewModelScope.launch {
            _liveDataClickSave.postValue(Resource.Loading())
            _liveDataClickSave.postValue(carsRepository.createModifyVehicle(car).toTyped(car))
        }
    }

    fun changeVehicleLocked() {
        _selectedCar.value?.let {
            val car = it
            car.vehicleStatus = !car.vehicleStatus
            viewModelScope.launch {
                _liveDataVehicleLockChanged.postValue(Resource.Loading())
                _liveDataVehicleLockChanged.postValue(carsRepository.createModifyVehicle(car).toTyped(car))

            }
        }
    }

    fun deleteCarFromLocal(position: Int){
        listCars.removeAt(position)
        if(listCars.isEmpty()){
            _liveDataCarsList.postValue(Resource.Success(listCars))
        }
    }

    private fun withSelectedCar(block: (Vehicle) -> Unit) = selectedCar.value?.let(block)

    override fun onCleared() {
        super.onCleared()
        findJob?.cancel()
        findJob = null
        carBackUp = null
    }
}