package ru.elegion.atimo.ui.adapters.notifications

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import ru.elegion.atimo.data.models.notifications.Notification
import ru.elegion.atimo.databinding.ItemNotificationBinding
import ru.elegion.atimo.ui.adapters.base.BaseAdapter

class NotificationsAdapter: BaseAdapter<Notification, ItemNotificationBinding>() {
    override fun getBinding(
        inflater: LayoutInflater,
        parent: ViewGroup,
        viewType: Int
    ): ItemNotificationBinding = ItemNotificationBinding.inflate(inflater, parent, false)

    override fun bindViewHolder(holder: ViewBindingHolder, data: Notification) {
        holder.binding{
            imageNotificationCircle.isVisible = data.status?:0 > 0
            textMessage.text = data.message
            textDate.text = data.date
        }
    }
}