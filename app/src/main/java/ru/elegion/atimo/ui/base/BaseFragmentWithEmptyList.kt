package ru.elegion.atimo.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.viewbinding.ViewBinding
import ru.elegion.atimo.databinding.LayoutEmptyListBinding

abstract class BaseFragmentWithEmptyList<B : ViewBinding> : BaseFragmentWithToolbarMenu<B>() {

    abstract val title: Int
    abstract val subTitle: Int
    abstract val btnTittle: Int
    abstract val imgSrc: Int
    abstract val navigateId: Int

    abstract fun initEmptyLayout(): LayoutEmptyListBinding

    private var _viewBindingEmpty: LayoutEmptyListBinding? = null
    protected val bindingEmpty get() = checkNotNull(_viewBindingEmpty)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _viewBindingEmpty = initEmptyLayout()
        _viewBindingEmpty?.apply {
            tvTittle.text = resources.getString(title)
            tvSubtitle.text = resources.getString(subTitle)
            imgEmpty.setImageDrawable(ContextCompat.getDrawable(requireContext(), imgSrc))
            btnEmpty.text = resources.getString(btnTittle)
            btnEmpty.setOnClickListener {
                navigate(navigateId)
            }
        }
    }

    open fun checkEmpty(list: List<Any>?) {
        if(list != null && list.isNotEmpty()){
            bindingEmpty.layoutEmpty.visibility = View.GONE
        }else{
            bindingEmpty.layoutEmpty.visibility = View.VISIBLE
        }
    }
}