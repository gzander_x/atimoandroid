package ru.elegion.atimo.ui.onboarding

import android.view.LayoutInflater
import android.view.ViewGroup
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.onboarding.OnboardingSegment
import ru.elegion.atimo.databinding.*
import ru.elegion.atimo.ui.base.BaseFragment
import ru.elegion.atimo.ui.base.BaseOnboardingFragment

@AndroidEntryPoint
class FifthOnboardingFragment: BaseOnboardingFragment<FragmentOnboarding5Binding>() {
    override val segment: OnboardingSegment = OnboardingSegment.FIFTH
    override val msgFooter: Int
        get() = R.string.onboarding_screen_5_msg
    override val imgFooter: Int
        get() = R.drawable.ic_clock

    override val actionBtnNext: () -> Unit
        get() = ({
            navigate(R.id.sixthOnboardingFragment)
        })

    override fun initBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentOnboarding5Binding = FragmentOnboarding5Binding.inflate(inflater)

    override fun initHeader(): ItemOnboardingHeaderBinding = binding.layoutHeader

    override fun initFooter(): ItemOnboardingFooterBinding = binding.layoutFooter
}