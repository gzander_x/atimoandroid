package ru.elegion.atimo.ui.onboarding

import android.view.LayoutInflater
import android.view.ViewGroup
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.onboarding.OnboardingSegment
import ru.elegion.atimo.databinding.*
import ru.elegion.atimo.ui.base.BaseFragment
import ru.elegion.atimo.ui.base.BaseOnboardingFragment

@AndroidEntryPoint
class FourthOnboardingFragment: BaseOnboardingFragment<FragmentOnboarding4Binding>() {
    override val segment: OnboardingSegment = OnboardingSegment.FOURTH
    override val msgFooter: Int
        get() = R.string.onboarding_screen_4_msg
    override val imgFooter: Int
        get() = R.drawable.ic_checkmark_square
    override val actionBtnNext: (() -> Unit)
        get() = ({
            navigate(R.id.fifthOnboardingFragment)
        })
    override fun initBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentOnboarding4Binding = FragmentOnboarding4Binding.inflate(inflater)

    override fun initHeader(): ItemOnboardingHeaderBinding = binding.layoutHeader

    override fun initFooter(): ItemOnboardingFooterBinding = binding.layoutFooter

}