package ru.elegion.atimo.ui.waybill

import androidx.lifecycle.*
import androidx.paging.PagingData
import dagger.hilt.android.lifecycle.HiltViewModel
import ru.elegion.atimo.data.models.BaseAdapterListModel
import ru.elegion.atimo.data.network.base.BaseParamsRequest
import ru.elegion.atimo.repository.drivers.DriversRepository
import ru.elegion.atimo.repository.user.UserRepository
import ru.elegion.atimo.ui.waybill.selected.WaybillsSelectedFragment
import ru.elegion.atimo.util.EventLiveData
import javax.inject.Inject

@HiltViewModel
class WaybillsViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    driversRepository: DriversRepository,
    userRepository: UserRepository
): ViewModel(){

    companion object{
        private const val DIVISOR = 1000
    }

    val updateAdapter = EventLiveData<Unit>()

    val waybillsStream: LiveData<PagingData<BaseAdapterListModel>>

    private val paramsReq: BaseParamsRequest

    init {
        val phone = savedStateHandle.get<String>(WaybillsSelectedFragment.BUNDLE_PHONE)
        paramsReq = BaseParamsRequest(phone = phone ?: userRepository.currentUser.phone)
        waybillsStream = driversRepository.waybillsStream(paramsReq)
    }

   fun getNewStream(){
        updateAdapter.setValue(Unit)
    }

    fun setPassed(passed: Boolean?){
        paramsReq.passed = passed
        getNewStream()
    }
    fun setDate(dateBegin: Long, dateEnd: Long){
        paramsReq.dateBegin = (dateBegin / DIVISOR).toString()
        paramsReq.dateEnd = (dateEnd / DIVISOR).toString()
        getNewStream()
    }
    fun setDateAll(){
        paramsReq.dateBegin = ""
        paramsReq.dateEnd = ""
        getNewStream()
    }

}