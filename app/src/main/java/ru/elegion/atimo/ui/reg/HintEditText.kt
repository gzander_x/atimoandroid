package ru.elegion.atimo.ui.reg

import android.content.Context
import android.content.res.Resources
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import androidx.annotation.ColorRes
import androidx.appcompat.widget.AppCompatEditText
import ru.elegion.atimo.R
import ru.elegion.atimo.util.getColorCompat

class HintEditText: AppCompatEditText {

    constructor(context: Context): super(context)
    constructor(context: Context, attrs: AttributeSet): super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private var textHint: String? = "----"
    private var textOffset = 0f
    var spaceSize = 0f
    var numberSize = 0f
    private val paint = Paint()
    private val rect = Rect()

    init {
        paint.color = context.getColorCompat(R.color.icon_grey)
        setPadding(0, 0, 0, 0)
    }

    fun getHintText(): String? {
        return textHint
    }

    fun setHintText(value: String?) {
        textHint = value
        onTextChange()
        text = text
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        onTextChange()
    }

    private fun onTextChange() {
        textOffset = if (length() > 0) getPaint().measureText(text, 0, length()) else 0F
        spaceSize = getPaint().measureText(" ")
        numberSize = getPaint().measureText("1")
        invalidate()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        onTextChange()
        if (textHint != null && length() < textHint!!.length) {
            val top: Int = measuredHeight / 2
            var offsetX = textOffset
            for (a in length() until textHint!!.length) {
                if (textHint!![a] == ' ') {
                    offsetX += spaceSize
                } else {
                    rect[offsetX.toInt() + pxFromDp(1f), top, (offsetX + numberSize).toInt() - pxFromDp(
                        1f
                    )] =
                        top + pxFromDp(2f)
                    canvas.drawCircle(rect.centerX().toFloat(), rect.centerY().toFloat(), 8f, paint)
                    offsetX += numberSize
                }
            }
        }
    }

    private fun pxFromDp(dp: Float): Int {
        return (dp * Resources.getSystem().displayMetrics.density).toInt()
    }
}