package ru.elegion.atimo.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ru.elegion.atimo.data.models.adapter.DrawerMenuModel
import ru.elegion.atimo.databinding.ItemDrawerMenuBinding
import ru.elegion.atimo.ui.adapters.base.BaseAdapter

class DrawerMenuAdapter : BaseAdapter<DrawerMenuModel, ItemDrawerMenuBinding>() {
    override fun getBinding(inflater: LayoutInflater, parent: ViewGroup, viewType: Int) =
        ItemDrawerMenuBinding.inflate(inflater, parent, false)

    override fun bindViewHolder(holder: ViewBindingHolder, data: DrawerMenuModel) {
        holder.binding{
            data.apply {
                textMenuTitle.text = title
                if(count != null){
                    textMenuCount.visibility = View.VISIBLE
                    textMenuCount.text = count.toString()
                }

            }
        }
    }
}