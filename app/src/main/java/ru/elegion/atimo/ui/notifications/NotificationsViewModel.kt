package ru.elegion.atimo.ui.notifications

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.elegion.atimo.data.models.notifications.Notification
import ru.elegion.atimo.data.models.notifications.NotificationRes
import ru.elegion.atimo.repository.notifications.NotificationsRepository
import ru.elegion.atimo.util.Resource
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class NotificationsViewModel @Inject constructor(
    private val notificationRepository: NotificationsRepository
): ViewModel() {

    private val _liveDataNotifications: MutableLiveData<Resource<NotificationRes>> = MutableLiveData()
    val liveDataNotifications: LiveData<Resource<NotificationRes>> = _liveDataNotifications

    private val _liveDataSelectedNotification: MutableLiveData<Notification> = MutableLiveData()
    val liveDataSelectedNotification: MutableLiveData<Notification> = _liveDataSelectedNotification

    init {
        getNotifications()
    }

    fun getNotifications(){
        viewModelScope.launch{
            _liveDataNotifications.postValue(Resource.Loading())
            delay(1000)
            _liveDataNotifications.postValue(notificationRepository.getNotifications())
        }
    }

    fun setNotification(notification: Notification){
        _liveDataSelectedNotification.value = notification
    }

    override fun onCleared() {
        super.onCleared()
        Timber.d("очистка")
    }
}