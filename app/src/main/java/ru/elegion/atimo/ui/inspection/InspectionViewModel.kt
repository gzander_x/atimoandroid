package ru.elegion.atimo.ui.inspection

import androidx.lifecycle.*
import androidx.paging.cachedIn
import dagger.hilt.android.lifecycle.HiltViewModel
import ru.elegion.atimo.data.models.FilteringListScreenState
import ru.elegion.atimo.data.models.vehicles.InspectionParams
import ru.elegion.atimo.repository.cars.CarsRepository
import ru.elegion.atimo.repository.user.UserRepository
import ru.elegion.atimo.util.EventLiveData
import ru.elegion.atimo.util.extensions.update
import javax.inject.Inject

@HiltViewModel
class InspectionViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    carsRepository: CarsRepository,
    private val userRepository: UserRepository
) : ViewModel() {

    private val carGrz: String? = savedStateHandle.get<String>(SelectedInspectionFragment.KEY_CAR_GRZ)

    private val _screenState = MutableLiveData(FilteringListScreenState())
    val screenState: LiveData<FilteringListScreenState>
        get() = _screenState

    val techInspections = carsRepository.techInspectionsStream(::createParams)
        .cachedIn(viewModelScope)

    val filterUpdated = EventLiveData<Unit>()

    fun updateDateRange(range: Pair<Long, Long>?) {
        if (_screenState.value?.dateRange == range) return

        _screenState.update { it.copy(dateRange = range) }
        filterUpdated.setValue(Unit)
    }

    fun updateFilter(filter: Boolean?) {
        if (_screenState.value?.filteringFlag == filter) return

        _screenState.update { it.copy(filteringFlag = filter) }
        filterUpdated.setValue(Unit)
    }

    private fun millisToSeconds(millis: Long) = millis / 1000

    private fun createParams(): InspectionParams {
        val screenState = _screenState.value
        return InspectionParams(
            telephone = userRepository.currentUser.phone,
            grz = carGrz,
            passed = screenState?.filteringFlag,
            dateBegin = screenState?.dateRange?.let {
                millisToSeconds(it.first).toString()
            } ?: "",
            dateEnd = screenState?.dateRange?.let {
                millisToSeconds(it.second).toString()
            } ?: ""
        )
    }
}