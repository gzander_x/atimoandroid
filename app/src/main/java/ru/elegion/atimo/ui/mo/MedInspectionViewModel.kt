package ru.elegion.atimo.ui.mo

import androidx.lifecycle.*
import androidx.paging.cachedIn
import dagger.hilt.android.lifecycle.HiltViewModel
import ru.elegion.atimo.data.models.FilteringListScreenState
import ru.elegion.atimo.data.models.vehicles.InspectionParams
import ru.elegion.atimo.repository.drivers.DriversRepository
import ru.elegion.atimo.repository.user.UserRepository
import ru.elegion.atimo.util.EventLiveData
import ru.elegion.atimo.util.extensions.update
import javax.inject.Inject

@HiltViewModel
class MedInspectionViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    driversRepository: DriversRepository,
    private val userRepository: UserRepository
) : ViewModel() {

    private val phone: String? = savedStateHandle.get<String>(SelectedMedInspectionFragment.KEY_DRIVER_PHONE)

    private val _screenState = MutableLiveData(FilteringListScreenState())
    val screenState: LiveData<FilteringListScreenState>
        get() = _screenState

    val medInspections = driversRepository.medInspectionsStream(::createParams)
        .cachedIn(viewModelScope)

    val filterUpdated = EventLiveData<Unit>()

    fun updateDateRange(range: Pair<Long, Long>?) {
        if (_screenState.value?.dateRange == range) return

        _screenState.update { it.copy(dateRange = range) }
        filterUpdated.setValue(Unit)
    }

    fun updateFilter(filter: Boolean?) {
        if (_screenState.value?.filteringFlag == filter) return

        _screenState.update { it.copy(filteringFlag = filter) }
        filterUpdated.setValue(Unit)
    }

    private fun millisToSeconds(millis: Long) = millis / 1000

    private fun createParams(): InspectionParams {
        val screenState = _screenState.value
        return InspectionParams(
            telephone = phone ?: userRepository.currentUser.phone,
            passed = screenState?.filteringFlag,
            dateBegin = screenState?.dateRange?.let {
                millisToSeconds(it.first).toString()
            } ?: "",
            dateEnd = screenState?.dateRange?.let {
                millisToSeconds(it.second).toString()
            } ?: ""
        )
    }
}