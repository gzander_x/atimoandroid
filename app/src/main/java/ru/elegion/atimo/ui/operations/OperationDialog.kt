package ru.elegion.atimo.ui.operations

import android.view.LayoutInflater
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.operations.Operation
import ru.elegion.atimo.databinding.DialogOperationBinding
import ru.elegion.atimo.util.BundleBuilder
import ru.elegion.atimo.util.Constants
import ru.elegion.atimo.util.Resource
import ru.elegion.atimo.util.bottomsheet.BaseBottomSheetRoundedFragment
import timber.log.Timber

@AndroidEntryPoint
class OperationDialog: BaseBottomSheetRoundedFragment<DialogOperationBinding>() {

    private val viewModel: OperationsViewModel by viewModels()

    companion object{
        const val EXTRA_OPERATION = "operation"
    }

    override fun initBinding(inflater: LayoutInflater): DialogOperationBinding {
        return DialogOperationBinding.inflate(inflater)
    }

    override fun initialize() {
        setMaxHeight(1.0)
        setupObserver()
        val operation = arguments?.getParcelable<Operation>(EXTRA_OPERATION)
        binding.imgClose.setOnClickListener {
            dismiss()
        }
        operation?.let {
            binding.containerNum.tvItemTittle.text = resources.getString(R.string.operation__num)
            binding.containerNum.tvItemValue.text = it.numDoc

            binding.containerDate.tvItemTittle.text = resources.getString(R.string.operation__date)
            binding.containerDate.tvItemValue.text = it.dateDoc

            binding.containerViews.tvItemTittle.text = resources.getString(R.string.operation__views)
            binding.containerViews.tvItemValue.text = it.countViews.toString()

            binding.containerSum.tvItemTittle.text = resources.getString(R.string.operation__sum)
            binding.containerSum.tvItemValue.text = "${it.sum} ₽"
        }
        binding.btnRequest.setOnClickListener {
            viewModel.sendDocByEmail()
        }
    }

    private fun setupObserver() {
        viewModel.liveDataSendDoc.observe(viewLifecycleOwner){
            Timber.d("Документ отпвлен на почту")
            when(it){
                is Resource.Loading -> {}
                is Resource.Success -> createSuccessRequest()
                is Resource.Error -> retryDialogMessage { viewModel.sendDocByEmail() }
            }
        }
    }

    private fun createSuccessRequest() {
        val dialogBundle = BundleBuilder.getAlertBundle(
            title = getString(R.string.operation_request_success_tittle),
            msg = "test@test.ru",
            btnType = Constants.ALERT_BTN_TYPE_NEG,
            srcImage = R.drawable.ic_checkmark,
        )
        showMessageDialog(bundle = dialogBundle)
    }
}