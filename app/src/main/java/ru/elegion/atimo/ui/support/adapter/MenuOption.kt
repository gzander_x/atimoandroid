package ru.elegion.atimo.ui.support.adapter

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class MenuOption(
    val id: String,
    @StringRes val titleRes: Int,
    @DrawableRes val iconRes: Int = 0,
    val onClickListener: (() -> Unit)? = null
)