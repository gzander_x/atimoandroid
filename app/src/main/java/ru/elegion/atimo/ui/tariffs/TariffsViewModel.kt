package ru.elegion.atimo.ui.tariffs

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import ru.elegion.atimo.util.EventLiveData
import javax.inject.Inject

@HiltViewModel
class TariffsViewModel @Inject constructor() : ViewModel() {

    val showSuccessDialogRequestBill = EventLiveData<Boolean>()

    fun requestBill(){
        //TODO not yet implements server
        showSuccessDialogRequestBill.postValue(true)
    }

}