package ru.elegion.atimo.ui.splash

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.core.os.postDelayed
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavOptions
import androidx.navigation.navOptions
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import ru.elegion.atimo.R
import ru.elegion.atimo.databinding.FragmentSplashBinding
import ru.elegion.atimo.ui.base.BaseFragment
import timber.log.Timber

@AndroidEntryPoint
class SplashFragment : BaseFragment<FragmentSplashBinding>(){

    private val viewModel: SplashViewModel by viewModels()
    private val handler = Handler(Looper.getMainLooper())

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //setOnClickListeners()
        setupObservers()

        initSplashLooper()
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentSplashBinding =
        FragmentSplashBinding.inflate(layoutInflater)

    private fun setOnClickListeners(){
    }

    private fun setupObservers() = viewModel.run {
        userAuthStateChecked.observe(viewLifecycleOwner, ::handleAuthState)
    }

    private fun handleAuthState(isLoggedIn: Boolean) {
        val destinationId = if (isLoggedIn) R.id.mapFragment else R.id.nested_navigation_onboarding
        val navOptions = NavOptions.Builder()
            .setPopUpTo(R.id.nav_graph, true)
            .build()
        navigate(destinationId, navOption = navOptions)
    }

    private fun initSplashLooper(){
        Timber.d("init loop")

        val splashImages = arrayListOf(
            R.drawable.ic_splash_1, R.drawable.ic_splash_2, R.drawable.ic_splash_3, R.drawable.ic_splash_4,
            R.drawable.ic_splash_5, R.drawable.ic_splash_6, R.drawable.ic_splash_7, R.drawable.ic_splash_8)

        var position = 0

        handler.post(object : Runnable {
            override fun run() {
                binding.imageSplash.setImageDrawable(ResourcesCompat.getDrawable(resources, splashImages[position], null))

                position++
                if(position >= 8)
                    position = 0

                Timber.d("handler loop $position")
                handler.postDelayed(this, 500)
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        handler.removeCallbacksAndMessages(null)
    }
}