package ru.elegion.atimo.ui.mo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.datepicker.MaterialDatePicker
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.drivers.DriverMo
import ru.elegion.atimo.databinding.FragmentInspectionBinding
import ru.elegion.atimo.ui.adapters.base.BaseListsAdapterWithPaging
import ru.elegion.atimo.ui.base.BaseFragmentWithToolbarMenu
import ru.elegion.atimo.util.Constants
import ru.elegion.atimo.util.FilterPassedDialogBundleBuilder
import java.text.SimpleDateFormat
import java.util.*
import kotlin.Pair
import androidx.core.util.Pair as AndroidxPair

@AndroidEntryPoint
class AllMedInspectionFragment : BaseFragmentWithToolbarMenu<FragmentInspectionBinding>() {

    override var menuRes: Int? = R.menu.filter_passed_menu
    override val actionsMenu:  HashMap<Int, () -> Unit> = hashMapOf(
        R.id.menuFilterDialog to {
            val bundle = FilterPassedDialogBundleBuilder.Builder()
                .setState(filterState as? Boolean?)
                .setButtonsText(R.array.filter_passed_tittle_buttons_v1).build().getBundle()
            findNavController().navigate(
                R.id.filterPassedDialog, bundle
            )
        }
    )

    override var KEY_REQUEST_MENU: String? = Constants.BUNDLE_DIALOG_PASSED_VALUE
    override var filterResultAction: ((value:Any?)->Unit)? = {
        filterState = it
        if(it is Boolean?){
            changedIconMenu(it)
            viewModel.updateFilter(it)
        }
    }

    private val viewModel: MedInspectionViewModel by viewModels()
    private val adapter = BaseListsAdapterWithPaging().apply {
        listener = {
            (it?.data as? DriverMo)?.let {
                navigate(R.id.medInspectionDialog, MedInspectionDialog.prepareBundle(it))
            }
        }
        addLoadStateListener { state ->
            showLoader(state.refresh == LoadState.Loading)

            if (state.refresh is LoadState.Error) {
                retryDialogMessage(::refresh)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        setupObservers()
    }

    override fun initBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentInspectionBinding = FragmentInspectionBinding.inflate(layoutInflater)

    override fun initClicks() = binding.run {
        viewSelectDate.setOnClickListener {
            showDateRangePicker()
        }
    }

    private fun initViews() = binding.run {
        textCarInfo.isVisible = false
        textCounter.text = resources.getQuantityString(
            R.plurals.inspection__counter,
            123,
            123
        ) // TODO как-то получать общее количество записей

        listInspection.adapter = adapter
        listInspection.layoutManager = LinearLayoutManager(requireContext())
    }

    private fun setupObservers() = viewModel.run {
        medInspections.observe(viewLifecycleOwner) {
            lifecycleScope.launch { adapter.submitData(it) }
        }
        screenState.observe(viewLifecycleOwner) { state ->
            binding.textSelectedDateRange.text = state?.dateRange?.let {
                formatDateRange(it)
            } ?: getString(R.string.inspection__date_all)
        }
        filterUpdated.observe(viewLifecycleOwner) {
            adapter.refresh()
        }
    }

    private fun showDateRangePicker() {
        val selectedRange = viewModel.screenState.value?.dateRange
        val dateRangePicker = MaterialDatePicker.Builder.dateRangePicker()
            .setTitleText(R.string.inspection__date_picker_title)
            .setSelection(
                AndroidxPair(
                    selectedRange?.first ?: MaterialDatePicker.thisMonthInUtcMilliseconds(),
                    selectedRange?.second ?: MaterialDatePicker.todayInUtcMilliseconds()
                )
            )
            .build()

        dateRangePicker.run {
            addOnNegativeButtonClickListener {
                viewModel.updateDateRange(null)
            }
            addOnPositiveButtonClickListener {
                viewModel.updateDateRange(Pair(it.first, it.second))
            }
            show(this@AllMedInspectionFragment.parentFragmentManager, null)
        }
    }

    // TODO обновить метод для отображения даты в формате из дизайна
    private fun formatDateRange(range: Pair<Long, Long>): String {
        val format = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())

        val from = format.format(Date(range.first))
        val to = format.format(Date(range.second))

        return getString(R.string.inspection__date_range, from, to)
    }

    companion object {

        fun prepareBundle() = bundleOf()
    }
}