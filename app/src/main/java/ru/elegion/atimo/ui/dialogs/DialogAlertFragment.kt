package ru.elegion.atimo.ui.dialogs

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.findNavController
import ru.elegion.atimo.databinding.DialogAlertBinding
import ru.elegion.atimo.util.Constants
import ru.elegion.atimo.util.setTextColorRes

class DialogAlertFragment : DialogFragment() {
    private var _viewBinding: DialogAlertBinding? = null
    private val binding get() = checkNotNull(_viewBinding)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _viewBinding = DialogAlertBinding.inflate(inflater)

        if (dialog != null && dialog!!.window != null) {
            dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog!!.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initClick()
        binding.apply {
            arguments?.let {
                val colorTittle = it.getInt(Constants.ALERT_TEXT_COLOR_TITTLE, -1)
                val colorButtonText = it.getInt(Constants.ALERT_TEXT_COLOR_BUTTON, -1)
                val tittle = it.getInt(Constants.ALERT_TITLE, -1)
                val msg = it.getString(Constants.ALERT_MSG)

                if(colorTittle != -1){
                    tvTittle.setTextColorRes(colorTittle)
                }
                if(colorButtonText != -1){
                    btnPositive.setTextColorRes(colorButtonText)
                }
                if(tittle != -1){
                    tvTittle.text = getText(tittle)
                }
                msg.let{
                    tvMessage.text = msg
                }
            }

        }
    }

    private fun initClick() {
        binding.apply {
            btnPositive.setOnClickListener {
                val requestKey = arguments?.getString(Constants.ALERT_REQUEST_KEY)
                requestKey?.let {key ->
                    parentFragmentManager.setFragmentResult(
                        key,
                        Bundle()
                    )
                }
                findNavController().popBackStack()
            }
            btnNegative.setOnClickListener {
                dismiss()
            }
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        _viewBinding = null
    }
}