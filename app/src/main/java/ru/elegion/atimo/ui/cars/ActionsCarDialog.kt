package ru.elegion.atimo.ui.cars

import android.view.LayoutInflater
import androidx.annotation.StringRes
import androidx.hilt.navigation.fragment.hiltNavGraphViewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.databinding.DialogActionsCarBinding
import ru.elegion.atimo.util.bottomsheet.BaseBottomSheetRoundedFragment

@AndroidEntryPoint
class ActionsCarDialog : BaseBottomSheetRoundedFragment<DialogActionsCarBinding>() {

    private val viewModel: CarsViewModel by hiltNavGraphViewModels(R.id.nav_graph)

    override fun initBinding(inflater: LayoutInflater): DialogActionsCarBinding {
        return DialogActionsCarBinding.inflate(inflater)
    }

    override fun initialize() {
        viewModel.selectedCar.observe(viewLifecycleOwner) {
            binding.apply {
                tvActionBlock.setText(it.vehicleStatus.blockText)
            }
        }
        binding.apply {
            imgClose.setOnClickListener { dismiss() }

            tvHistoryTechInspection.setOnClickListener {
                dismiss()
                viewModel.onClickHistory()
            }
            tvActionChange.setOnClickListener {
                dismiss()
                viewModel.onClickChanged()
            }
            tvActionBlock.setOnClickListener {
                findNavController().popBackStack()
                viewModel.onClickBlock()
            }
        }
    }


    private val Boolean.blockText
        @StringRes
        get() = if (this) {
            R.string.actions_unblock
        } else {
            R.string.actions_block
        }
}