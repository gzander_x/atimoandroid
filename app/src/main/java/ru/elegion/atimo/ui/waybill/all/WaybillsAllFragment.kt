package ru.elegion.atimo.ui.waybill.all

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.util.Pair
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.datepicker.MaterialDatePicker
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.waybills.Waybill
import ru.elegion.atimo.databinding.FragmentListWaybillsAllBinding
import ru.elegion.atimo.ui.adapters.base.BaseListsAdapterWithPaging
import ru.elegion.atimo.ui.base.BaseFragmentWithToolbarMenu
import ru.elegion.atimo.ui.waybill.WaybillDialog
import ru.elegion.atimo.ui.waybill.WaybillsViewModel
import ru.elegion.atimo.util.Constants
import ru.elegion.atimo.util.FilterPassedDialogBundleBuilder
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class WaybillsAllFragment : BaseFragmentWithToolbarMenu<FragmentListWaybillsAllBinding>() {

    override var menuRes: Int? = R.menu.filter_passed_menu
    override val actionsMenu: HashMap<Int, () -> Unit>?
        get() = hashMapOf(
            R.id.menuFilterDialog to {
                val bundle = FilterPassedDialogBundleBuilder.Builder()
                    .setState(filterState as? Boolean?)
                    .setButtonsText(R.array.filter_passed_tittle_buttons_v2).build().getBundle()
                findNavController().navigate(
                    R.id.filterPassedDialog, bundle
                )
            }
        )
    private lateinit var adapter: BaseListsAdapterWithPaging

    private val viewModel: WaybillsViewModel by viewModels()

    private var startSelection: Long? = null
    private var endSelection: Long? = null

    override var KEY_REQUEST_MENU: String? = Constants.BUNDLE_DIALOG_PASSED_VALUE
    override var filterResultAction: ((value: Any?) -> Unit)? = {
        filterState = it
        if (it is Boolean?) {
            changedIconMenu(it)
            viewModel.setPassed(it)
        }
    }

    override fun initBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentListWaybillsAllBinding = FragmentListWaybillsAllBinding.inflate(layoutInflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecycler()
        setObservers()
        setListeners()
        // TODO заменить на значение, которое будет доступно в пердыдущем фрагменте
        binding.textCntViews.text = getString(R.string.checkup_count, 300)
        binding.textDateFilter.text = getString(R.string.filter_date_msg_all_time)
    }

    private fun setListeners() {
        binding.viewSelectDate.setOnClickListener {
            showDateDialog()
        }
    }

    private fun showDateDialog() {
        val builder = MaterialDatePicker.Builder.dateRangePicker()
            .setTitleText(resources.getString(R.string.personal_account__dialog__tittle))
        if (startSelection != null && endSelection != null) {
            builder.setSelection(Pair(startSelection, endSelection))
        }
        val dateRangePicker = builder.build()
        dateRangePicker.show(parentFragmentManager, "datePicker")
        dateRangePicker.addOnPositiveButtonClickListener {
            startSelection = it.first
            endSelection = it.second
            val dateFormat = SimpleDateFormat("MM-dd", Locale.getDefault())
            val from = dateFormat.format(Date(it.first))
            val to = dateFormat.format(Date(it.second))
            binding.textDateFilter.text = "$from - $to"
            Timber.d("addOnPositiveButtonClickListener from = $from to = $to")
            viewModel.setDate(it.first, it.second)
        }
        dateRangePicker.addOnNegativeButtonClickListener {
            if (startSelection != null && endSelection != null) {
                binding.textDateFilter.text = getString(R.string.filter_date_msg_all_time)
                viewModel.setDateAll()
            }
        }
    }

    private fun setObservers() {
        viewModel.run {
            waybillsStream.observe(viewLifecycleOwner) {
                Timber.d("Отработала LiveData")
                lifecycleScope.launch { adapter.submitData(it) }
            }
            updateAdapter.observe(viewLifecycleOwner) {
                adapter.refresh()
            }
        }
    }

    private fun initRecycler() {
        adapter = BaseListsAdapterWithPaging().apply {
            listener = { fromAdapter ->
                val data = fromAdapter?.data as? Waybill
                data?.let {
                    navigate(R.id.waybillDialog, bundleOf(WaybillDialog.BUNDLE_WAYBILL to it))
                }
                Timber.e("$data")
            }
            addLoadStateListener { state ->
                showLoader(state.refresh == LoadState.Loading)
                if (state.refresh is LoadState.Error) {

                    retryDialogMessage {
                        viewModel.getNewStream()
                    }
                }
            }
        }
        binding.apply {
            recyclerWaybills.adapter = adapter
            recyclerWaybills.layoutManager = LinearLayoutManager(requireContext())
        }
    }
}
