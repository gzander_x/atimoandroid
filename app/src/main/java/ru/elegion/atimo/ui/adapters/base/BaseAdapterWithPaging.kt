package ru.elegion.atimo.ui.adapters.base

import androidx.recyclerview.widget.DiffUtil
import androidx.viewbinding.ViewBinding

abstract class BaseAdapterWithPaging<T : Any, V : ViewBinding>(
    diffCallback: DiffUtil.ItemCallback<T>
) : BasePagingAdapter<T, V>(diffCallback)
