package ru.elegion.atimo.ui.profile

import android.graphics.Color
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.NavOptions
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.adapter.ProfileModel
import ru.elegion.atimo.data.models.company.CompanyInfo
import ru.elegion.atimo.databinding.FragmentProfileBinding
import ru.elegion.atimo.repository.user.UserAuthStorage
import ru.elegion.atimo.ui.adapters.ProfileAdapter
import ru.elegion.atimo.ui.base.BaseFragmentWithToolbarMenu
import ru.elegion.atimo.util.AlertDialogBundleBuilder
import ru.elegion.atimo.util.Constants
import ru.elegion.atimo.util.Resource
import ru.elegion.atimo.util.extensions.toast
import javax.inject.Inject

@AndroidEntryPoint
class ProfileFragment : BaseFragmentWithToolbarMenu<FragmentProfileBinding>(){

    override var menuRes: Int? = R.menu.logout_menu

    override val actionsMenu: HashMap<Int, () -> Unit>?
        get() = hashMapOf(
            R.id.menuLogOut to {
                showLogOutDialog()
            }
        )
    private val viewModel: ProfileViewModel by viewModels()
    private lateinit var profileAdapter: ProfileAdapter

    @Inject
    lateinit var userAuthStorage: UserAuthStorage

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        setupObservers()
        initRecycler()
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentProfileBinding =
        FragmentProfileBinding.inflate(layoutInflater)

    private fun setListeners(){
        binding.run{
            btnProfileEdit.setOnClickListener {
                if(!profileAdapter.editState){
                    btnProfileEdit.setTextColor(Color.WHITE)
                    btnProfileEdit.background.setTint(requireContext().getColor(R.color.purple))
                    btnProfileEdit.text = getString(R.string.profile_save_btn)
                }else{
                    viewModel.updateCompanyInfo(profileAdapter.getCompanyInfo())
                    btnProfileEdit.setTextColor(Color.BLACK)
                    btnProfileEdit.background.setTint(requireContext().getColor(R.color.button_grey_bg))
                    btnProfileEdit.text = getString(R.string.profile_edit_btn)
                }
                profileAdapter.setState()
            }
        }
    }

    private fun setupObservers() = viewModel.run {
        navigateToSignIn.observe(viewLifecycleOwner) {
            val navOptions = NavOptions.Builder()
                .setPopUpTo(R.id.nav_graph, true)
                .build()
            navigate(R.id.loginFragment, navOption = navOptions)
        }
        successUpdateCompanyLiveData.observe(viewLifecycleOwner){
            showLoader(it is Resource.Loading)
            when(it){
                is Resource.Loading -> {}
                is Resource.Success -> {
                    if(it.data!!.success){
                        toast(requireContext(), "Обновлено")
                    }else{
                        toast(requireContext(), "Ошибка обновления")
                    }
                }
                is Resource.Error -> {
                    viewModel.rollbackCompanyInfo()
                    retryDialogMessage {
                        viewModel.updateCompanyInfo(profileAdapter.getCompanyInfo())
                    }
                }
            }
        }
    }

    private fun initRecycler() {
        val user = userAuthStorage.currentUser
        val items = listOf(
            ProfileModel(CompanyInfo.CompanyName, getString(R.string.reg_org_name), ""), //TODO Не приходит с сервера
            ProfileModel(CompanyInfo.CompanyInn, getString(R.string.inn), "", InputType.TYPE_CLASS_NUMBER), //TODO Не приходит с сервера
            ProfileModel(CompanyInfo.CompanyPhone, getString(R.string.phone), user.getPhoneForProfile()),
            ProfileModel(CompanyInfo.CompanyEmail, getString(R.string.email), user.email ?: "", InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS), //TODO Не приходит с сервера
            ProfileModel(CompanyInfo.CompanySecondName, getString(R.string.reg_lastname), user.secondName, InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS),
            ProfileModel(CompanyInfo.CompanyFirstName, getString(R.string.reg_name), user.firstName, InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS),
            ProfileModel(CompanyInfo.CompanyLastName, getString(R.string.reg_middle_name), user.lastName, InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS),
            ProfileModel(CompanyInfo.CompanyBik, "БИК", "", InputType.TYPE_CLASS_NUMBER), //TODO Не приходит с сервера
            ProfileModel(CompanyInfo.CompanyAccount,"Расчетный счёт", "", InputType.TYPE_CLASS_NUMBER) //TODO Не приходит с сервера
        )
        with(binding) {
            profileAdapter = ProfileAdapter().apply {
                setData(items)
            }
            listProfile.adapter = profileAdapter
        }
    }

    private fun showLogOutDialog() {
        val bundle = AlertDialogBundleBuilder.Builder()
            .setTitle(R.string.logout_dialog_title)
            .setMessage(getString(R.string.logout_dialog_message))
            .setTittleColorText(R.color.error_text)
            .setBtnColorText(R.color.error_text)
            .setRequestKey()
            .build()
            .getBundle()
        showAlertDialog(bundle, Constants.ALERT_REQUEST_KEY, viewModel::logOut)
    }
}