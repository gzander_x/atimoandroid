package ru.elegion.atimo.ui.reg

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.company.CompanyCreateModifyRes
import ru.elegion.atimo.data.models.company.CompanyInfoReq
import ru.elegion.atimo.repository.reg.RegRepository
import ru.elegion.atimo.util.EventLiveData
import ru.elegion.atimo.util.Resource
import javax.inject.Inject

@HiltViewModel
class RegViewModel @Inject constructor(
    private val repository: RegRepository
) : ViewModel() {

    private val _response: MutableLiveData<Resource<List<String>>> = MutableLiveData()
    val responseOrganization: LiveData<Resource<List<String>>> get() = _response

    private val _liveDataCompanyReg: MutableLiveData<Resource<CompanyCreateModifyRes>> = MutableLiveData()
    val liveDataCompanyReg: LiveData<Resource<CompanyCreateModifyRes>> = _liveDataCompanyReg

    val navigateToMap = EventLiveData<Unit>()

    fun getOrganizations() {
        viewModelScope.launch(Dispatchers.IO) {
            _response.postValue(Resource.Loading())
            try {
                repository.getOrganizations().let { organizations ->
                    _response.postValue(Resource.Success(organizations))
                }
            } catch (e: Exception) {
                _response.postValue(Resource.Error(R.string.test_error))
            }
        }
    }

    fun regCompany(companyInfo: CompanyInfoReq) {
        if (companyInfo.phone.isNotEmpty()){
            viewModelScope.launch(Dispatchers.IO){
                _liveDataCompanyReg.postValue(Resource.Loading())
                _liveDataCompanyReg.postValue(repository.createModifyCompany(companyInfo))
            }
        }
    }

    fun accountRegistered() {
        viewModelScope.launch {
            delay(300)
            navigateToMap.setValue(Unit)
        }
    }
}