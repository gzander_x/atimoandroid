package ru.elegion.atimo.ui.settings

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.fragment.app.viewModels
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.notifications.NotificationType
import ru.elegion.atimo.databinding.FragmentSettingsBinding
import ru.elegion.atimo.ui.base.BaseFragment
import ru.elegion.atimo.util.Resource

@AndroidEntryPoint
class SettingsFragment : BaseFragment<FragmentSettingsBinding>() {

    private val viewModel: SettingsViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupObservers()
        initViews()
    }

    override fun initBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentSettingsBinding.inflate(inflater, container, false)

    override fun initClicks() = binding.run {
        layoutNotificationWaybillEnd.switchPush.setOnClickListener {
            viewModel.updatePushNotificationsSetting(NotificationType.WAYBILL_END)
        }
        layoutNotificationBalanceUpdate.switchPush.setOnClickListener {
            viewModel.updatePushNotificationsSetting(NotificationType.BALANCE_UPDATE)
        }
        layoutNotificationDriverBlocked.switchPush.setOnClickListener {
            viewModel.updatePushNotificationsSetting(NotificationType.DRIVER_BLOCKED)
        }
        layoutGeolocation.setOnClickListener { openAppSettings() }
    }

    private fun initViews() = binding.run {
        layoutNotificationWaybillEnd.textTitle.setText(R.string.notification_type_waybill_end)
        layoutNotificationDriverBlocked.textTitle.setText(R.string.notification_type_driver_block)
        layoutNotificationBalanceUpdate.textTitle.setText(R.string.notification_type_balance_update)
    }

    private fun setupObservers() = viewModel.run {
        messageEvent.observe(viewLifecycleOwner, ::showMessage)
        displayedPushState.observe(viewLifecycleOwner, ::updatePushState)
        pushUpdateResource.observe(viewLifecycleOwner) { resource ->
            showLoader(resource is Resource.Loading)
        }
    }

    private fun openAppSettings() {
        val uri = Uri.fromParts("package", requireContext().packageName, null)
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, uri).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }

        intent.resolveActivity(requireActivity().packageManager)?.let {
            startActivity(intent)
        }
    }

    private fun updatePushState(state: Map<NotificationType, Boolean>) = binding.run {
        state.get(NotificationType.WAYBILL_END)
            ?.let(layoutNotificationWaybillEnd.switchPush::setChecked)
        state.get(NotificationType.BALANCE_UPDATE)
            ?.let(layoutNotificationBalanceUpdate.switchPush::setChecked)
        state.get(NotificationType.DRIVER_BLOCKED)
            ?.let(layoutNotificationDriverBlocked.switchPush::setChecked)
    }

    private fun showMessage(@StringRes messageRes: Int) =
        Snackbar.make(requireView(), messageRes, Snackbar.LENGTH_SHORT)
            .show()
}