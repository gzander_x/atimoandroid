package ru.elegion.atimo.ui.notifications

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.data.models.notifications.Notification
import ru.elegion.atimo.databinding.FragmentNotificationBinding
import ru.elegion.atimo.ui.MainActivity
import ru.elegion.atimo.ui.base.BaseFragment

@AndroidEntryPoint
class NotificationFragment: BaseFragment<FragmentNotificationBinding>() {

    override fun initBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentNotificationBinding = FragmentNotificationBinding.inflate(inflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val notification = arguments?.getParcelable(BUNDLE_NOTIFICATION) as? Notification
        notification?.let {
            (requireActivity() as? MainActivity)?.changeTittle(it.tittle?:"")
            binding.textMessage.text = it.message
            binding.textDate.text = it.date
        }
    }

    companion object{
        private const val BUNDLE_NOTIFICATION = "notification"
        fun prepareBundle(notification: Notification): Bundle{
            return bundleOf(BUNDLE_NOTIFICATION to notification)
        }
    }

}