package ru.elegion.atimo.ui.waybill

import android.view.LayoutInflater
import ru.elegion.atimo.databinding.DialogWaybillEmptyBinding
import ru.elegion.atimo.util.bottomsheet.BaseBottomSheetRoundedFragment

class WaybillEmptyDialog : BaseBottomSheetRoundedFragment<DialogWaybillEmptyBinding>() {

    override fun initialize() {
        setMaxHeight(1.0)
        binding.btnClose.setOnClickListener { closeSheet() }
    }

    override fun initBinding(inflater: LayoutInflater) = DialogWaybillEmptyBinding.inflate(inflater)
}
