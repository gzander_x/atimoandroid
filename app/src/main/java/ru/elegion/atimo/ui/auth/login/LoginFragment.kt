package ru.elegion.atimo.ui.auth.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.databinding.FragmentLoginBinding
import ru.elegion.atimo.ui.auth.code.CodeEnterFragment
import ru.elegion.atimo.ui.base.BaseFragment
import ru.elegion.atimo.ui.splash.SplashViewModel
import ru.elegion.atimo.util.MaskWatcher

@AndroidEntryPoint
class LoginFragment : BaseFragment<FragmentLoginBinding>() {

    private val viewModel: LoginViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setListeners()
        // setupObservers()
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentLoginBinding =
        FragmentLoginBinding.inflate(layoutInflater)

    private fun setListeners() {
        binding.editPhone.addTextChangedListener(MaskWatcher("### ###-##-##"))

        binding.editPhone.addTextChangedListener {
            binding.btnSend.isEnabled = it?.length == 13
        }

        binding.btnSend.setOnClickListener {
            val args = CodeEnterFragment.prepareBundle("+7 ${binding.editPhone.text}")
            findNavController().navigate(R.id.codeEnterFragment, args)
        }
    }

    private fun setupObservers() {
    }
}
