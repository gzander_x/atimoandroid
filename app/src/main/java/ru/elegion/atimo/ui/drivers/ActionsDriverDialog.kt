package ru.elegion.atimo.ui.drivers

import android.view.LayoutInflater
import androidx.hilt.navigation.fragment.hiltNavGraphViewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.databinding.DialogActionsDriversBinding
import ru.elegion.atimo.util.bottomsheet.BaseBottomSheetRoundedFragment
import timber.log.Timber

@AndroidEntryPoint
class ActionsDriverDialog: BaseBottomSheetRoundedFragment<DialogActionsDriversBinding>() {

    companion object{
        const val ARGUMENT_IS_BLOCK = "is block"
    }

    private val viewModel: DriversViewModel by hiltNavGraphViewModels(R.id.nav_graph)

    override fun initBinding(inflater: LayoutInflater): DialogActionsDriversBinding {
        return DialogActionsDriversBinding.inflate(inflater)
    }

    override fun initialize() {
        binding.imgClose.setOnClickListener { findNavController().popBackStack() }
        val textBlocking = arguments?.getInt(ARGUMENT_IS_BLOCK, 0) ?: 0
        binding.apply {
            tvActionWaybill.setOnClickListener {
                viewModel.onClickWaybill()
                findNavController().popBackStack()
            }
            tvActionMedical.setOnClickListener {
                viewModel.onClickMedical()
                findNavController().popBackStack()
            }
            tvActionChange.setOnClickListener {
                viewModel.onClickChanged()
                findNavController().popBackStack()
            }
            tvActionBlock.text = getString(
                if(textBlocking == 0){
                    R.string.actions_block
                }else{
                    R.string.actions_unblock
                }
            )
            tvActionBlock.setOnClickListener {
                viewModel.onClickBlock()
                findNavController().popBackStack()
            }

        }
    }
}