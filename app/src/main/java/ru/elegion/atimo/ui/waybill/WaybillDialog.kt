package ru.elegion.atimo.ui.waybill

import android.view.LayoutInflater
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.waybills.Waybill
import ru.elegion.atimo.databinding.DialogWaybillBinding
import ru.elegion.atimo.util.bottomsheet.BaseBottomSheetRoundedFragment

@AndroidEntryPoint
class WaybillDialog : BaseBottomSheetRoundedFragment<DialogWaybillBinding>() {

    companion object {
        const val BUNDLE_WAYBILL = "BUNDLE_WAYBILL"
    }

    override fun initBinding(inflater: LayoutInflater) = DialogWaybillBinding.inflate(inflater)

    override fun initialize() {
        initText()
        setMaxHeight(1.0)
    }

    private fun initText() {
        val waybill = arguments?.getParcelable(BUNDLE_WAYBILL) as? Waybill
        binding.apply {
            numberWaybill.textTittle.text = getString(R.string.text_num_waybill)
            numberWaybill.textValue.text = waybill?.number
            idKisArt.textTittle.text = getString(R.string.text_id_kis_art)
            idKisArt.textValue.text = waybill?.numberKA
            viewStatus.textTittle.text = getString(R.string.text_status)
            viewStatus.textValue.text = waybill?.status
            startTime.textTittle.text = getString(R.string.text_start_time)
            startTime.textValue.text = waybill?.dateBegin
            endTime.textTittle.text = getString(R.string.text_end_time)
            // TODO если дата окончания не заполнена, будет +12 часов от даты начала
            endTime.textValue.text = waybill?.dateEnd
        }
    }

    override fun initClicks() {
        binding.apply {
            btnClose.setOnClickListener { closeSheet() }
        }
    }
}
