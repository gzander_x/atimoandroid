package ru.elegion.atimo.ui.adapters.indication

import android.view.LayoutInflater
import android.view.ViewGroup
import ru.elegion.atimo.data.models.drivers.DriverMo
import ru.elegion.atimo.databinding.ItemInspectionBinding
import ru.elegion.atimo.ui.adapters.base.BaseAdapter

class IndicationMoAdapter : BaseAdapter<DriverMo, ItemInspectionBinding>() {
    override fun getBinding(inflater: LayoutInflater, parent: ViewGroup, viewType: Int) =
        ItemInspectionBinding.inflate(inflater, parent, false)

    override fun bindViewHolder(holder: ViewBindingHolder, data: DriverMo) {
        holder.binding{
            data.apply {

            }
        }
    }
}