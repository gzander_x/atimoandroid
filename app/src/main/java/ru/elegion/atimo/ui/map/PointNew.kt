package ru.elegion.atimo.ui.map

import com.yandex.runtime.bindings.Archive
import com.yandex.runtime.bindings.Serializable

class PointNew(var latitude: Double, var longitude: Double, var type: Int) :
    Serializable {

    override fun serialize(archive: Archive) {
        latitude = archive.add(latitude)
        longitude = archive.add(longitude)
        type = archive.add(type)
    }
}
