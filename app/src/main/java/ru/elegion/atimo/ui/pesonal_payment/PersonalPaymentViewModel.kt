package ru.elegion.atimo.ui.pesonal_payment

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import ru.elegion.atimo.data.models.personal_payment.PersonalPaymentInfo
import ru.elegion.atimo.util.EventLiveData
import javax.inject.Inject

@HiltViewModel
class PersonalPaymentViewModel @Inject constructor() : ViewModel() {

    val showDialogApplicationSend = EventLiveData<Boolean>()

    fun sendPersonalPayment(paymentInfo: PersonalPaymentInfo) {
        //TODO not yet implements server
        if(paymentInfo.isBecomeReleasePoint){
            showDialogApplicationSend.postValue(true)
        }else{
            showDialogApplicationSend.postValue(false)
        }
    }


}