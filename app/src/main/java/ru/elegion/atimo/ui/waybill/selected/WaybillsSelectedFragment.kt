package ru.elegion.atimo.ui.waybill.selected

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.waybills.Waybill
import ru.elegion.atimo.databinding.FragmentListWaybillsSelectedBinding
import ru.elegion.atimo.ui.adapters.base.BaseListsAdapterWithPaging
import ru.elegion.atimo.ui.base.BaseFragment
import ru.elegion.atimo.ui.waybill.WaybillDialog
import ru.elegion.atimo.ui.waybill.WaybillsViewModel
import timber.log.Timber

@AndroidEntryPoint
class WaybillsSelectedFragment: BaseFragment<FragmentListWaybillsSelectedBinding>() {

    companion object{
        const val BUNDLE_NAME_DRIVER = "BUNDLE_NAME_DRIVER"
        const val BUNDLE_PHONE = "BUNDLE_PHONE"
    }

    private lateinit var adapter: BaseListsAdapterWithPaging

    private val viewModel: WaybillsViewModel by viewModels()

    override fun initBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentListWaybillsSelectedBinding = FragmentListWaybillsSelectedBinding.inflate(layoutInflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.textTittle.text = arguments?.getString(BUNDLE_NAME_DRIVER) ?: ""
        initRecycler()
        setObservers()
    }

    private fun initRecycler() {
        adapter = BaseListsAdapterWithPaging().apply {
            listener = { fromAdapter ->
                val data = fromAdapter?.data as? Waybill
                data?.let {
                    navigate(R.id.waybillDialog, bundleOf(WaybillDialog.BUNDLE_WAYBILL to it))
                }
            }
            addLoadStateListener { state ->
                showLoader(state.refresh == LoadState.Loading)
                if(state.refresh is LoadState.Error){

                    retryDialogMessage {
                        viewModel.getNewStream()
                    }
                }
            }
        }
        binding.apply {
            recyclerWaybills.adapter = adapter
            recyclerWaybills.layoutManager = LinearLayoutManager(requireContext())

        }
    }

    private fun setObservers() {
        viewModel.run {
            waybillsStream.observe(viewLifecycleOwner){
                Timber.d("Отработала LiveData")
                lifecycleScope.launch { adapter.submitData(it) }

            }
            updateAdapter.observe(viewLifecycleOwner){
                adapter.refresh()
            }
        }
    }

}