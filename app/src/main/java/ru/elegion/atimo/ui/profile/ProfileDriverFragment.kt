package ru.elegion.atimo.ui.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.NavOptions
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.databinding.FragmentProfileDriverBinding
import ru.elegion.atimo.repository.user.UserAuthStorage
import ru.elegion.atimo.ui.base.BaseFragmentWithToolbarMenu
import ru.elegion.atimo.util.AlertDialogBundleBuilder
import ru.elegion.atimo.util.Constants
import ru.elegion.atimo.util.Resource
import javax.inject.Inject

@AndroidEntryPoint
class ProfileDriverFragment() : BaseFragmentWithToolbarMenu<FragmentProfileDriverBinding>() {

    @Inject
    lateinit var userAuthStorage: UserAuthStorage

    override var menuRes: Int? = R.menu.logout_menu

    override val actionsMenu: HashMap<Int, () -> Unit>?
        get() = hashMapOf(
            R.id.menuLogOut to {
                showLogOutDialog()
            }
        )

    private val viewModel: ProfileViewModel by viewModels()

    override fun initBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentProfileDriverBinding = FragmentProfileDriverBinding.inflate(inflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObservers()
        setListener()
        setInfo()
    }

    private fun setListener() = binding.run {
        btnQuit.setOnClickListener {
            showQuitDialog()
        }
    }

    private fun setInfo() {
        with(binding){
            val user = userAuthStorage.currentUser
            lastname.text = user.lastName
            name.text = user.firstName
            middleName.text = user.secondName
            numberVu.text = "" //TODO не приходит с сервера
            birthday.text = "" //TODO не приходит с сервера
            sex.text = "" //TODO не приходит с сервера
            phone.text = user.getPhoneForProfile()

            textOrganization.text = getString(R.string.your_organization, "") //TODO не приходит с сервера
        }
    }

    private fun setupObservers() = viewModel.run {
        navigateToSignIn.observe(viewLifecycleOwner) {
            val navOptions = NavOptions.Builder()
                .setPopUpTo(R.id.nav_graph, true)
                .build()
            navigate(R.id.loginFragment, navOption = navOptions)
        }
        isSuccessQuit.observe(viewLifecycleOwner){
            showLoader(it is Resource.Loading)
            when(it){
                is Resource.Error -> {
                    retryDialogMessage {
                        viewModel.quit()
                    }
                }
                is Resource.Loading -> {}
                is Resource.Success -> {
                    viewModel.logOut()
                }
            }
        }
    }

    private fun showQuitDialog() {
        val bundle = AlertDialogBundleBuilder.Builder()
            .setTitle(R.string.quit)
            .setMessage(getString(R.string.quit_organization, ""))
            .setTittleColorText(R.color.error_text)
            .setBtnColorText(R.color.error_text)
            .setRequestKey()
            .build()
            .getBundle()
        showAlertDialog(bundle, Constants.ALERT_REQUEST_KEY, viewModel::quit)
    }

    private fun showLogOutDialog() {
        val bundle = AlertDialogBundleBuilder.Builder()
            .setTitle(R.string.logout_dialog_title)
            .setMessage(getString(R.string.logout_dialog_message))
            .setTittleColorText(R.color.error_text)
            .setBtnColorText(R.color.error_text)
            .setRequestKey()
            .build()
            .getBundle()
        showAlertDialog(bundle, Constants.ALERT_REQUEST_KEY, viewModel::logOut)
    }

}