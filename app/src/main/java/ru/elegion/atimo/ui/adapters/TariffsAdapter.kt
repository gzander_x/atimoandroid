package ru.elegion.atimo.ui.adapters

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.rates.RateUI
import ru.elegion.atimo.databinding.ItemTariffBinding
import ru.elegion.atimo.ui.adapters.base.BaseAdapter

class TariffsAdapter: BaseAdapter<RateUI, ItemTariffBinding>() {

    var selectedTariff: RateUI? = null


    override fun getBinding(inflater: LayoutInflater, parent: ViewGroup, viewType: Int) =
        ItemTariffBinding.inflate(inflater, parent, false)

    @SuppressLint("NotifyDataSetChanged")
    fun setSelected(selected: RateUI){
        this.selectedTariff = selected
        notifyDataSetChanged()
    }

    override fun bindViewHolder(holder: ViewBindingHolder, data: RateUI) {
        holder.binding {
            data.apply {

                tariffCount.text = if(data.isPromo){
                    ""
                }else{
                    data.count.toString()

                }
                tariffDesc.text = if(data.isPromo){
                    holder.itemView.context.getString(R.string.promo)
                }else{
                    data.count.toString()
                    holder.itemView.context.getString(R.string.checkup)
                }

                if(selectedTariff == data){
                    tariffCount.background = if(data.isPromo) AppCompatResources.getDrawable(holder.itemView.context, R.drawable.ic_promo_selected) else null
                    tariffBg.visibility = View.VISIBLE
                    root.cardElevation = 0f
                    tariffCount.setTextColor(Color.WHITE)
                    tariffDesc.setTextColor(Color.WHITE)
                }else{
                    tariffCount.background = if(data.isPromo) AppCompatResources.getDrawable(holder.itemView.context, R.drawable.ic_promo) else null
                    tariffBg.visibility = View.GONE
                    root.cardElevation = root.resources.getDimension(R.dimen.margin_tariff_hor)
                    tariffCount.setTextColor(Color.BLACK)
                    tariffDesc.setTextColor(holder.itemView.context.getColor(if (data.isPromo) R.color.purple else R.color.black))
                }
            }

            val h = root.resources.getDimensionPixelSize(R.dimen.margin_tariff_hor)
            val v = root.resources.getDimensionPixelSize(R.dimen.margin_tariff_ver)

            val param = root.layoutParams as ViewGroup.MarginLayoutParams

            when (holder.absoluteAdapterPosition) {
                0 -> param.setMargins(h, v, v, v)
                1 -> param.setMargins(v, v, v, v)
                2 -> param.setMargins(v, v, h, v)
                3 -> param.setMargins(h, v, v, h)
                4 -> param.setMargins(v, v, v, h)
                5 -> param.setMargins(v, v, h, h)
            }

            root.layoutParams = param
        }
    }
}