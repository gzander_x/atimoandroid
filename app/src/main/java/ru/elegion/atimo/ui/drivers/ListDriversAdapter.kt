package ru.elegion.atimo.ui.drivers

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import ru.elegion.atimo.data.models.drivers.Driver
import ru.elegion.atimo.databinding.ItemDriverBinding
import ru.elegion.atimo.ui.adapters.base.BaseAdapter

class ListDriversAdapter(private val callback: Callback): BaseAdapter<Driver, ItemDriverBinding>() {

    override fun getBinding(
        inflater: LayoutInflater,
        parent: ViewGroup,
        viewType: Int
    ): ItemDriverBinding {
        return ItemDriverBinding.inflate(inflater)
    }

    override fun bindViewHolder(holder: ViewBindingHolder, data: Driver) {
        holder.binding.tvDriverName.text = "${data.lastName} ${data.firstName}"
        holder.binding.tvDriverNumber.text = data.getPhoneForList()
        holder.binding.imgLock.isVisible = data.driverStatus == 1
        holder.binding.imgMenu.setOnClickListener {
            callback.onClickMenu(data, holder.adapterPosition)
        }
    }

    override fun setData(data: List<Driver>) {
        super.setData(data)
    }

    interface Callback{
        fun onClickMenu(driver: Driver, position: Int)
    }
}