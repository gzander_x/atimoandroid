package ru.elegion.atimo.ui.drivers

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.drivers.Driver
import ru.elegion.atimo.repository.drivers.DriversRepository
import ru.elegion.atimo.util.EventLiveData
import ru.elegion.atimo.util.Resource
import ru.elegion.atimo.util.toTyped
import javax.inject.Inject

@HiltViewModel
class DriversViewModel @Inject constructor(
    private val driversRepository: DriversRepository
    ): ViewModel() {

    private var findJob: Job? = null
    private var listDrivers: MutableList<Driver> = mutableListOf()

    private val _liveDataDriversList: MutableLiveData<Resource<List<Driver>>> = MutableLiveData()
    val liveDataDriversList: LiveData<Resource<List<Driver>>> = _liveDataDriversList

    private var driverBackUp: Driver? = null
    private val _selectedDriver: MutableLiveData<Driver> = MutableLiveData()
    val liveDataSelectedDriver: LiveData<Driver> = _selectedDriver

    private val _liveDataClickSave: EventLiveData<Resource<Driver>> = EventLiveData()
    val liveDataClickSave: LiveData<Resource<Driver>> = _liveDataClickSave

    private val _liveDataClickBlock: EventLiveData<Resource<Driver>> = EventLiveData()
    val liveDataClickBlock: LiveData<Resource<Driver>> = _liveDataClickBlock

    val isVisiblePassed: Boolean
        get() = driversRepository.isBlockedDrivers

    val isStateFilterLiveData = MutableLiveData<Boolean?>()

    val showToastNotSearch = EventLiveData<Int>()
    val showBlockDialog = EventLiveData<Driver>()
    val navigateToChanged = EventLiveData<Driver>()
    val navigateToWaybill = EventLiveData<Driver>()
    val navigateToMedical = EventLiveData<Driver>()

    fun getListDrivers(){
        viewModelScope.launch(Dispatchers.IO){
            val responseDrivers = driversRepository.getDriversCash()
            listDrivers = responseDrivers.toMutableList()
            _liveDataDriversList.postValue(Resource.Success(responseDrivers))
        }
    }

    fun getDriversWithFilter(isBlocked: Boolean?){
        isStateFilterLiveData.value = isBlocked
        viewModelScope.launch(Dispatchers.IO){
            val responseDrivers = driversRepository.getDriversCashWithFilter(isBlocked)
            listDrivers = responseDrivers.toMutableList()
            _liveDataDriversList.postValue(Resource.Success(responseDrivers))
        }
    }

    fun findDrivers(name: String){
        if(findJob?.isActive == true){
            findJob?.cancel()
        }
        if (name.isNotBlank()){
            findJob = viewModelScope.launch(Dispatchers.IO){
                delay(300)
                val list = listDrivers.toMutableList().filter { ("${it.lastName} ${it.firstName}").lowercase().contains(name.lowercase()) }
                if(list.isNotEmpty()){
                    _liveDataDriversList.postValue(Resource.Success(list))
                }else{
                    showToastNotSearch.postValue(R.string.drivers__bad_find)
                    _liveDataDriversList.postValue(Resource.Success(listDrivers))
                }
            }
        }else{
            _liveDataDriversList.value = Resource.Success(listDrivers)
        }
    }

    fun setDriver(driver: Driver){
        driverBackUp = Driver(driver)
        _selectedDriver.value = driver
    }

    fun rollbackDriver() {
        if(driverBackUp != null){
            _selectedDriver.value?.let {
                val driver = it
                driver.rollback(driverBackUp!!)
            }
        }
    }

    fun onClickSave(driver: Driver){
        viewModelScope.launch(Dispatchers.IO){
            _liveDataClickSave.postValue( Resource.Loading())
            _liveDataClickSave.postValue( driversRepository.createModifyDriver(driver).toTyped(driver))
        }
    }

    fun onClickBlock(){
        _selectedDriver.value?.let {
            viewModelScope.launch(Dispatchers.IO){
                showBlockDialog.postValue(it)
            }
        }
    }

    fun onClickChanged(){
        _selectedDriver.value?.let {
            viewModelScope.launch(Dispatchers.IO){
                navigateToChanged.postValue(it)
            }
        }
    }

    fun onClickWaybill(){
        _selectedDriver.value?.let {
            viewModelScope.launch(Dispatchers.IO){
                navigateToWaybill.postValue(it)
            }
        }
    }

    fun onClickMedical(){
        _selectedDriver.value?.let {
            viewModelScope.launch(Dispatchers.IO){
                navigateToMedical.postValue(it)
            }
        }
    }

    fun blockDriver() {
        _selectedDriver.value?.let{
            val driver = it ////it.copy(driverStatus =  if(it.driverStatus > 0) 0 else 1)
            driver.driverStatus = if(driver.driverStatus > 0) 0 else 1
            viewModelScope.launch(Dispatchers.IO){
                _liveDataClickBlock.postValue(Resource.Loading())
                _liveDataClickBlock.postValue( driversRepository.createModifyDriver(driver).toTyped(driver))
            }
        }
    }

    fun deleteCarFromLocal(position: Int){
        listDrivers.removeAt(position)
        if(listDrivers.isEmpty()){
            _liveDataDriversList.postValue(Resource.Success(listDrivers))
        }
    }

    override fun onCleared() {
        super.onCleared()
        findJob?.cancel()
        findJob = null
        driverBackUp = null
    }

    fun localAdd(driver: Driver) {
        driversRepository.addDriver(driver)
        getListDrivers()
    }
}