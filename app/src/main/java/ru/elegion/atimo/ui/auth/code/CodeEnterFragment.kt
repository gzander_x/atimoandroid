package ru.elegion.atimo.ui.auth.code

import android.os.Bundle
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import androidx.navigation.NavOptions
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.auth.UserRes
import ru.elegion.atimo.data.network.model.UnsuccessfulResponseException
import ru.elegion.atimo.databinding.FragmentCodeEnterBinding
import ru.elegion.atimo.ui.MainActivity
import ru.elegion.atimo.ui.base.BaseFragment
import ru.elegion.atimo.ui.reg.RegFragment
import ru.elegion.atimo.util.Constants
import ru.elegion.atimo.util.Resource
import ru.elegion.atimo.util.setTextColorRes
import timber.log.Timber
import java.net.SocketException


@AndroidEntryPoint
class CodeEnterFragment : BaseFragment<FragmentCodeEnterBinding>() {

    private val viewModel: CodeEnterViewModel by viewModels()
    private var numberPhone: String? = null
    private var number: String? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setListeners()
        setupObservers()

        number = arguments?.getString(KEY_PHONE_NUMBER)

        numberPhone = number?.replace("-", "")
            ?.replace(" ", "")?.replace("+", "")

        sendCode()
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentCodeEnterBinding =
        FragmentCodeEnterBinding.inflate(layoutInflater)


    private fun setupObservers() {
        viewModel.apply {
            smsCodeStatus.observe(viewLifecycleOwner){
                when(it){
                    is Resource.Loading -> Timber.d("Загрузка")
                    is Resource.Success -> Snackbar.make(requireView(), "Код отправлен", Snackbar.LENGTH_LONG).show()
                    is Resource.Error -> Snackbar.make(requireView(), it.messages!!, Snackbar.LENGTH_LONG).show()
                }
            }
            phoneCodeStatus.observe(viewLifecycleOwner){
                when(it){
                    is Resource.Loading -> Timber.d("Загрузка")
                    is Resource.Success -> {
                        val message = when(it.data?.status){
                            Constants.SMS_RU_STATUS_CALL_ERROR -> {
                                it.data.statusText.toString()
                            }
                            Constants.SMS_RU_STATUS_CALL_OK -> {
                                "Звонок отправлен"
                            }
                            else ->{
                                 "Звонок не отправлен"
                            }
                        }
                        Snackbar.make(requireView(), message, Snackbar.LENGTH_LONG).show()
                    }
                    is Resource.Error -> Snackbar.make(requireView(), it.messages!!, Snackbar.LENGTH_LONG).show()
                }
            }
            codeCorrect.observe(viewLifecycleOwner){
                if(it){
                    val phone = numberPhone
                    viewModel.getUserData(phone)
                }else{
                    changingColorEditCode(isCorrect = false)
                }
            }
            userData.observe(viewLifecycleOwner) {
                showLoader(it is Resource.Loading)
                when(it) {
                    is Resource.Loading -> Timber.d("Загрузка данных пользователя")
                    is Resource.Success -> {
                        (requireActivity() as? MainActivity)?.initNavigationHeader()
                        val navOptions = NavOptions.Builder()
                            .setPopUpTo(R.id.nav_graph, true)
                            .build()
                        navigate(R.id.mapFragment, navOption = navOptions)
                    }
                    is Resource.Error -> handleUserError(it)
                }
            }
            isGetCodeSms.observe(viewLifecycleOwner){
                changeText(it)
            }
        }
    }

    private fun setListeners() {
        binding.timerCode.run {
            setOnChronometerTickListener {
                val elapsedMillis: Long = (base - SystemClock.elapsedRealtime())

                if(elapsedMillis <= 0)
                    setTimer(false)
            }
        }

        binding.editCode.addTextChangedListener {
            if(it?.length == 4){
                viewModel.isCodeCorrect(it.toString())
            }else{
                changingColorEditCode(isCorrect = true)
            }
        }

        binding.textCodeTime.setOnClickListener {
            sendCode()

        }

        binding.btnCodeCall.setOnClickListener {
            viewModel.changeTypeCode()
            sendCode()
        }
    }
    private fun changeText(isGetCodeSms: Boolean) {
        binding.apply {
            if(isGetCodeSms){
                btnCodeCall.text = getString(R.string.code_enter_call)
                number?.let {
                    textCodeDesc.text = getString(R.string.code_enter_desc, it)
                }
            }else{
                btnCodeCall.text = getString(R.string.code_enter_sms)
                textCodeDesc.text = getString(R.string.code_enter_call_desc)
            }
        }
    }

    private fun changingColorEditCode(isCorrect: Boolean){
        if(isCorrect){
            binding.editCode.setTextColorRes(R.color.black)
            binding.textError.visibility = View.INVISIBLE
        }else{
            binding.editCode.setTextColorRes(R.color.error_text)
            binding.textError.visibility = View.VISIBLE
        }

    }

    private fun sendCode(){
        numberPhone?.let {
            setTimer(true)
            viewModel.sendCode(it)
        }
    }

    private fun setTimer(start: Boolean){

        if(start) {
            binding.btnCodeCall.visibility = View.GONE

            binding.timerCode.run {
                visibility = View.VISIBLE
                base = SystemClock.elapsedRealtime() + 60000
                isCountDown = true
                start()
            }
        }else {
            binding.btnCodeCall.visibility = View.VISIBLE

            binding.timerCode.visibility = View.GONE
            binding.timerCode.stop()
        }

        binding.textCodeTime.run {
            if(start){
                text = if(viewModel.isGetCodeSms.value != false) getString(R.string.code_enter_time) else getString(R.string.code_enter_call_time)
                setTextColor(resources.getColor(R.color.text_grey, null))
                isClickable = false
            }else{
                text = getString(R.string.code_enter_resend)
                setTextColor(resources.getColor(R.color.button_bg, null))
                isClickable = true
            }
        }
    }

    // TODO доработать логику обработки ошибок
    private fun handleUserError(res: Resource.Error<UserRes>) = when (res.error) {
        is SocketException -> {
            binding.editCode.setText("")
            Snackbar.make(binding.root, res.messages!!, Snackbar.LENGTH_LONG).show()
        }
        is UnsuccessfulResponseException -> {
            if(res.error.statusCode == Constants.STATUS_NOT_REG){
                Toast.makeText(requireContext(), "Пользователь не найден", Toast.LENGTH_SHORT).show()
                val navOptions = NavOptions.Builder()
                    .setPopUpTo(R.id.nav_graph, true)
                    .build()
                navigate(R.id.regFragment, RegFragment.createBundle(numberPhone?:""),navOption = navOptions)
            }else{
                binding.editCode.setText("")
                Snackbar.make(
                    binding.root,
                    "Ошибка регистрации! ${res.error.message}",
                    Snackbar.LENGTH_SHORT
                ).show()
            }
        }
        else -> {
            binding.editCode.setText("")
            Snackbar.make(
                binding.root,
                "Ошибка регистрации! ${res.messages}",
                Snackbar.LENGTH_SHORT
            ).show()
        }
    }

    companion object {
        private const val KEY_PHONE_NUMBER = "phone_number"

        fun prepareBundle(phoneNumber: String) = bundleOf(
            KEY_PHONE_NUMBER to phoneNumber
        )
    }
}