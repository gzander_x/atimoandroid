package ru.elegion.atimo.ui.base

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.annotation.MenuRes
import androidx.core.content.ContextCompat
import androidx.viewbinding.ViewBinding
import ru.elegion.atimo.R
import ru.elegion.atimo.util.Constants

abstract class BaseFragmentWithToolbarMenu<B : ViewBinding>  : BaseFragment<B>() {

    private val menuWithFilter = listOf(R.menu.add_menu_with_passed, R.menu.filter_passed_menu)

    abstract var menuRes: Int?
        @MenuRes get
    /**
     * Действие по клику на иконку меню в тулбаре
     * K - @IdRes id элемента меню
     * V - действие по нажатие на элемент меню
     **/
    abstract val actionsMenu: Map<Int, (()->Unit)>?
    /**
     * Необходимо переопределить ключ, если нужно вернуть значение
     * Например:  Вернуть какое-то значение фильтра
     **/
    open var KEY_REQUEST_MENU: String? = null
    /**
     * Необходимо переопределить, если нужно выполнить действия в дочернем фрагменте по возращению
     **/
    open var filterResultAction: ((value:Any?)->Unit)? = null
    /**
     * Сохраненное состояние любого объекта
     * Например: Сохранить состояние фильтра
     **/
    open var filterState: Any? = null

    private var _menu: Menu? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        this._menu = menu
        menuRes?.let {
            requireActivity().menuInflater.inflate(menuRes!!, menu)
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        if(menuRes in menuWithFilter && menu.size() > 0){
            menu.findItem(R.id.menuFilterDialog).icon = if (filterState != null) ContextCompat.getDrawable(
                requireContext(),
                R.drawable.ic_menu_filter_selected
            ) else ContextCompat.getDrawable(requireContext(), R.drawable.ic_menu_filter)
        }
        super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val lambdaMenu: ( (actions: Map<Int, (()->Unit)>?) -> Boolean) = { actions ->
            if(actions?.containsKey(item.itemId) == true){
                actions[item.itemId]?.invoke()
                true
            }else{
                super.onOptionsItemSelected(item)
            }
        }
        return lambdaMenu.invoke(actionsMenu)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        parentFragmentManager.setFragmentResultListener(
            Constants.BUNDLE_MENU_VALUE_KEY_REQUEST,
            this
        ){ _, bundle ->
            filterResultAction?.invoke(bundle.getSerializable(KEY_REQUEST_MENU) as? Any)
        }
    }

    fun changedIconMenu(isPassed: Boolean?){
        _menu?.let {
            if(isPassed != null) {
                it.findItem(R.id.menuFilterDialog).icon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_menu_filter_selected)
            }else{
                it.findItem(R.id.menuFilterDialog).icon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_menu_filter)
            }
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        _menu = null
        menuRes = null
        KEY_REQUEST_MENU = null
        filterResultAction = null
        filterState = null
    }
}