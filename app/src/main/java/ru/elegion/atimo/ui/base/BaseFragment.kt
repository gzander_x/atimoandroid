package ru.elegion.atimo.ui.base

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import ru.elegion.atimo.R
import ru.elegion.atimo.ui.MainActivity
import ru.elegion.atimo.util.BundleBuilder
import ru.elegion.atimo.util.Constants

abstract class BaseFragment<B : ViewBinding> : Fragment() {

    companion object{
        private const val KEY_REQUEST_RETRY = "RetryActionFragment"
    }

    private var _viewBinding: B? = null
    protected val binding get() = checkNotNull(_viewBinding)

    private var actionMessageDialog: (()->Unit)? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _viewBinding = initBinding(inflater, container)
        initClicks()
        return binding.root
    }

    abstract fun initBinding(inflater: LayoutInflater, container: ViewGroup?): B
    protected open fun initClicks() {
    }

    fun navigate(@IdRes resId: Int, args: Bundle? = null, navOption: NavOptions? = null){
        findNavController().navigate(resId = resId, args = args, navOptions = navOption)
    }

    fun popBackStackWithDelay(){
        Handler(Looper.getMainLooper()).postDelayed({findNavController().popBackStack()},300)
    }

    fun showLoader(isShow: Boolean){
        val activity = requireActivity() as? MainActivity
        activity?.let {
            if(isShow){
                activity.showLoadingDialog()
            }else{
                activity.hideLoadingDialog()
            }
        }
    }

    fun retryDialogMessage(action:()->Unit){
        showMessageDialog(
            action,
            BundleBuilder.getAlertBundle(
                getString(R.string.message_dialog_tittle_error),
                getString(R.string.retry_message),
                getString(R.string.retry),
                Constants.ALERT_BTN_TYPE_POS_NEG,
                R.drawable.ic_error,
                KEY_REQUEST_RETRY
            ),
            KEY_REQUEST_RETRY
        )
    }
    /**
     * action:(()->Unit)? - действие которое произойдет если будет нажата кнопка
     * bundle: Bundle - набор параметров для построение диалога.
     *         P/S см. BundleBuilder что нужно передавать для построения MessageDiolog
     *         P/S чтобы action заработал необходимо передать в bundle в параметр ALERT_REQUEST_KEY такое же значение что и в key
     * key: String - ключ по которому parentFragmentManager примет результат от MessageDialog
     **/
    private fun showMessageDialog(action:(()->Unit)? = null, bundle: Bundle, key: String? = null){
        actionMessageDialog = action
        findNavController().navigate(
            R.id.messageDialog, bundle
        )
        if(key != null){
            setListenerDialog(key)
        }
    }

    /**
     * Вызов стандартного [showMessageDialog] с другим порядком параметров, чтобы лямбду можно
     * было вынести за скобки.
     */
    fun showMessageDialog(bundle: Bundle, key: String? = null, action: (() -> Unit)? = null) =
        showMessageDialog(action, bundle, key)

    /**
     * Принимает сигнал от MessageDialog и выполяется действие, которое мы передали
     **/
    private fun setListenerDialog(key: String){

        parentFragmentManager.setFragmentResultListener(
            key,
            this
        ){ _, _ ->
            actionMessageDialog?.invoke()
        }
    }
    fun showAlertDialog(action:(()->Unit)? = null, bundle: Bundle, key: String? = null){
        actionMessageDialog = action
        findNavController().navigate(
            R.id.dialogAlertFragment, bundle
        )
        if(key != null){
            setListenerDialog(key)
        }
    }

    /**
     * Вызов стандартного [showAlertDialog] с другим порядком параметров, чтобы лямбду можно
     * было вынести за скобки.
     */
    fun showAlertDialog(bundle: Bundle, key: String? = null, action: (() -> Unit)? = null) =
        showAlertDialog(action, bundle, key)

    override fun onDestroy() {
        super.onDestroy()
        actionMessageDialog = null
        _viewBinding = null
    }
}