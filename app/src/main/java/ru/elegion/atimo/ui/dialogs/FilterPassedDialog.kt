package ru.elegion.atimo.ui.dialogs

import android.view.LayoutInflater
import android.widget.RadioGroup
import androidx.core.os.bundleOf
import ru.elegion.atimo.R
import ru.elegion.atimo.databinding.DialogFilterPassedBinding
import ru.elegion.atimo.util.Constants
import ru.elegion.atimo.util.bottomsheet.BaseBottomSheetRoundedFragment
import timber.log.Timber

class FilterPassedDialog : BaseBottomSheetRoundedFragment<DialogFilterPassedBinding>() {

    private var passed: Boolean? = null

    private val checkedChangeListener = RadioGroup.OnCheckedChangeListener { _, checkId ->
        Timber.d("Нажата кнопка = $checkId")
        when(checkId){
            R.id.rbTru -> passed = true
            R.id.rbFalse -> passed = false
        }
    }

    override fun initBinding(inflater: LayoutInflater): DialogFilterPassedBinding =
        DialogFilterPassedBinding.inflate(layoutInflater)

    override fun initialize() {
        setData()
        setListeners()
    }


    private fun setData() {
        binding.run {
            val passedState = arguments?.getSerializable(Constants.BUNDLE_DIALOG_PASSED_STATE) as? Boolean
            passedState?.let {
                if(it){
                    binding.rbTru.isChecked = true
                }else{
                    binding.rbFalse.isChecked = true
                }
            }
            val stringResArray = arguments?.getInt(Constants.BUNDLE_DIALOG_PASSED_TEXT_BUTTONS)
            val stringArray = resources.getStringArray(stringResArray ?: -1)
            rbTru.text = stringArray.first()
            rbFalse.text = stringArray.last()
        }
    }

    private fun setListeners() {
        binding.run {
            textApply.setOnClickListener {
                sendValueInFragment(passed)
                dismiss()
            }
            textTittleDop.setOnClickListener {
                radioGroup.setOnCheckedChangeListener(null)
                radioGroup.clearCheck()
                passed = null
                radioGroup.setOnCheckedChangeListener(checkedChangeListener)
            }
            radioGroup.setOnCheckedChangeListener(checkedChangeListener)
        }
    }

    private fun sendValueInFragment(passed: Boolean?){
        parentFragmentManager.setFragmentResult(
            Constants.BUNDLE_MENU_VALUE_KEY_REQUEST,
            bundleOf(Constants.BUNDLE_DIALOG_PASSED_VALUE to passed)
        )
    }

}