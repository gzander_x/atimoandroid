package ru.elegion.atimo.ui.personal_account

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.databinding.FragmentPersonalAccountBinding
import ru.elegion.atimo.ui.base.BaseFragment
import ru.elegion.atimo.ui.cars.AddCarFragment
import ru.elegion.atimo.ui.drivers.AddDriverFragment
import ru.elegion.atimo.util.Constants
import ru.elegion.atimo.util.Resource
import timber.log.Timber

@AndroidEntryPoint
class PersonalAccountFragment: BaseFragment<FragmentPersonalAccountBinding>() {

    private val viewModel: PersonalAccountViewModel by viewModels()

    override fun initBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentPersonalAccountBinding = FragmentPersonalAccountBinding.inflate(inflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getInfo()
        setListener()
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.apply {
            liveDataLoader.observe(viewLifecycleOwner){
                when(it){
                    is Resource.Loading -> showLoader(true)
                    is Resource.Error -> {
                        showLoader(false)
                        retryDialogMessage {
                            viewModel.getInfo()
                        }
                    }
                    is Resource.Success -> showLoader(false)
                }
            }
            liveDataBalance.observe(viewLifecycleOwner){
                binding.tvViews.text = getString(R.string.personal_account__views, it)
            }
            liveDataCarsSize.observe(viewLifecycleOwner){
                binding.tvCarsSize.visibility = if(it>0) View.VISIBLE else View.INVISIBLE
                binding.tvCarsSize.text = it.toString()
            }
            liveDataDriversSize.observe(viewLifecycleOwner){
                binding.tvDriversSize.visibility = if(it>0) View.VISIBLE else View.INVISIBLE
                binding.tvDriversSize.text = it.toString()
            }
        }
    }


    private fun setListener() {
        binding.apply {
            btnTopUp.setOnClickListener {
                navigate(R.id.tariffsFragment)
            }
            containerListDrivers.setOnClickListener {
                navigate(R.id.listDriversFragment)
            }
            containerListCars.setOnClickListener {
                navigate(R.id.listCarsFragment)
            }
            containerHistoryOperation.setOnClickListener {
                navigate(R.id.operationsFragment)
            }
            containerContracts.setOnClickListener {
                navigate(R.id.contractsFragment)
            }
            btnAddDrivers.setOnClickListener {
                navigate(R.id.addDriverFragment, bundleOf(AddDriverFragment.EXTRA_TYPE_OPEN to Constants.TYPE_ADD))
            }
            btnAddCars.setOnClickListener {
                navigate(R.id.addCarFragment, bundleOf(AddCarFragment.EXTRA_TYPE_OPEN to Constants.TYPE_ADD))
            }
            btnCreateJournal.setOnClickListener {
                navigate(R.id.createJournalDialog)
            }
        }
    }

}