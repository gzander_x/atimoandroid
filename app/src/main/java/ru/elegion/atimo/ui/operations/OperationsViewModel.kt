package ru.elegion.atimo.ui.operations

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ru.elegion.atimo.data.models.operations.Operation
import ru.elegion.atimo.repository.operations.OperationsRepository
import ru.elegion.atimo.util.Resource
import javax.inject.Inject

@HiltViewModel
class OperationsViewModel @Inject constructor(
    private val operationsRepository: OperationsRepository
): ViewModel() {

    private val _liveDataOperations: MutableLiveData<Resource<List<Operation>>> = MutableLiveData()
    val liveDataOperations: LiveData<Resource<List<Operation>>> = _liveDataOperations

    private val _liveDataSendDoc: MutableLiveData<Resource<Unit>> = MutableLiveData()
    val liveDataSendDoc: MutableLiveData<Resource<Unit>> = _liveDataSendDoc
    init {
        getOperations()
    }

    private fun getOperations() {
        viewModelScope.launch(Dispatchers.IO){
            val responseOperations = operationsRepository.getOperation()
            _liveDataOperations.postValue(Resource.Success(responseOperations))
        }
    }

    fun sendDocByEmail(){
        //TODO отправить на бек запрос
        _liveDataSendDoc.value = Resource.Success(Unit)
    }

}