package ru.elegion.atimo.ui.adapters.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

abstract class BasePagingAdapter<T : Any, V : ViewBinding>(
    diffCallback: DiffUtil.ItemCallback<T>,
    mainDispatcher: CoroutineDispatcher = Dispatchers.Main,
    workerDispatcher: CoroutineDispatcher = Dispatchers.Default
) : PagingDataAdapter<T, BasePagingAdapter<T, V>.BindingHolder>(
    diffCallback = diffCallback,
    mainDispatcher = mainDispatcher,
    workerDispatcher = workerDispatcher
) {

    var listener: ((data: T?) -> Unit)? = null
    var listenerPosition: ((position: Int) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = getBinding(inflater, parent, viewType)
        return BindingHolder(binding)
    }

    override fun onBindViewHolder(holder: BindingHolder, position: Int) {
        val item = getItem(position)
        holder.apply {
            bindViewHolder(this, item)
            holder.itemView.setOnClickListener { listener?.invoke(item) }
        }
    }

    abstract fun getBinding(inflater: LayoutInflater, parent: ViewGroup, viewType: Int): V

    abstract fun bindViewHolder(holder: BindingHolder, data: T?)

    inner class BindingHolder(val binding: V) : RecyclerView.ViewHolder(binding.root)

    operator fun V.invoke(body: V.() -> Unit): V = this.apply(body)
}
