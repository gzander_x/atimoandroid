package ru.elegion.atimo.ui.howWork

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.databinding.FragmentHistoryBinding
import ru.elegion.atimo.databinding.FragmentHowWorkBinding
import ru.elegion.atimo.ui.base.BaseFragment

//TODO not yet implements
@AndroidEntryPoint
class HowWorkFragment : BaseFragment<FragmentHowWorkBinding>(){

    private val viewModel: HowWorkViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //setOnClickListeners()
        //setupObservers()
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentHowWorkBinding =
        FragmentHowWorkBinding.inflate(layoutInflater)

    private fun setOnClickListeners(){
    }

    private fun setupObservers(){
    }
}