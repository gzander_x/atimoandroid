package ru.elegion.atimo.ui.personal_account

import android.view.LayoutInflater
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.android.material.datepicker.MaterialDatePicker
import ru.elegion.atimo.R
import ru.elegion.atimo.databinding.DialogCreateJournalBinding
import ru.elegion.atimo.util.BundleBuilder
import ru.elegion.atimo.util.Constants
import ru.elegion.atimo.util.Resource
import ru.elegion.atimo.util.bottomsheet.BaseBottomSheetRoundedFragment
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

class CreateJournalDialog: BaseBottomSheetRoundedFragment<DialogCreateJournalBinding>() {

    companion object{
        private const val KEY_REQUEST_CREATE = "CreateJournal"
        private const val KEY_REQUEST_DOWNLOAD = "DownloadJournal"
    }

    private val viewModel: CreateJournalViewModel by viewModels()

    override fun initBinding(inflater: LayoutInflater): DialogCreateJournalBinding {
        return DialogCreateJournalBinding.inflate(inflater)
    }

    override fun initialize() {
        binding.imgClose.setOnClickListener {
            dismiss()
        }
        binding.btnSelectPeriod.setOnClickListener {
            showDateDialog()
        }
        setupObserves()
    }

    private fun setupObserves() {
        viewModel.apply {
            liveDataCreateJournal.observe(viewLifecycleOwner){
                when(it){
                    is Resource.Loading -> {showLoader(true)}
                    is Resource.Success -> {
                        showLoader(false)
                        showSuccessCreateJournal()
                    }
                    is Resource.Error -> {
                        showLoader(false)
                        retryDialogMessage{
                            viewModel.createJournal(true)
                        }
                    }
                }
            }
            liveDataDownload.observe(viewLifecycleOwner){
                when(it){
                    is Resource.Loading -> {showLoader(true)}
                    is Resource.Success -> {
                        showLoader(false)
                        showSuccessDownloadJournal()
                    }
                    is Resource.Error ->{
                        showLoader(false)
                        retryDialogMessage{
                            viewModel.downloadJournal(true)
                        }
                    }
                }
            }
        }
    }

    private fun showSuccessCreateJournal() {
        showMessageDialog(
            { viewModel.downloadJournal() },
            BundleBuilder.getAlertBundle(
                getString(R.string.create_journal_status__tittle_success),
                getString(R.string.select_action),
                getString(R.string.download),
                Constants.ALERT_BTN_TYPE_POS_NEG,
                R.drawable.ic_checkmark,
                KEY_REQUEST_CREATE
            ),
            KEY_REQUEST_CREATE
        )
    }

    private fun showSuccessDownloadJournal() {
        showMessageDialog(
            { Toast.makeText(requireContext(), "Открывается файл", Toast.LENGTH_LONG).show() },
            BundleBuilder.getAlertBundle(
                getString(R.string.create_journal_status__tittle_success),
                getString(R.string.select_action),
                getString(R.string.open),
                Constants.ALERT_BTN_TYPE_POS_NEG,
                R.drawable.ic_checkmark,
                KEY_REQUEST_DOWNLOAD
            ),
            KEY_REQUEST_DOWNLOAD
        )
    }

    private fun showDateDialog() {
        val dateRangePicker =
            MaterialDatePicker.Builder.dateRangePicker()
                .setTitleText(resources.getString(R.string.personal_account__dialog__tittle))
                .build()

        dateRangePicker.show(parentFragmentManager, "datePicker")
        dateRangePicker.addOnPositiveButtonClickListener {
            val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            val from = dateFormat.format(Date(it.first))
            val to = dateFormat.format(Date(it.second))
            Timber.d("addOnPositiveButtonClickListener from = $from to = $to")
            sendResult(from, to)
        }
    }

    private fun sendResult(dateFrom: String, dateTo: String){
        //TODO передавать и другие значения
        viewModel.createJournal()
    }

}