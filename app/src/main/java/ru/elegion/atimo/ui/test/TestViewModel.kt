package ru.elegion.atimo.ui.test

import androidx.lifecycle.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.Test
import ru.elegion.atimo.repository.test.TestRepository
import ru.elegion.atimo.util.Resource
import javax.inject.Inject

@HiltViewModel
class TestViewModel @Inject constructor(
    private val repository: TestRepository
) : ViewModel() {

    private val _response: MutableLiveData<Resource<Test>> = MutableLiveData()
    val response: LiveData<Resource<Test>> get() = _response

    fun getTestInfo() {
        viewModelScope.launch(Dispatchers.IO) {
            _response.postValue(Resource.Loading())
            try {
                repository.getTest().let { test ->
                    _response.postValue(Resource.Success(test))
                }
            } catch (e: Exception) {
                _response.postValue(Resource.Error(R.string.test_error))
            }
        }
    }
}