package ru.elegion.atimo.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import ru.elegion.atimo.data.models.contracts.Contract
import ru.elegion.atimo.data.models.operations.Operation
import ru.elegion.atimo.databinding.ItemOperationContractBinding
import ru.elegion.atimo.ui.adapters.base.BaseAdapter

class OperationsAndContactsAdapter: BaseAdapter<Any, ItemOperationContractBinding>() {
    override fun getBinding(
        inflater: LayoutInflater,
        parent: ViewGroup,
        viewType: Int
    ): ItemOperationContractBinding {
        return  ItemOperationContractBinding.inflate(inflater)
    }

    override fun bindViewHolder(holder: ViewBindingHolder, data: Any) {
        when(data){
            is Operation ->{ bindOperation(holder, data) }
            is Contract -> { bindContact(holder, data) }
        }
    }

    private fun bindOperation(holder: ViewBindingHolder, operation: Operation){
        holder.binding.apply {
            tvTitle.text = "Документ №${operation.numDoc}"
            tvSubtitle.text = operation.dateDoc
        }
    }
    private fun bindContact(holder: ViewBindingHolder, contract: Contract){
        holder.binding.apply{
            tvTitle.text = "Договор на ${contract.numDoc}"
            tvSubtitle.text = "${contract.dateDoc} · ${contract.formatFile} · ${contract.size}"
        }
    }

}