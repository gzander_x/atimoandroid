package ru.elegion.atimo.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.adapter.ProfileModel
import ru.elegion.atimo.data.models.company.CompanyInfo
import ru.elegion.atimo.data.models.company.CompanyInfoReq
import ru.elegion.atimo.databinding.ItemProfileBinding
import ru.elegion.atimo.ui.adapters.base.BaseAdapter

class ProfileAdapter: BaseAdapter<ProfileModel, ItemProfileBinding>() {
    var editState = false

    override fun getBinding(inflater: LayoutInflater, parent: ViewGroup, viewType: Int) =
        ItemProfileBinding.inflate(inflater, parent, false)

    override fun bindViewHolder(holder: ViewBindingHolder, data: ProfileModel) {
        holder.binding{
            data.apply {
                profileInput.hint = hint
                profileEdit.setText(text)
                profileEdit.inputType = inputType
                profileEdit.isEnabled = editState

                if(editState){
                  profileInput.boxStrokeWidth = root.resources.getDimensionPixelSize(R.dimen.input_box_stoke)
                }else{
                    profileInput.boxStrokeWidth = 0
                }
            }
            profileEdit.addTextChangedListener{
                items[holder.absoluteAdapterPosition].text = profileEdit.text.toString()
            }
        }
    }

    fun getCompanyInfo(): CompanyInfoReq{
        val info = CompanyInfoReq()
        for(item in items){
            val value = item.text
            when(item.typeObject){
                is CompanyInfo.CompanyInn -> info.companyInn = value
                is CompanyInfo.CompanyPhone -> info.phone = value
                    .replace("+","").replace("-", "").replace(" ", "")
                is CompanyInfo.CompanyEmail -> info.email = value
                is CompanyInfo.CompanyFirstName -> info.firstName = value
                is CompanyInfo.CompanySecondName -> info.secondName = value
                is CompanyInfo.CompanyLastName -> info.lastName = value
                else -> {}
            }
        }
        return info
    }

    fun setState(){
        editState = !editState
        notifyDataSetChanged()
    }
}