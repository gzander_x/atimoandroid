package ru.elegion.atimo.ui.inspection

import android.view.LayoutInflater
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.vehicles.TechInspectionRes
import ru.elegion.atimo.databinding.DialogInspectionBinding
import ru.elegion.atimo.util.bottomsheet.BaseBottomSheetRoundedFragment
import ru.elegion.atimo.util.extensions.changedDateToDisplay

@AndroidEntryPoint
class InspectionDialog : BaseBottomSheetRoundedFragment<DialogInspectionBinding>() {

    override fun initBinding(inflater: LayoutInflater) = DialogInspectionBinding.inflate(inflater)

    override fun initialize() {
        val to = arguments?.getParcelable(KEY_TO) as? TechInspectionRes
        to?.let(::renderScreenData)

        setMaxHeight(1.0)
    }

    override fun initClicks() = binding.run {
        buttonClose.setOnClickListener { dismiss() }
    }

    private fun renderScreenData(data: TechInspectionRes) = binding.run {
        textTitle.setText(R.string.inspection__to_title)
        textDate.text = data.date?.changedDateToDisplay()
        textStatus.text = data.status
        textOrganization.text = data.technicianCompany
        textInCharge.text = data.technicianFio
        renderReason(data.comment)
        textCertificate.text = getString(R.string.inspection__certificate_hash, data.technicianHash)
        textOwner.text = getString(R.string.inspection__certificate_owner, data.technicianFio)
        //TODO not yet implement (скоро должны прийти с сервера)
        textValidity.text = getString(R.string.inspection__certificate_valid_date, "с 06.08.2021 по 06.08.2022")
    }

    private fun renderReason(reason: String?) = binding.run {
        textReason.text = reason

        val hasReason = !reason.isNullOrBlank()
        textReason.isVisible = hasReason
        textLabelReason.isVisible = hasReason
        barrierReason.isVisible = hasReason
        dividerReason.isVisible = hasReason
    }

    companion object {

        private const val KEY_TO = "to"

        fun prepareBundle(to: TechInspectionRes) = bundleOf(
            KEY_TO to to
        )
    }
}