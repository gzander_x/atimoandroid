package ru.elegion.atimo.ui.info

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.databinding.FragmentHistoryBinding
import ru.elegion.atimo.databinding.FragmentInfoBinding
import ru.elegion.atimo.ui.base.BaseFragment
import ru.elegion.atimo.util.extensions.toast

@AndroidEntryPoint
class InfoFragment : BaseFragment<FragmentInfoBinding>(){


    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentInfoBinding =
        FragmentInfoBinding.inflate(layoutInflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
    }

    private fun setListeners() {
        binding.run {
            howWork.setOnClickListener {
                toast(requireContext(), "В разработке")
                navigate(R.id.howWorkFragment)
            }
            FAQ.setOnClickListener {
                toast(requireContext(), "В разработке")
                navigate(R.id.faqFragment)
            }
            legal.setOnClickListener {
                navigate(R.id.legalFragment)
            }
            aboutApp.setOnClickListener {
                navigate(R.id.aboutFragment)
            }
        }
    }

}