package ru.elegion.atimo.ui.cars

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.hilt.navigation.fragment.hiltNavGraphViewModels
import androidx.navigation.fragment.findNavController
import com.google.android.material.datepicker.MaterialDatePicker
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.vehicles.Vehicle
import ru.elegion.atimo.data.network.model.UnsuccessfulResponseException
import ru.elegion.atimo.databinding.FragmentAddCarBinding
import ru.elegion.atimo.repository.user.UserRepository
import ru.elegion.atimo.ui.MainActivity
import ru.elegion.atimo.ui.base.BaseFragment
import ru.elegion.atimo.util.BundleBuilder
import ru.elegion.atimo.util.Constants
import ru.elegion.atimo.util.Resource
import ru.elegion.atimo.util.extensions.changedDateToDisplay
import ru.elegion.atimo.util.extensions.changedDateToServer
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class AddCarFragment : BaseFragment<FragmentAddCarBinding>() {

    companion object {
        const val EXTRA_TYPE_OPEN = "type_open"
    }

    @Inject
    lateinit var userRepository: UserRepository

    private val viewModel: CarsViewModel by hiltNavGraphViewModels(R.id.nav_graph)
    private var typeOpen: Int = Constants.TYPE_ADD
    private var car: Vehicle? = null

    override fun initBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentAddCarBinding {
        return FragmentAddCarBinding.inflate(inflater)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        typeOpen = (arguments?.getInt(EXTRA_TYPE_OPEN, Constants.TYPE_ADD)) ?: Constants.TYPE_ADD
        changedDesign()
        setListener()
        setupObservers()
        setTypeMenu()
    }

    private fun changedDesign(){
        if(typeOpen == Constants.TYPE_CHANGE){
            (requireActivity() as? MainActivity)?.changeTittle(getString(R.string.actions_change))
        }else{
            (requireActivity() as? MainActivity)?.changeTittle(getString(R.string.cars__add_btn))
        }
    }

    private fun setupObservers() {
        viewModel.selectedCar.observe(viewLifecycleOwner) {
            initCarInfo(it)
        }
        viewModel.liveDataClickSave.observe(viewLifecycleOwner) {
            when (it) {
                is Resource.Loading -> showLoader(true)
                is Resource.Success -> {
                    showDialogSuccess(it.data!!)
                }
                is Resource.Error -> {
                    viewModel.rollbackCar()
                    retryDialogMessage { viewModel.onClickSave(getCarInfo()) }
                }
            }
            showLoader(false)
        }
    }

    private fun showAlreadyExistsDialog() = showMessageDialog(
        bundle = BundleBuilder.getAlertBundle(
            title = getString(R.string.car__dialog_already_exists_tittle),
            msg = getString(R.string.car__dialog_already_exists_msg),
            btnType = Constants.ALERT_BTN_TYPE_POS_NEG,
            srcImage = R.drawable.ic_error,
            posText = getString(R.string.drawer_menu_supp),
            requestKey = Constants.ALERT_REQUEST_KEY
        ),
        key = Constants.ALERT_REQUEST_KEY
    ) {
        //TODO переход на службу поддержки
    }

    private fun showDialogSuccess(data: Vehicle) = showMessageDialog(
        bundle = BundleBuilder.getAlertBundle(
            title = getString(R.string.car__dialog_success_tittle),
            msg = getString(R.string.car__dialog_success_msg, "${data.model}"),
            btnType = Constants.ALERT_BTN_TYPE_POS,
            srcImage = R.drawable.ic_checkmark,
            posText = getString(R.string.close),
            requestKey = Constants.ALERT_REQUEST_KEY,
            colorBtn = R.color.button_grey_bg,
            textColorBtn = R.color.black
        ),
        key = Constants.ALERT_REQUEST_KEY
    ) {
        findNavController().popBackStack()
    }

    private fun initCarInfo(car: Vehicle) {
        if (typeOpen == Constants.TYPE_CHANGE) {
            this.car = car
            binding.apply {
                editType.setText(car.vehicleType)
                editGrz.setText(car.vehicleGrz)
                editVin.setText(car.vehicleVin)
                editLicense.setText(car.vehicleLicense)
                editDateLicense.setText(car.vehicleLicenseDate.changedDateToDisplay())
                setTypeMenu()
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setListener() = binding.run {
        editDateLicense.setOnTouchListener { _, motionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_UP) {
                openDataPicker()
            }
            return@setOnTouchListener false
        }
        editType.addTextChangedListener {
            checkButtonSaveActive()
        }
        editGrz.addTextChangedListener {
            checkButtonSaveActive()
        }
        editVin.addTextChangedListener {
            checkButtonSaveActive()
        }
        editLicense.addTextChangedListener {
            checkButtonSaveActive()
        }
        editDateLicense.addTextChangedListener {
            checkButtonSaveActive()
        }
        btnSave.setOnClickListener {
            viewModel.onClickSave(getCarInfo())
        }
    }

    private fun getCarInfo(): Vehicle{
        if(typeOpen == Constants.TYPE_CHANGE){
            car?.apply {
                vehicleType = binding.editType.text.toString()
                vehicleGrz = binding.editGrz.text.toString()
                vehicleVin = binding.editVin.text.toString()
                vehicleLicense = binding.editLicense.text.toString()
                vehicleLicenseDate = binding.editDateLicense.text.toString().changedDateToServer()
            }
        }else{
            car = Vehicle(
                companyId = userRepository.currentUser.companyId,
                vehicleType = binding.editType.text.toString(),
                vehicleGrz = binding.editGrz.text.toString(),
                vehicleVin = binding.editVin.text.toString(),
                vehicleLicense = binding.editLicense.text.toString(),
                vehicleStatus = true,
                vehicleLicenseDate = binding.editDateLicense.text.toString().changedDateToServer(),
            )
        }
        return car!!
    }

    private fun setTypeMenu() {
        val adapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_dropdown_item_1line,
            listOf(Vehicle.VEHICLE_PASSENGER, Vehicle.VEHICLE_CARGO)
        )
        binding.editType.setAdapter(adapter)
    }

    private fun openDataPicker() {
        val datePicker =
            MaterialDatePicker.Builder.datePicker()
                .setTitleText(resources.getString(R.string.car__add_date_license))
                .build()
        datePicker.addOnPositiveButtonClickListener {
            val dateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())
            binding.editDateLicense.setText(dateFormat.format(Date(it)))
        }
        datePicker.show(parentFragmentManager, "date picker")
    }

    private fun checkButtonSaveActive() = binding.run {
        val type = editType.text.toString()
        val grz = editGrz.text.toString()
        val vin = editVin.text.toString()
        val license = editLicense.text.toString()
        val dateLicense = editDateLicense.text.toString()

        val checkedList = listOf(type, grz, vin, license, dateLicense)


        btnSave.isEnabled = !checkedList.any(String::isBlank)
    }
}