package ru.elegion.atimo.ui.auth.code

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.auth.UserRes
import ru.elegion.atimo.data.network.model.SmsRuCallRes
import ru.elegion.atimo.data.network.model.SmsRuSmsRes
import ru.elegion.atimo.repository.auth.AuthRepository
import ru.elegion.atimo.repository.user.UserRepository
import ru.elegion.atimo.util.Resource
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class CodeEnterViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val userRepository: UserRepository
) : ViewModel() {

    var smsCode: String? = null
        private set
    var phoneCode: String? = null
        private set

    private val _isGetCodeSms: MutableLiveData<Boolean> = MutableLiveData(true)
    val isGetCodeSms: LiveData<Boolean> = _isGetCodeSms

    private val _smsCodeStatus: MutableLiveData<Resource<SmsRuSmsRes>> = MutableLiveData()
    val smsCodeStatus: LiveData<Resource<SmsRuSmsRes>> = _smsCodeStatus
    private val _phoneCodeStatus: MutableLiveData<Resource<SmsRuCallRes>> = MutableLiveData()
    val phoneCodeStatus: LiveData<Resource<SmsRuCallRes>> = _phoneCodeStatus

    private val _codeCorrect: MutableLiveData<Boolean> = MutableLiveData()
    val codeCorrect: LiveData<Boolean> = _codeCorrect

    private val _userData = MutableLiveData<Resource<UserRes>>()
    val userData: LiveData<Resource<UserRes>>
        get() = _userData

    private fun sendSms(phoneNumber: String){
        viewModelScope.launch(Dispatchers.IO){
                _smsCodeStatus.postValue(Resource.Loading())
                smsCode = generateCode()
                val response = authRepository.sendSms(phoneNumber = phoneNumber, msg = smsCode!!)
                Timber.d("Пришло от sms.ru = data = ${response.data} error = ${response.error}" )
                _smsCodeStatus.postValue(response)
        }
    }

    private fun sendCall(phoneNumber: String){
        viewModelScope.launch(Dispatchers.IO) {
            try {
                _phoneCodeStatus.postValue(Resource.Loading())
                val response = authRepository.sendCall(phoneNumber)
                Timber.d("Пришло от sms.ru sendCall = data = ${response.data} error = ${response.error}" )
                if(response is Resource.Success){
                    phoneCode = response.data?.code
                }
                _phoneCodeStatus.postValue(response)
            }catch (e: Exception) {
                Timber.e("Ошикба sendCall = ${e.message}")
                _phoneCodeStatus.postValue(Resource.Error(messages = R.string.send_call_err))
            }
        }
    }

    private fun isSmsCodeCorrect(smsCode: String){
        _codeCorrect.value = smsCode == this.smsCode
    }

    private fun isPhoneCodeCorrect(phoneCode: String){
        _codeCorrect.value = phoneCode == this.phoneCode
    }

    fun isCodeCorrect(code: String){
        if(_isGetCodeSms.value!!){
            isSmsCodeCorrect(code)
        }else{
            isPhoneCodeCorrect(code)
        }
    }

    fun changeTypeCode(){
        _isGetCodeSms.value = !_isGetCodeSms.value!!
    }

    fun sendCode(phoneNumber: String){
        if(_isGetCodeSms.value!!){
            sendSms(phoneNumber)
        }else{
            sendCall(phoneNumber)
        }
    }

    fun getUserData(phone: String?) {
        if (phone == null) {
            _userData.value = Resource.Error(messages = R.string.sign_in_error)
            return
        }

        viewModelScope.launch {
            _userData.postValue( Resource.Loading())
            _userData.postValue(userRepository.getUser(phone))
        }
    }

    private fun generateCode(): String {
        return "${(0..9999).shuffled().first()}".padStart(4, '0').also {
            Timber.d("Сгенерированный код: $it")
        }
    }
}