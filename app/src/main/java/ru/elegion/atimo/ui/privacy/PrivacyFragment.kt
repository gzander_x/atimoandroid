package ru.elegion.atimo.ui.privacy

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.databinding.FragmentFaqBinding
import ru.elegion.atimo.databinding.FragmentHistoryBinding
import ru.elegion.atimo.databinding.FragmentHowWorkBinding
import ru.elegion.atimo.databinding.FragmentPrivacyBinding
import ru.elegion.atimo.ui.base.BaseFragment

@AndroidEntryPoint
class PrivacyFragment : BaseFragment<FragmentPrivacyBinding>(){

    private val viewModel: PrivacyViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //setOnClickListeners()
        //setupObservers()
    }

    override fun initBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentPrivacyBinding =
        FragmentPrivacyBinding.inflate(layoutInflater)

    private fun setOnClickListeners(){
    }

    private fun setupObservers(){
    }
}