package ru.elegion.atimo.ui.drivers

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.hilt.navigation.fragment.hiltNavGraphViewModels
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.google.android.material.datepicker.MaterialDatePicker
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.drivers.Driver
import ru.elegion.atimo.data.network.model.UnsuccessfulResponseException
import ru.elegion.atimo.databinding.FragmentAddDriverBinding
import ru.elegion.atimo.repository.user.UserRepository
import ru.elegion.atimo.ui.MainActivity
import ru.elegion.atimo.ui.base.BaseFragment
import ru.elegion.atimo.util.*
import ru.elegion.atimo.util.MaskWatcher
import ru.elegion.atimo.util.extensions.changedDateToServer
import ru.elegion.atimo.util.extensions.toast
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class AddDriverFragment: BaseFragment<FragmentAddDriverBinding>() {

    companion object{
        const val EXTRA_TYPE_OPEN = "type_open"
    }

    @Inject lateinit var userRepository: UserRepository

    private var typeOpen: Int = Constants.TYPE_ADD
    private var driver: Driver? = null
    private val viewModel: DriversViewModel by hiltNavGraphViewModels(R.id.nav_graph)

    private val requestCameraPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted: Boolean ->
            if (isGranted) {
                takePhotoFromCameraLauncher.launch(Unit)
            }
        }

    private val requestPhotoStoragePermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted: Boolean ->
            if (isGranted) {
                pickPhotoLauncher.launch(Unit)
            }
        }

    private val takePhotoFromCameraLauncher = registerForActivityResult(TakePhoto()) {
        Timber.d("Фото с камеры $it")
        addAvatar(it)
    }


    private val pickPhotoLauncher = registerForActivityResult(PickPhotoGallery()) { uri: Uri? ->
        Timber.d("Фото из галлереи $uri")
        addAvatar(uri)
    }

    override fun initBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentAddDriverBinding {
        return FragmentAddDriverBinding.inflate(inflater)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        typeOpen = (arguments?.getInt(EXTRA_TYPE_OPEN, Constants.TYPE_ADD))?:Constants.TYPE_ADD
        changedDesign()
        setGenderMenu()
        setListener()
        setupObservers()
    }

    private fun changedDesign(){
        if(typeOpen == Constants.TYPE_CHANGE){
            (requireActivity() as? MainActivity)?.changeTittle(getString(R.string.actions_change))
            with(binding){
                divider.isVisible = false
                switchAddSelf.isVisible = false
                tvDescSwitcher.isVisible = false
                textAddDataDrivers.isVisible = false
                tvNecessarily.isVisible = false
            }
        }else{
            (requireActivity() as? MainActivity)?.changeTittle(getString(R.string.drivers__add_btn))
        }
    }

    private fun setupObservers() {
        viewModel.liveDataSelectedDriver.observe(viewLifecycleOwner){
            initDriverInfo(it)
        }
        viewModel.liveDataClickSave.observe(viewLifecycleOwner){
            when(it){
                is Resource.Loading -> showLoader(true)
                is Resource.Success -> {
                    if(typeOpen == Constants.TYPE_ADD){
                        viewModel.localAdd(getDriverInfo())
                    }
                    showDialogSuccess(it.data!!)
                }
                is Resource.Error -> {
                    viewModel.rollbackDriver()
                    retryDialogMessage { viewModel.onClickSave(getDriverInfo()) }
                }
            }
            showLoader(false)
        }
    }

    private fun showAlreadyExistsDialog() {
        showMessageDialog(
            bundle = BundleBuilder.getAlertBundle(
                title = getString(R.string.driver__dialog_already_exists_tittle),
                msg = getString(R.string.driver__dialog_already_exists_msg),
                btnType = Constants.ALERT_BTN_TYPE_POS_NEG,
                srcImage = R.drawable.ic_error,
                posText = getString(R.string.drawer_menu_supp),
                requestKey = Constants.ALERT_REQUEST_KEY
            ),
            key = Constants.ALERT_REQUEST_KEY
        ) {
            toast(requireContext(), "Переход в службу поддержки")
        }
    }

    private fun showDialogSuccess(driver: Driver) {
        showMessageDialog(
            bundle = BundleBuilder.getAlertBundle(
                title = getString(if(typeOpen == Constants.TYPE_ADD) R.string.driver__dialog_success_tittle else R.string.driver__dialog_success_tittle_2),
                msg = getString(R.string.driver__dialog_success_msg, "${driver.lastName} ${driver.firstName} ${driver.secondName}"),
                btnType = Constants.ALERT_BTN_TYPE_POS,
                srcImage = R.drawable.ic_checkmark,
                posText = getString(R.string.close),
                requestKey = Constants.ALERT_REQUEST_KEY,
                colorBtn = R.color.button_grey_bg,
                textColorBtn = R.color.black
            ),
            key = Constants.ALERT_REQUEST_KEY
        ) {
            findNavController().popBackStack()
        }
    }

    private fun initDriverInfo(driver: Driver) {
        if(typeOpen == Constants.TYPE_CHANGE){
            this.driver = driver
            binding.apply {
                editSurname.setText(driver.secondName)
                editFirstName.setText(driver.firstName)
                editMiddleName.setText(driver.lastName)
                editNumDriverLicense.setText(driver.driverLicense)
                editPhone.setText(driver.getPhoneForModify())
                editBirthday.setText(driver.getDate())
                editGender.setText(driver.getSex())
                setGenderMenu()
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setListener() {
        binding.apply {
            switchAddSelf.setOnCheckedChangeListener { _, isChecked ->
                binding.editFirstName.setText(if (isChecked) userRepository.currentUser.firstName else "")
                binding.editSurname.setText(if (isChecked) userRepository.currentUser.secondName else "")
                binding.editMiddleName.setText(if (isChecked) userRepository.currentUser.lastName else "")
            }
            editNumDriverLicense.addTextChangedListener(MaskWatcher("#### ######"))
            editPhone.addTextChangedListener(MaskWatcher("### ###-##-##"))
            editBirthday.setOnTouchListener { _, motionEvent ->
                if(motionEvent.action == MotionEvent.ACTION_UP){
                    openDataPicker()
                }
                return@setOnTouchListener false
            }

            editSurname.addTextChangedListener {
                checkButtonSaveActive()
            }
            editFirstName.addTextChangedListener {
                checkButtonSaveActive()
            }
            editMiddleName.addTextChangedListener {
                tvMiddleNameHint.isVisible = it?.length == 0
            }
            editNumDriverLicense.addTextChangedListener {
                checkButtonSaveActive()
            }
            editBirthday.addTextChangedListener {
                checkButtonSaveActive()
            }
            editGender.addTextChangedListener {
                checkButtonSaveActive()
            }
            editPhone.addTextChangedListener {
                checkButtonSaveActive()
            }

            tvChangePhoto.setOnClickListener {
                navigate(R.id.actionPhotoFragment)
            }
            btnSave.setOnClickListener {
                viewModel.onClickSave(getDriverInfo())
            }
        }

        parentFragmentManager.setFragmentResultListener(
            ActionPhotoFragment.KEY_REQUEST_ACTION_PHOTO,
            this
        ) { _, result ->
            val typeAction = result.getInt(ActionPhotoFragment.EXTRA_TYPE_ACTION_PHOTO)
            if(typeAction == Constants.TYPE_TAKE_PHOTO){
                requestCameraPermission()
            }else{
                requestPhotoStoragePermission()
            }
        }
    }

    private fun getDriverInfo(): Driver {
        if(typeOpen == Constants.TYPE_CHANGE){
            driver?.firstName = binding.editFirstName.text.toString()
            driver?.secondName = binding.editSurname.text.toString()
            driver?.lastName = binding.editMiddleName.text.toString()
            driver?.driverLicense = binding.editNumDriverLicense.text.toString()
            driver?.setDate(binding.editBirthday.text.toString())
            driver?.setSex(binding.editGender.text.toString())
            driver?.phone = "+7" + binding.editPhone.text.toString()
                .replace("-", "")
                .replace(" ", "")
                .replace("+", "")
        }else{
            driver = Driver(
                firstName = binding.editFirstName.text.toString(),
                secondName = binding.editSurname.text.toString(),
                lastName = binding.editMiddleName.text.toString(),
                birthdate = binding.editBirthday.text.toString().changedDateToServer(),
                companyId = userRepository.currentUser.companyId,
                driverLicense = binding.editNumDriverLicense.text.toString(),
                phone = "+7" + binding.editPhone.text.toString()
                    .replace("-", "")
                    .replace(" ", ""),
                driverStatus = 0,
                sex = Driver.parserGender(binding.editGender.text.toString())
            )
        }
        return driver!!
    }


    private fun addAvatar(photo: Uri?){
        Glide.with(requireContext())
            .load(photo) // Uri of the picture
            .circleCrop()
            .error(ContextCompat.getDrawable(requireContext(), R.drawable.ic_no_photo))
        .into(binding.imgPhoto)
    }

    private fun requestCameraPermission() {
        when (PackageManager.PERMISSION_GRANTED) {
            ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.CAMERA
            ) -> {
                takePhotoFromCameraLauncher.launch(Unit)
            }
            else -> requestCameraPermissionLauncher.launch(Manifest.permission.CAMERA)
        }
    }

    private fun requestPhotoStoragePermission() {
        when (PackageManager.PERMISSION_GRANTED) {
            ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) -> pickPhotoLauncher.launch(Unit)
            else -> requestPhotoStoragePermissionLauncher.launch(Manifest.permission.READ_EXTERNAL_STORAGE)
        }
    }

    private fun openDataPicker() {
        val datePicker =
            MaterialDatePicker.Builder.datePicker()
                .setTitleText(resources.getString(R.string.driver__add_birthday_tittle))
                .build()
        datePicker.addOnPositiveButtonClickListener {
            val dateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())
            binding.editBirthday.setText(dateFormat.format(Date(it)))
        }
        datePicker.show(parentFragmentManager, "date picker")
    }

    private fun setGenderMenu(){
        val adapter = ArrayAdapter(requireContext(), android.R.layout.simple_dropdown_item_1line, listOf(Driver.GENDER_MALE, Driver.GENDER_FEMALE))
        binding.editGender.setAdapter(adapter)
    }

    private fun checkButtonSaveActive(){
        binding.apply {
            val surname = editSurname.text.toString()
            val name = editFirstName.text.toString()
            //val middleName = editMiddleName.text.toString()
            val numDriverLicense = editNumDriverLicense.text.toString()
            val birthday = editBirthday.text.toString()
            val gender = editGender.text.toString()
            val phone = editPhone.text.toString()
            btnSave.isEnabled = !listOf(surname, name, /*middleName,*/ numDriverLicense, birthday, gender, phone).any(String::isBlank)
        }
    }

}