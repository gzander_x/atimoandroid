package ru.elegion.atimo.ui.personal_account

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.elegion.atimo.R
import ru.elegion.atimo.util.Resource
import javax.inject.Inject

@HiltViewModel
class CreateJournalViewModel @Inject constructor() : ViewModel() {

    private val _liveDataCreateJournal: MutableLiveData<Resource<Unit>> = MutableLiveData()
    val liveDataCreateJournal: MutableLiveData<Resource<Unit>> = _liveDataCreateJournal

    private val _liveDataDownload: MutableLiveData<Resource<Unit>> = MutableLiveData()
    val liveDataDownload: MutableLiveData<Resource<Unit>> = _liveDataDownload

    fun createJournal(test: Boolean = false){
        viewModelScope.launch(Dispatchers.IO) {
            _liveDataCreateJournal.postValue(Resource.Loading())
            delay(5000)
            if (test){
                _liveDataCreateJournal.postValue(Resource.Success(Unit))
            }else {
                _liveDataCreateJournal.postValue(Resource.Error(R.string.message_dialog_tittle_error))
            }
        }
    }

    fun downloadJournal(test: Boolean = false){
        viewModelScope.launch(Dispatchers.IO) {
            _liveDataDownload.postValue(Resource.Loading())
            delay(5000)
            if (test){
                _liveDataDownload.postValue(Resource.Success(Unit))
            }else{
                _liveDataDownload.postValue(Resource.Error(R.string.message_dialog_tittle_error))
            }
        }
    }

}