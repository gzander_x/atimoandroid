package ru.elegion.atimo.ui.contracts

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ru.elegion.atimo.data.models.contracts.Contract
import ru.elegion.atimo.data.models.operations.Operation
import ru.elegion.atimo.repository.contracts.ContractsRepository
import ru.elegion.atimo.repository.operations.OperationsRepository
import ru.elegion.atimo.util.Resource
import javax.inject.Inject

@HiltViewModel
class ContractsViewModel @Inject constructor(
    private val contractsRepository: ContractsRepository
): ViewModel() {

    private val _liveDataContracts: MutableLiveData<Resource<List<Contract>>> = MutableLiveData()
    val liveDataContracts: LiveData<Resource<List<Contract>>> = _liveDataContracts

    init {
        getContracts()
    }

    private fun getContracts() {
        viewModelScope.launch(Dispatchers.IO){
            val responseOperations = contractsRepository.getContacts()
            _liveDataContracts.postValue(Resource.Success(responseOperations))
        }
    }

}