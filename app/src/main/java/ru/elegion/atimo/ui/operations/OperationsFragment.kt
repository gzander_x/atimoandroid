package ru.elegion.atimo.ui.operations

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.operations.Operation
import ru.elegion.atimo.databinding.FragmentOperationsBinding
import ru.elegion.atimo.databinding.LayoutEmptyListBinding
import ru.elegion.atimo.ui.adapters.OperationsAndContactsAdapter
import ru.elegion.atimo.ui.base.BaseFragment
import ru.elegion.atimo.ui.base.BaseFragmentWithEmptyList
import ru.elegion.atimo.util.Resource
import timber.log.Timber

@AndroidEntryPoint
class OperationsFragment :  BaseFragmentWithEmptyList<FragmentOperationsBinding>() {

    override val title
        @StringRes get() = R.string.empty_title
    override val subTitle: Int
        @StringRes get() = R.string.contracts__empty_subtitle
    override val btnTittle: Int
        @StringRes get() = R.string.contracts__empty_btn
    override val imgSrc: Int
        @DrawableRes get() = R.drawable.ic_empty_contracts
    override val navigateId: Int = R.id.tariffsFragment
    override var menuRes: Int? = null
    override val actionsMenu: Map<Int, () -> Unit>? = null


    private val viewModel: OperationsViewModel by viewModels()

    private lateinit var adapter: OperationsAndContactsAdapter

    override fun initBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentOperationsBinding {
        return FragmentOperationsBinding.inflate(inflater)
    }

    override fun initEmptyLayout(): LayoutEmptyListBinding {
        return binding.layoutEmptyList
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecycler()
        setupObservers()
    }

    private fun initRecycler() {
        adapter = OperationsAndContactsAdapter().apply {
            listener = {
                val operation = it as? Operation
                Timber.d("Нажана на операцию $operation")
                operation?.let { operation ->
                    navigate(R.id.operationDialog, bundleOf(OperationDialog.EXTRA_OPERATION to operation))
                }
            }
        }
        binding.apply {
            recyclerOperations.adapter = adapter
            recyclerOperations.layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun setupObservers() {
        viewModel.apply {
            liveDataOperations.observe(viewLifecycleOwner){
                when(it){
                    is Resource.Loading -> {}
                    is Resource.Success -> {
                        checkEmpty(it.data)
                        it.data?.let { list ->
                            adapter.setData(list)
                        }
                    }
                    is Resource.Error -> {}
                }
            }
        }
    }

}