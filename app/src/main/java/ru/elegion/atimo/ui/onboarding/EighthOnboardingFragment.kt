package ru.elegion.atimo.ui.onboarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.onboarding.OnboardingSegment
import ru.elegion.atimo.databinding.*
import ru.elegion.atimo.ui.base.BaseFragment
import ru.elegion.atimo.ui.base.BaseOnboardingFragment

@AndroidEntryPoint
class EighthOnboardingFragment: BaseOnboardingFragment<FragmentOnboarding8Binding>() {
    override val segment: OnboardingSegment = OnboardingSegment.EIGHTH
    override val msgFooter: Int
        get() = R.string.onboarding_screen_8_msg
    override val imgFooter: Int
        get() = R.drawable.ic_support

    override val actionBtnNext: (() -> Unit)?
        get() = null
    override fun initBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentOnboarding8Binding = FragmentOnboarding8Binding.inflate(inflater)

    override fun initHeader(): ItemOnboardingHeaderBinding = binding.layoutHeader

    override fun initFooter(): ItemOnboardingFooterBinding = binding.layoutFooter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindingFooter?.run {
            btnNext.isInvisible = true
            btnReg.isVisible = true
            btnReg.setOnClickListener {
                navigate(
                    resId = R.id.loginFragment,
                    navOption = NavOptions.Builder()
                        .setPopUpTo(R.id.nav_graph, true)
                        .build()
                )
            }
        }
    }

}