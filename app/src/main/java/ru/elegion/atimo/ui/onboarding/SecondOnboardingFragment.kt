package ru.elegion.atimo.ui.onboarding

import android.view.LayoutInflater
import android.view.ViewGroup
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.onboarding.OnboardingSegment
import ru.elegion.atimo.databinding.FragmentOnboarding1Binding
import ru.elegion.atimo.databinding.FragmentOnboarding2Binding
import ru.elegion.atimo.databinding.ItemOnboardingFooterBinding
import ru.elegion.atimo.databinding.ItemOnboardingHeaderBinding
import ru.elegion.atimo.ui.base.BaseFragment
import ru.elegion.atimo.ui.base.BaseOnboardingFragment

@AndroidEntryPoint
class SecondOnboardingFragment: BaseOnboardingFragment<FragmentOnboarding2Binding>() {
    override val segment: OnboardingSegment = OnboardingSegment.SECOND
    override val msgFooter: Int
        get() = R.string.onboarding_screen_2_msg
    override val imgFooter: Int
        get() = R.drawable.ic_person
    override val actionBtnNext: (() -> Unit)
        get() = ({
            navigate(R.id.thirdOnboardingFragment)
        })
    override fun initBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentOnboarding2Binding = FragmentOnboarding2Binding.inflate(inflater)

    override fun initHeader(): ItemOnboardingHeaderBinding = binding.layoutHeader

    override fun initFooter(): ItemOnboardingFooterBinding = binding.layoutFooter

}