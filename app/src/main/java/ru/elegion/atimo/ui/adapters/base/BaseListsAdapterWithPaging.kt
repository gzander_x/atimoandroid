package ru.elegion.atimo.ui.adapters.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.BaseAdapterListModel
import ru.elegion.atimo.databinding.ItemListBaseBinding

class BaseListsAdapterWithPaging
    : BaseAdapterWithPaging<BaseAdapterListModel, ItemListBaseBinding>(DiffCallback) {
    override fun getBinding(
        inflater: LayoutInflater,
        parent: ViewGroup,
        viewType: Int
    ): ItemListBaseBinding {
        return ItemListBaseBinding.inflate(inflater, parent, false)
    }

    override fun bindViewHolder(holder: BindingHolder, data: BaseAdapterListModel?) {
        holder.binding.apply {
            imagePicture.setImageDrawable(ContextCompat.getDrawable(holder.itemView.context, data?.image ?: R.drawable.ic_icon_close))
            textTitle.text = data?.tittle
            textSubtitle.text = data?.subTittle
        }
    }

    private object DiffCallback: DiffUtil.ItemCallback<BaseAdapterListModel>(){
        override fun areItemsTheSame(
            oldItem: BaseAdapterListModel,
            newItem: BaseAdapterListModel
        ): Boolean {
            return oldItem.data == newItem.data
        }

        override fun areContentsTheSame(
            oldItem: BaseAdapterListModel,
            newItem: BaseAdapterListModel
        ): Boolean {
            return oldItem == newItem
        }

    }

}