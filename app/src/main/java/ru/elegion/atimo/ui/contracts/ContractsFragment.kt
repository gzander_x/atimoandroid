package ru.elegion.atimo.ui.contracts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.contracts.Contract
import ru.elegion.atimo.databinding.FragmentContractsBinding
import ru.elegion.atimo.databinding.LayoutEmptyListBinding
import ru.elegion.atimo.ui.adapters.OperationsAndContactsAdapter
import ru.elegion.atimo.ui.base.BaseFragment
import ru.elegion.atimo.ui.base.BaseFragmentWithEmptyList
import ru.elegion.atimo.util.Resource
import timber.log.Timber

@AndroidEntryPoint
class ContractsFragment() : BaseFragmentWithEmptyList<FragmentContractsBinding>() {

    override var menuRes: Int? = null

    override val actionsMenu: HashMap<Int, () -> Unit>?
        get() = null

    private val viewModel: ContractsViewModel by viewModels()

    private lateinit var adapter: OperationsAndContactsAdapter

    override val title
        @StringRes get() = R.string.empty_title
    override val subTitle: Int
        @StringRes get() = R.string.contracts__empty_subtitle
    override val btnTittle: Int
        @StringRes get() = R.string.contracts__empty_btn
    override val imgSrc: Int
        @DrawableRes get() = R.drawable.ic_empty_contracts
    override val navigateId: Int
        @IdRes get() = R.id.tariffsFragment
    override fun initBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentContractsBinding {
        return FragmentContractsBinding.inflate(inflater)
    }

    override fun initEmptyLayout(): LayoutEmptyListBinding {
        return binding.layoutEmptyList
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecycler()
        setupObservers()
    }

    private fun initRecycler() {
        adapter = OperationsAndContactsAdapter().apply {
            listener = {
                //TODO переход на фрагмент Операция
                val operation = it as? Contract
                Timber.d("Нажана на операцию $operation")
            }
        }
        binding.apply {
            recyclerContracts.adapter = adapter
            recyclerContracts.layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun setupObservers() {
        viewModel.apply {
            liveDataContracts.observe(viewLifecycleOwner){
                when(it){
                    is Resource.Loading -> {}
                    is Resource.Success -> {
                        checkEmpty(it.data)
                        it.data?.let { list ->
                            adapter.setData(list)
                        }
                    }
                    is Resource.Error -> {}
                }
            }
        }
    }
}