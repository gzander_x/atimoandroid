package ru.elegion.atimo.ui.splash

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.elegion.atimo.repository.user.UserRepository
import ru.elegion.atimo.util.EventLiveData
import javax.inject.Inject

private const val ARTIFICIAL_DELAY_MILLIS = 4000L

@HiltViewModel
class SplashViewModel @Inject constructor(
    private val userRepository: UserRepository
) : ViewModel() {

    val userAuthStateChecked = EventLiveData<Boolean>()

    init { checkUserAuth() }

    private fun checkUserAuth() {
        viewModelScope.launch {
            delay(ARTIFICIAL_DELAY_MILLIS)

            val isLoggedIn = userRepository.isUserValid
            userAuthStateChecked.setValue(isLoggedIn)
        }
    }
}