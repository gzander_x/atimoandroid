package ru.elegion.atimo.ui.drivers

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.DrawableRes
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.hilt.navigation.fragment.hiltNavGraphViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.drivers.Driver
import ru.elegion.atimo.databinding.FragmentListDriversBinding
import ru.elegion.atimo.databinding.LayoutEmptyListBinding
import ru.elegion.atimo.ui.base.BaseFragmentWithEmptyList
import ru.elegion.atimo.ui.mo.SelectedMedInspectionFragment
import ru.elegion.atimo.ui.waybill.selected.WaybillsSelectedFragment
import ru.elegion.atimo.util.AlertDialogBundleBuilder
import ru.elegion.atimo.util.Constants
import ru.elegion.atimo.util.FilterPassedDialogBundleBuilder
import ru.elegion.atimo.util.Resource
import ru.elegion.atimo.util.extensions.toast

@AndroidEntryPoint
class ListDriversFragment : BaseFragmentWithEmptyList<FragmentListDriversBinding>() {

    private lateinit var adapter: ListDriversAdapter
    private var adapterPosition: Int? = null
    private val viewModel: DriversViewModel by hiltNavGraphViewModels(R.id.nav_graph)

    override var menuRes: Int? = null

    override val actionsMenu: HashMap<Int, () -> Unit>
        get() = hashMapOf(
            R.id.menuAdd to {
                navigate(
                    R.id.addDriverFragment,
                    bundleOf(AddDriverFragment.EXTRA_TYPE_OPEN to Constants.TYPE_ADD)
                )
            },
            R.id.menuFilterDialog to {
                val bundle = FilterPassedDialogBundleBuilder.Builder()
                    .setState(filterState as? Boolean?)
                    .setButtonsText(R.array.filter_passed_tittle_buttons_v3).build().getBundle()
                findNavController().navigate(
                    R.id.filterPassedDialog, bundle
                )
            }
        )
    override var KEY_REQUEST_MENU: String? = Constants.BUNDLE_DIALOG_PASSED_VALUE
    override var filterResultAction: ((value:Any?)->Unit)? = {
        filterState = it
        if(it is Boolean?){
            changedIconMenu(it)
            viewModel.getDriversWithFilter(it)
        }
    }
    override val title: Int
        @StringRes get() = R.string.empty_title
    override val subTitle: Int
        @StringRes get() = R.string.drivers__empty_subtitle
    override val btnTittle: Int
        @StringRes get() = R.string.drivers__add_btn
    override val imgSrc: Int
        @DrawableRes get() = R.drawable.ic_person
    override val navigateId: Int
        @IdRes get() = R.id.addDriverFragment

    override fun initBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentListDriversBinding{
        menuRes = if(viewModel.isVisiblePassed) R.menu.add_menu_with_passed else R.menu.add_menu_without_passed
        return FragmentListDriversBinding.inflate(inflater)
    }

    override fun initEmptyLayout(): LayoutEmptyListBinding {
        return binding.layoutEmptyList
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getListDrivers()
        adapter = ListDriversAdapter(object : ListDriversAdapter.Callback{
            override fun onClickMenu(driver: Driver, position: Int) {
                adapterPosition = position
                viewModel.setDriver(driver)
                val isBlock = driver.driverStatus
                navigate(R.id.actionsDriverDialog, bundleOf(ActionsDriverDialog.ARGUMENT_IS_BLOCK to isBlock))
            }
        })
        binding.apply {
            recyclerDrivers.adapter = adapter
            recyclerDrivers.layoutManager = LinearLayoutManager(requireContext())

        }
        setupObservers()
        setListener()
    }

    private fun setListener() {
        binding.apply {
            edFindDrivers.addTextChangedListener {
                viewModel.findDrivers(it.toString())
            }
        }
    }



    private fun setupObservers(){
        viewModel.apply {
            isStateFilterLiveData.observe(viewLifecycleOwner) {
                filterState = it
            }
            liveDataDriversList.observe(viewLifecycleOwner){
                when(it){
                    is Resource.Loading -> {}
                    is Resource.Success -> {
                        checkEmpty(it.data)
                        it.data?.let { list ->
                            adapter.setData(list)
                        }
                    }
                    is Resource.Error -> {}
                }
            }
            showToastNotSearch.observe(viewLifecycleOwner){
                Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
            }
            showBlockDialog.observe(viewLifecycleOwner){
                showAlertDialog(
                    {
                        viewModel.blockDriver()
                    },
                    AlertDialogBundleBuilder.Builder()
                        .setTitle(if (it.driverStatus == 0) R.string.dialog_block_tittle else R.string.dialog_unblock_tittle)
                        .setMessage(
                            getString(
                                if (it.driverStatus == 0) R.string.dialog_block_msg_something else R.string.dialog_unblock_msg_something,
                                "${it.lastName} ${it.firstName} ${it.secondName}"
                            )
                        )
                        .setRequestKey()
                        .build()
                        .getBundle(),
                    Constants.ALERT_REQUEST_KEY
                )
            }
            navigateToChanged.observe(viewLifecycleOwner){
                navigate(R.id.addDriverFragment, bundleOf(AddDriverFragment.EXTRA_TYPE_OPEN to Constants.TYPE_CHANGE))
            }
            navigateToMedical.observe(viewLifecycleOwner){
                navigate(
                    R.id.selectedMedInspectionFragment,
                    SelectedMedInspectionFragment.prepareBundle(it.getFullName(), it.phone)
                )
            }
            navigateToWaybill.observe(viewLifecycleOwner){
                val bundle = bundleOf(
                    WaybillsSelectedFragment.BUNDLE_NAME_DRIVER to it.getFullName(),
                    WaybillsSelectedFragment.BUNDLE_PHONE to it.phone
                )
                navigate(R.id.waybillsSelected, bundle)
            }
            liveDataClickBlock.observe(viewLifecycleOwner){
                when(it){
                    is Resource.Loading -> showLoader(true)
                    is Resource.Success -> {
                        val toastText = getString(
                            if (it.data!!.driverStatus == 0) R.string.drivers_msg_unblock else R.string.drivers_msg_block,
                            "${it.data.lastName} ${it.data.firstName} ${it.data.secondName}"
                        )
                        toast(requireContext(), toastText)
                        adapterPosition?.let { position ->
                            when(filterState) {
                                true -> {
                                    if (it.data.driverStatus == 0) {
                                        adapter.notifyItemChanged(position)
                                    } else {
                                        deleteCarFromLocal(position)
                                        adapter.deleteItem(position)
                                    }
                                }
                                false -> {
                                    if (it.data.driverStatus == 0) {
                                        deleteCarFromLocal(position)
                                        adapter.deleteItem(position)
                                    } else {
                                        adapter.notifyItemChanged(position)
                                    }
                                }
                                else -> { adapter.notifyItemChanged(position) }
                            }

                        }
                    }
                    is Resource.Error -> {
                        viewModel.rollbackDriver()
                        adapterPosition?.let { position ->
                            adapter.notifyItemChanged(position)
                        }
                        retryDialogMessage { viewModel.blockDriver() }
                    }
                }
                showLoader(false)
            }
        }
    }

    override fun onDetach() {
        super.onDetach()
        viewModel.getDriversWithFilter(null)
    }

}