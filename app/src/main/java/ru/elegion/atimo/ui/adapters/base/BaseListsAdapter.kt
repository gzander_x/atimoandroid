package ru.elegion.atimo.ui.adapters.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import ru.elegion.atimo.data.models.BaseAdapterListModel
import ru.elegion.atimo.databinding.ItemListBaseBinding

class BaseListsAdapter: BaseAdapter<BaseAdapterListModel, ItemListBaseBinding>() {
    override fun getBinding(
        inflater: LayoutInflater,
        parent: ViewGroup,
        viewType: Int
    ): ItemListBaseBinding {
        return ItemListBaseBinding.inflate(inflater, parent, false)
    }

    override fun bindViewHolder(holder: ViewBindingHolder, data: BaseAdapterListModel) {
        holder.binding.apply {
            imagePicture.setImageDrawable(ContextCompat.getDrawable(holder.itemView.context, data.image))
            textTitle.text = data.tittle
            textSubtitle.text = data.subTittle
        }
    }
}