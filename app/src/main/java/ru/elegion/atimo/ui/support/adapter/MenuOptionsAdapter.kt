package ru.elegion.atimo.ui.support.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import ru.elegion.atimo.databinding.ItemMenuDenseBinding

class MenuOptionsAdapter : ListAdapter<MenuOption, MenuOptionsAdapter.ViewHolder>(ItemCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder.fromParent(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(getItem(position))

    class ViewHolder private constructor(
        private val binding: ItemMenuDenseBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: MenuOption) = binding.run {
            textTitle.setText(item.titleRes)
            imageLeadingIcon.setImageResource(item.iconRes)
            root.setOnClickListener { item.onClickListener?.invoke() }
        }

        companion object {
            fun fromParent(parent: ViewGroup): ViewHolder {
                val inflater = LayoutInflater.from(parent.context)
                val binding = ItemMenuDenseBinding.inflate(inflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }

    object ItemCallback : DiffUtil.ItemCallback<MenuOption>() {

        override fun areItemsTheSame(oldItem: MenuOption, newItem: MenuOption) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: MenuOption, newItem: MenuOption) =
            oldItem == newItem
    }
}