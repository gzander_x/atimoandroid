package ru.elegion.atimo.ui.base

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.hilt.navigation.fragment.hiltNavGraphViewModels
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.onboarding.OnboardingSegment
import ru.elegion.atimo.databinding.ItemOnboardingFooterBinding
import ru.elegion.atimo.databinding.ItemOnboardingHeaderBinding
import ru.elegion.atimo.ui.onboarding.vm.OnboardingViewModel
import timber.log.Timber

abstract class BaseOnboardingFragment<B : ViewBinding>: BaseFragment<B>() {

    private val viewModel: OnboardingViewModel by hiltNavGraphViewModels(R.id.nested_navigation_onboarding)

    abstract val segment: OnboardingSegment
    @get:StringRes
    abstract val msgFooter: Int?
    @get:DrawableRes
    abstract val imgFooter: Int?

    private var _bindingHeader: ItemOnboardingHeaderBinding? = null
    protected val bindingHeader get() = checkNotNull(_bindingHeader)

    private var _bindingFooter: ItemOnboardingFooterBinding? = null
    protected val bindingFooter get() = _bindingFooter

    abstract fun initHeader(): ItemOnboardingHeaderBinding
    abstract fun initFooter(): ItemOnboardingFooterBinding?

    abstract val actionBtnNext: (()->Unit)?

    private fun setProgressPrevSegments(views: List<ProgressBar>){
        for(view in views){
            view.progress = view.max
        }
    }

    private fun renderHeader(){
        _bindingHeader = initHeader()
        _bindingHeader?.apply {
            imgClose.setOnClickListener{
                navigate(
                    resId = R.id.loginFragment,
                    navOption = NavOptions.Builder()
                        .setPopUpTo(R.id.nav_graph, true)
                        .build()
                )
            }
            when(segment){
                OnboardingSegment.FIRST -> {}
                OnboardingSegment.SECOND -> { setProgressPrevSegments(listOf(progress1)) }
                OnboardingSegment.THIRD -> { setProgressPrevSegments(listOf(progress1, progress2)) }
                OnboardingSegment.FOURTH -> { setProgressPrevSegments(listOf(progress1, progress2, progress3)) }
                OnboardingSegment.FIFTH -> { setProgressPrevSegments(listOf(progress1, progress2, progress3, progress4)) }
                OnboardingSegment.SIXTH -> { setProgressPrevSegments(listOf(progress1, progress2, progress3, progress4, progress5)) }
                OnboardingSegment.SEVENTH -> { setProgressPrevSegments(listOf(progress1, progress2, progress3, progress4, progress5, progress6)) }
                OnboardingSegment.EIGHTH -> { setProgressPrevSegments(listOf(progress1, progress2, progress3, progress4, progress5, progress6, progress7)) }
            }
        }
    }

    private fun renderFooter(){
        _bindingFooter = initFooter()
        bindingFooter?.apply {
            msgFooter?.let {
                textMsg.text = getString(it)
            }
            imgFooter?.let{
                imageFooter.setBackgroundResource(it)
            }
            btnPrev.setOnClickListener {
                viewModel.stopTimer()
                findNavController().popBackStack()
            }
            btnNext.setOnClickListener {
                viewModel.stopTimer()
                actionBtnNext?.invoke()
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        renderHeader()
        renderFooter()
        setupObservers()
        viewModel.starTimer()
    }

    private fun setValueProgress(view: ProgressBar, value: Long){
        view.apply {
            progress = max - value.toInt()
        }
    }

    private fun setupObservers() = viewModel.run{
        timerValue.observe(viewLifecycleOwner){
            when(segment){
                OnboardingSegment.FIRST -> { setValueProgress(bindingHeader.progress1, it) }
                OnboardingSegment.SECOND -> { setValueProgress(bindingHeader.progress2, it) }
                OnboardingSegment.THIRD -> { setValueProgress(bindingHeader.progress3, it) }
                OnboardingSegment.FOURTH -> { setValueProgress(bindingHeader.progress4, it) }
                OnboardingSegment.FIFTH -> { setValueProgress(bindingHeader.progress5, it) }
                OnboardingSegment.SIXTH -> { setValueProgress(bindingHeader.progress6, it) }
                OnboardingSegment.SEVENTH -> { setValueProgress(bindingHeader.progress7, it) }
                OnboardingSegment.EIGHTH -> { setValueProgress(bindingHeader.progress8, it) }
            }
        }
        navigateNextFragment.observe(viewLifecycleOwner){
            if(segment != OnboardingSegment.EIGHTH){
                actionBtnNext?.invoke()
            }
        }
    }
    override fun onDestroy() {
        super.onDestroy()
        _bindingHeader = null
        _bindingFooter = null
    }


}