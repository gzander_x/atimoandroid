package ru.elegion.atimo.ui.drivers

import android.os.Bundle
import android.view.LayoutInflater
import ru.elegion.atimo.databinding.DialogActionPhotoBinding
import ru.elegion.atimo.util.Constants.TYPE_FROM_GALLERY
import ru.elegion.atimo.util.Constants.TYPE_TAKE_PHOTO
import ru.elegion.atimo.util.bottomsheet.BaseBottomSheetRoundedFragment

class ActionPhotoFragment : BaseBottomSheetRoundedFragment<DialogActionPhotoBinding>() {

    companion object{
        const val KEY_REQUEST_ACTION_PHOTO = "ActionPhotoFragment"
        const val EXTRA_TYPE_ACTION_PHOTO = "type action"
    }

    override fun initBinding(inflater: LayoutInflater): DialogActionPhotoBinding {
        return DialogActionPhotoBinding.inflate(inflater)
    }

    override fun initialize() {
        binding.imgClose.setOnClickListener {
            dismiss()
        }
        binding.tvTakePhoto.setOnClickListener {
            sendResult(TYPE_TAKE_PHOTO)
        }
        binding.tvChooseGallery.setOnClickListener {
            sendResult(TYPE_FROM_GALLERY)
        }
    }

    private fun sendResult(typeChoose: Int){
        val bundle = Bundle()
        bundle.putInt(EXTRA_TYPE_ACTION_PHOTO, typeChoose)
        parentFragmentManager.setFragmentResult(
            KEY_REQUEST_ACTION_PHOTO,
            bundle
        )
        dismiss()
    }
}