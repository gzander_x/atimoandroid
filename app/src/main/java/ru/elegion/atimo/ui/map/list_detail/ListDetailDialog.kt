package ru.elegion.atimo.ui.map.list_detail

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.content.res.AppCompatResources
import com.google.android.material.bottomsheet.BottomSheetDialog
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.points.Point
import ru.elegion.atimo.databinding.DialogListDetailBinding
import ru.elegion.atimo.util.Constants.KEY_POINT
import ru.elegion.atimo.util.bottomsheet.BaseBottomSheetRoundedFragment
import ru.elegion.atimo.util.gone
import timber.log.Timber

@AndroidEntryPoint
class ListDetailDialog : BaseBottomSheetRoundedFragment<DialogListDetailBinding>() {
    private val data = (1..4).toList()
    private var point: Point? = null

    override fun initBinding(inflater: LayoutInflater) = DialogListDetailBinding.inflate(inflater)
    override fun initialize() {
        point = arguments?.getParcelable(KEY_POINT)

        checkStatus()
        binding.apply {
            point?.apply {
                // TODO вставить название пунтка с бека
                tvAddress.text = name
                tvWorkTime.text = "${getString(R.string.daily)} $work_time_begin : $work_time_end"
            }
        }
        Timber.e("$point")
    }

    private fun initNavigation(latitude: Double?, longitude: Double?) {
        val google: Uri = Uri.parse(
            (
                "google.navigation:q=" +
                    latitude
                ) + "," +
                longitude
                    .toString() + "&mode=g"
        )
        val intentGoogle = Intent(Intent.ACTION_VIEW, google)
        val yandex: Uri = Uri.parse(
            (
                (
                    "yandexnavi://build_route_on_map?lat_to=" +
                        latitude
                    ) + "&lon_to=" +
                    longitude
                )
        )
        val intentYandex = Intent(Intent.ACTION_VIEW, yandex)
        val chooser = Intent.createChooser(intentGoogle, "") // default action
        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf(intentYandex))
        startActivity(chooser)
    }

    private fun checkStatus() {
        binding.tvStatus.apply {
            if (point?.status == 1) {
                if (point?.mo == true && point?.to == false) {
                    background =
                        AppCompatResources.getDrawable(requireContext(), R.color.purple)
                    text = getString(R.string.point_no_MO)
                } else if (point?.to == true && point?.mo == false) {
                    background =
                        AppCompatResources.getDrawable(requireContext(), R.color.dark_blue)
                    text = getString(R.string.point_no_TO)
                } else if (point?.to == true && point?.mo == true) {
                    gone()
                }
            } else {
                background =
                    AppCompatResources.getDrawable(requireContext(), R.color.icon_grey)
                setTextColor(resources.getColor(R.color.black, null))
                text = getString(R.string.point_repair)
            }
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): BottomSheetDialog {
        return BottomSheetDialog(requireContext(), R.style.MyTransparentBottomSheetDialogTheme)
    }

    override fun initClicks() {
        binding.apply {
            close.setOnClickListener { closeSheet() }
            setDirectionBtn.setOnClickListener {
                initNavigation(
                    longitude = point?.longitude?.toDouble(),
                    latitude = point?.latitude?.toDouble()
                )
            }
        }
    }
}
