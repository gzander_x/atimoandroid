package ru.elegion.atimo.ui.adapters.base

import androidx.paging.PagingSource
import androidx.paging.PagingState

private const val DEFAULT_FIRST_PAGE_NUMBER = 1
private const val DEFAULT_STEP = 1

class PageKeyedPagingSource<T : Any>(
    private val firstPageNumber: Int = DEFAULT_FIRST_PAGE_NUMBER,
    private val step: Int = DEFAULT_STEP,
    private val loader: suspend (page: Int, perPage: Int) -> List<T>
) : PagingSource<Int, T>() {

    override suspend fun load(params: LoadParams<Int>) = try {
        val pageNumber = params.key ?: firstPageNumber
        val data = loader(pageNumber, params.loadSize)

        val prevKey = (pageNumber - step).takeIf { pageNumber > firstPageNumber }
        val nextKey = (pageNumber + step).takeIf { data.isNotEmpty() }

        LoadResult.Page(data, prevKey, nextKey)
    } catch (e: Exception) {
        LoadResult.Error(e)
    }

    override fun getRefreshKey(state: PagingState<Int, T>) = state.anchorPosition?.let {
        state.closestPageToPosition(it)?.prevKey?.plus(step)
            ?: state.closestPageToPosition(it)?.nextKey?.minus(step)
    }
}
