package ru.elegion.atimo.ui.cars

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.DrawableRes
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.hilt.navigation.fragment.hiltNavGraphViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.vehicles.Vehicle
import ru.elegion.atimo.databinding.FragmentListCarsBinding
import ru.elegion.atimo.databinding.LayoutEmptyListBinding
import ru.elegion.atimo.ui.base.BaseFragmentWithEmptyList
import ru.elegion.atimo.ui.inspection.SelectedInspectionFragment
import ru.elegion.atimo.util.AlertDialogBundleBuilder
import ru.elegion.atimo.util.Constants
import ru.elegion.atimo.util.FilterPassedDialogBundleBuilder
import ru.elegion.atimo.util.Resource
import ru.elegion.atimo.util.extensions.toast

@AndroidEntryPoint
class ListCarsFragment : BaseFragmentWithEmptyList<FragmentListCarsBinding>() {

    private lateinit var adapter: ListCarsAdapter
    private var adapterPosition: Int? = null

    private val viewModel: CarsViewModel by hiltNavGraphViewModels(R.id.nav_graph)

    override var menuRes: Int? = null

    override val actionsMenu: HashMap<Int, () -> Unit>
        get() = hashMapOf(
            R.id.menuAdd to {navigate(R.id.addCarFragment, bundleOf(AddCarFragment.EXTRA_TYPE_OPEN to Constants.TYPE_ADD))},
            R.id.menuFilterDialog to {
                val bundle = FilterPassedDialogBundleBuilder.Builder()
                    .setState(filterState as? Boolean?)
                    .setButtonsText(R.array.filter_passed_tittle_buttons_v3).build().getBundle()
                findNavController().navigate(
                    R.id.filterPassedDialog, bundle
                )
            }
        )
    override var KEY_REQUEST_MENU: String? = Constants.BUNDLE_DIALOG_PASSED_VALUE
    override var filterResultAction: ((value:Any?)->Unit)? = {
        filterState = it
        if(it is Boolean?){
            changedIconMenu(it)
            viewModel.getListCatsWithFilter(it)
        }
    }
    override val title: Int
        @StringRes get() = R.string.empty_title
    override val subTitle: Int
        @StringRes get() = R.string.cars__empty_subtitle
    override val btnTittle: Int
        @StringRes get() = R.string.cars__add_btn
    override val imgSrc: Int
        @DrawableRes get() =  R.drawable.ic_car
    override val navigateId: Int
        @IdRes get() = R.id.addCarFragment

    override fun initBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentListCarsBinding {
        menuRes = if(viewModel.isVisiblePassed) R.menu.add_menu_with_passed else R.menu.add_menu_without_passed
        return FragmentListCarsBinding.inflate(inflater)
    }

    override fun initEmptyLayout(): LayoutEmptyListBinding {
        return binding.layoutEmptyList
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = ListCarsAdapter(object : ListCarsAdapter.Callback{
            override fun onClickMenu(car: Vehicle, position: Int) {
                adapterPosition = position
                viewModel.setCarMenu(car)
                navigate(R.id.actionsCarDialog)
            }
        })
        binding.apply {
            recyclerCars.adapter = adapter
            recyclerCars.layoutManager = LinearLayoutManager(requireContext())
        }
        setupObservers()
        setListener()
    }

    private fun setupObservers(){
        viewModel.apply {
            isStateFilterLiveData.observe(viewLifecycleOwner) {
                filterState = it
            }
            liveDataCarsList.observe(viewLifecycleOwner){
                when(it){
                    is Resource.Loading -> {}
                    is Resource.Success -> {
                        checkEmpty(it.data)
                        it.data?.let { list ->
                            adapter.setData(list)
                        }
                    }
                    is Resource.Error -> Toast.makeText(requireContext(), it.messages!!, Toast.LENGTH_SHORT).show()
                }
            }
            showToastNotSearch.observe(viewLifecycleOwner){
                Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
            }
            showBlockDialog.observe(viewLifecycleOwner){
                showAlertDialog(
                    {
                        viewModel.changeVehicleLocked()
                    },
                    AlertDialogBundleBuilder.Builder()
                        .setTitle(it.vehicleStatus.blockTitle)
                        .setMessage(getString(it.vehicleStatus.blockSubtitle, it.model))
                        .setRequestKey()
                        .build()
                        .getBundle(),
                    Constants.ALERT_REQUEST_KEY
                )
            }
            liveDataVehicleLockChanged.observe(viewLifecycleOwner) {
                when(it){
                    is Resource.Success -> it.data?.vehicleStatus?.let { isBlocked ->
                        val toastText = getString(
                            isBlocked.toastText,
                            it.data.model
                        )
                        toast(requireContext(), toastText)
                        adapterPosition?.let { position ->
                            when(filterState) {
                                true -> {
                                    if (!it.data.vehicleStatus) {
                                        adapter.notifyItemChanged(position)
                                    } else {
                                        viewModel.deleteCarFromLocal(position)
                                        adapter.deleteItem(position)
                                    }
                                }
                                false -> {
                                    if (!it.data.vehicleStatus) {
                                        viewModel.deleteCarFromLocal(position)
                                        adapter.deleteItem(position)
                                    } else {
                                        adapter.notifyItemChanged(position)
                                    }
                                }
                                else -> { adapter.notifyItemChanged(position) }
                            }
                        }
                    }
                    is Resource.Error -> {
                        Toast.makeText(requireContext(), it.messages!!, Toast.LENGTH_SHORT).show()
                        viewModel.rollbackCar()
                        adapterPosition?.let { position ->
                            adapter.notifyItemChanged(position)
                        }
                        retryDialogMessage { viewModel.changeVehicleLocked() }
                    }
                    is Resource.Loading -> showLoader(true)
                }
                showLoader(false)
            }
            navigateToHistory.observe(viewLifecycleOwner){
                navigate(
                    R.id.selectedInspectionFragment,
                    SelectedInspectionFragment.prepareBundle(it.model, it.vehicleGrz)
                )
            }
            navigateToChanged.observe(viewLifecycleOwner){
                navigate(R.id.addCarFragment, bundleOf(AddCarFragment.EXTRA_TYPE_OPEN to Constants.TYPE_CHANGE))
            }

        }
    }

    private fun setListener() {
        binding.apply {
            edFindCars.addTextChangedListener {
                viewModel.findCars(it.toString())
            }
        }
    }

    private val Boolean.toastText
        @StringRes
        get() = if (this) {
            R.string.car__blocked_successfully
        } else {
            R.string.car__unblocked_successfully
        }

    private val Boolean.blockTitle
        @StringRes
        get() = if (this) {
            R.string.actions_unblock
        } else {
            R.string.actions_block
        }

    private val Boolean.blockSubtitle
        @StringRes
        get() = if (this) {
            R.string.dialog_unblock_msg_something
        } else {
            R.string.dialog_block_msg_something
        }

    override fun onDetach() {
        super.onDetach()
        viewModel.getListCatsWithFilter(null)
    }
}