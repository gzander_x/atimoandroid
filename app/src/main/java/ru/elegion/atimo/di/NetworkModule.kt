package ru.elegion.atimo.di

import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.elegion.atimo.data.network.AtimoApiService
import ru.elegion.atimo.data.network.AuthApiService
import ru.elegion.atimo.data.network.base.BasicAuthInterceptor
import ru.elegion.atimo.data.network.base.NoAuthUrlStorage
import ru.elegion.atimo.data.network.base.TEST_API_URL
import ru.elegion.atimo.util.NetworkUtils
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Singleton
    @Provides
    fun providesHttpLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
    }

    @Singleton
    @Provides
    fun provideBasicAuthInterceptor(
        noAuthUrlStorage: NoAuthUrlStorage
    ) = BasicAuthInterceptor(
        username = "MB",
        password = "Mb3462609",
        noAuthUrlStorage = noAuthUrlStorage
    )

    @Singleton
    @Provides
    fun providesOkHttpClient(
        basicAuthInterceptor: BasicAuthInterceptor,
        httpLoggingInterceptor: HttpLoggingInterceptor
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(basicAuthInterceptor)
            .addInterceptor(httpLoggingInterceptor)
            .build()
    }

    @Singleton
    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        val gson = GsonBuilder()
            .setLenient()
            .create()
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl("$TEST_API_URL/")
            .client(okHttpClient)
            .build()
    }

    @Singleton
    @Provides
    fun provideAuthApiService(retrofit: Retrofit): AuthApiService {
        return retrofit.create(AuthApiService::class.java)
    }

    @Singleton
    @Provides
    fun provideAtimoApiService(retrofit: Retrofit): AtimoApiService{
        return retrofit.create(AtimoApiService::class.java)
    }

    @Singleton
    @Provides
    fun provideNetworkUtil(retrofit: Retrofit): NetworkUtils{
        return NetworkUtils(retrofit)
    }

}
