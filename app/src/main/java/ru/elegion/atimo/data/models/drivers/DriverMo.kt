package ru.elegion.atimo.data.models.drivers

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.BaseAdapterListModel

@Parcelize
data class DriverMo(
    @SerializedName("date") val date: String? = null,
    @SerializedName("number") val number: String? = null,
    @SerializedName("driver_fio") val driverFio: String? = null,
    @SerializedName("medic_fio") val medicFio: String? = null,
    @SerializedName("medic_hash") val medicHash: String? = null,
    @SerializedName("medic_company_name") val medicCompanyName: String? = null,
    @SerializedName("medic_company_license") val medicCompanyLicense: String? = null,
    @SerializedName("status") val status: String? = null,
    @SerializedName("comment") val comment: String? = null
) : Parcelable {

    private val String?.booleanStatus
        get() = this == "Успешный"

    fun toAdapterModel() = BaseAdapterListModel(
        image = if (status.booleanStatus) {
            R.drawable.ic_inspection_pos
        } else {
            R.drawable.ic_inspection_neg
        },
        tittle = status ?: "",
        subTittle = date ?: "",
        data = this
    )
}
