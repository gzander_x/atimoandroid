package ru.elegion.atimo.data.models.history

import com.google.gson.annotations.SerializedName

//TODO добавить SerializedName
data class HistoryItem(
    @SerializedName("") val cntAll: Int?,
    @SerializedName("") val cntPassed: Int? = null,
    @SerializedName("") val cntNotPassed: Int? = null
)