package ru.elegion.atimo.data.models.personal_payment

data class PersonalPaymentInfo (
    val countDrivers: Int,
    val countCars: Int,
    val isNeedTechInspection: Boolean,
    val isBecomeReleasePoint: Boolean
    )