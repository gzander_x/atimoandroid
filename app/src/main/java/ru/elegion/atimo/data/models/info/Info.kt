package ru.elegion.atimo.data.models.info

data class Info(
    val info_id:Int,
    val title:String,
    val text:String,
    val created:String
)
