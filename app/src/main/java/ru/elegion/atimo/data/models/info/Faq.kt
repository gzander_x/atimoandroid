package ru.elegion.atimo.data.models.info

data class Faq(
    val faq_id:Int,
    val title:String,
    val text:String,
    val created:String
)
