package ru.elegion.atimo.data.models.vehicles

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.BaseAdapterListModel

@Parcelize
data class TechInspectionRes(
    @SerializedName("date") val date: String? = null,
    @SerializedName("number") val number: String? = null,
    @SerializedName("vehicle_type") val vehicleType: String? = null,
    @SerializedName("model") val model: String? = null,
    @SerializedName("technician_fio") val technicianFio: String? = null,
    @SerializedName("technician_hash") val technicianHash: String? = null,
    @SerializedName("technician_company") val technicianCompany: String? = null,
    @SerializedName("technician_company_license") val technicianCompanyLicense: String? = null,
    @SerializedName("odometer") val odometer: Int? = null,
    @SerializedName("vin") val vin: String? = null,
    @SerializedName("vehicle_year") val vehicleYear: String? = null,
    @SerializedName("status") val status: String? = null,
    @SerializedName("comment") val comment: String? = null
) : Parcelable {
    private val String?.booleanStatus
        get() = this == "Пройден"
    fun toAdapterModel() = BaseAdapterListModel(
        image = if (status.booleanStatus) {
            R.drawable.ic_inspection_pos
        } else {
            R.drawable.ic_inspection_neg
        },
        tittle = status?:"",
        subTittle = date?:"",
        data = this
    )
}
