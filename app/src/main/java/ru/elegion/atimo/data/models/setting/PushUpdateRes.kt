package ru.elegion.atimo.data.models.setting

import com.google.gson.annotations.SerializedName

data class PushUpdateRes(
    @SerializedName("Success") val success: Boolean? = null
)
