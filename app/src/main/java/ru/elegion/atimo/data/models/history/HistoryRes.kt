package ru.elegion.atimo.data.models.history

import com.google.gson.annotations.SerializedName

//TODO добавить SerializedName
data class HistoryRes(
    @SerializedName("Success") val success: Boolean? = null,
    @SerializedName("") val techInspections: HistoryItem,
    @SerializedName("") val medInspections: HistoryItem,
    @SerializedName("") val waybills: HistoryItem,
)