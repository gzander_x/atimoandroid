package ru.elegion.atimo.data.models.drivers

import com.google.gson.annotations.SerializedName

//Отличается от модели, которая приходит с сервера. Почему? Потому что бек говно(
data class DriverCreateModify(
    @SerializedName("company_id") val companyId: Int, //Отличие
    @SerializedName("first_name") val firstName: String,
    @SerializedName("second_name") val secondName: String,
    @SerializedName("last_name") val lastName: String,
    @SerializedName("phone") val phone: String,
    @SerializedName("sex") val sex: Int,
    @SerializedName("driver_license") val driverLicense: String,
    @SerializedName("birthdate") val birthdate: String,
    @SerializedName("blocked") val driverStatus: Boolean, //Отличие
)