package ru.elegion.atimo.data.models

data class FilteringListScreenState(
    val dateRange: Pair<Long, Long>? = null,
    val filteringFlag: Boolean? = null
)
