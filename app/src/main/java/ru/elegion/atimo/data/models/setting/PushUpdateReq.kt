package ru.elegion.atimo.data.models.setting

import com.google.gson.annotations.SerializedName

data class PushUpdateReq(
    @SerializedName("phone") val phone: String,
    @SerializedName("push_list") val pushList: List<PushUpdateReqBody>
)
