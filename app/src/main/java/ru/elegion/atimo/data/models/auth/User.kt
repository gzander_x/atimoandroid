package ru.elegion.atimo.data.models.auth

import com.google.gson.annotations.SerializedName
import ru.elegion.atimo.data.models.company.CompanyInfoReq
import ru.elegion.atimo.data.models.drivers.Driver

data class User(
    @SerializedName("first_name") var firstName: String,
    @SerializedName("second_name") var secondName: String,
    @SerializedName("last_name") var lastName: String,
    @SerializedName("phone") var phone: String,
    @SerializedName("user_role") val userRole: Int,
    @SerializedName("company_id") val companyId: String,
    var email: String? = ""
){
    fun getPhoneForProfile(): String{
        return if(phone.length == 11){
            var builder = StringBuilder()
            val phoneNew = "+$phone"
            for(i in phoneNew.indices){
                builder = when(i){
                    2,5 -> {
                        builder.append(" ")
                        builder.append(phoneNew[i])
                    }
                    8, 10 -> {
                        builder.append("-")
                        builder.append(phoneNew[i])
                    }
                    else -> builder.append(phoneNew[i])
                }
            }
            builder.toString()
        }
        else{
            phone
        }
    }

    fun fromCompany(companyInfo: CompanyInfoReq){
        firstName = companyInfo.firstName
        secondName = companyInfo.secondName
        lastName = companyInfo.lastName
        phone = companyInfo.phone
        email = companyInfo.email
    }

    fun toDriver()= Driver(
        companyId = this.companyId,
        firstName = this.firstName,
        secondName = this.secondName,
        lastName = this.lastName,
        phone = this.phone,
        sex = 0, //TODO не приходит с сервера
        driverLicense = "", //TODO не приходит с сервера
        birthdate = "",//TODO не приходит с сервера
        driverStatus = 1
    )
}
