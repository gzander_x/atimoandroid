package ru.elegion.atimo.data.models.onboarding

enum class OnboardingSegment {
    FIRST, SECOND, THIRD, FOURTH, FIFTH, SIXTH, SEVENTH, EIGHTH
}