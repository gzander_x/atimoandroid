package ru.elegion.atimo.data.models.rates

data class RateRes(
    val success:Boolean,
    val rates:Rate
)
