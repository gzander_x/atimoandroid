package ru.elegion.atimo.data.network.base

/**
 * URL всех серверных запросов
 *
 * ВАЖНО!
 * 1. Url конкретного апи не должен заканчиваться слэшом
 * 2. Url конкретного апи не должен начинаться слэшом
 */

const val TEST_API_HOST = "тест.атимо.рф/ATM_PRG_2"
const val PROD_API_HOST = ""

const val TEST_API_URL = "http://$TEST_API_HOST/hs/mobile"
const val PROD_API_URL = "https://$PROD_API_HOST/api"

/**
 * Url для работы с SMS-ru
 */
const val SMS_RU_KEY = "9C31B76D-C655-76FE-3C5F-B47696D41ED8"
object UrlSmsRu {
    private const val SMS_RU_HOST = "https://sms.ru"

    const val CALL = "$SMS_RU_HOST/code/call?"
    const val SMS = "$SMS_RU_HOST/sms/send?json=1"
}

/**
 * Url для работы с пользователями
 */
object UrlUser {
    const val USER = "$TEST_API_URL/UserGET"
    const val PUSH = "$TEST_API_URL/CreateModifyPushPOST"
}

/**
 * Url для работы с компаниями
 */
object UrlCompany {
    const val CREATE_MODIFY_COMPANY = "$TEST_API_URL/CreateModifyOrganizationPOST"
}

/**
 * Url для работы с водителями
 */
object UrlDriver {

    const val CREATE_MODIFY_DRIVER = "$TEST_API_URL/CreateModifyDriverPOST"
    const val CREATE_MODIFY_DRIVER_PHOTO = "$TEST_API_URL/CreateModifyDriverPhotoPOST"
    const val DRIVERS_LIST = "$TEST_API_URL/DriversListGET"
    const val MEDICAL_CHECKUP_LIST = "$TEST_API_URL/MedicalCheckupListGET"
}

object UrlWaybill {
    const val WAYBILL_LIST = "$TEST_API_URL/WaybillsListGET"
}

/**
 * Url для работы с транспортными средствами
 */
object UrlVehicle {
    const val CREATE_MODIFY_VEHICLE = "$TEST_API_URL/CreateModifyVehiclePOST"
    const val VEHICLES_LIST = "$TEST_API_URL/VehiclesListGET"
    const val TECHNICAL_CHECKUP_LIST = "$TEST_API_URL/TechnicalCheckupListGET"
}

/**
 * Url для работы с тарифами, оплатами и счетами
 */
object UrlPayment {
    private const val URL_RATE = "rate"
    private const val URL_PAYMENT = "payment"

    const val LIST = "$URL_RATE/list"
    const val CARD = "$URL_PAYMENT/card"
    const val BILL = "$URL_PAYMENT/bill"
    const val ESTIMATE = "$URL_PAYMENT/estimate"
}

/**
 * Url для работы с точками обслуживания
 */
object UrlPoints {
    const val TERMINAL_LIST = "$TEST_API_URL/TerminalsListGET"
}

/**
 * Url для работы с уведомлениями
 */
object UrlNotification {
    private const val URL_NOTIFICATION = "notification"

    const val LIST = "$URL_NOTIFICATION/list"
    const val USER_LIST = "$URL_NOTIFICATION/user-list"
    const val UPDATE = "$URL_NOTIFICATION/update"
}

/**
 * Url для работы с информацией о системе
 */
object UrlInfo {
    private const val URL_INFO = "info"

    const val FAQ = "$URL_INFO/faq"
    const val QA = "$URL_INFO/qa"
    const val INFO = "$URL_INFO/info"
}
