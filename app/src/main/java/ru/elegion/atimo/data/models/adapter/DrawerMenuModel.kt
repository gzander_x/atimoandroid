package ru.elegion.atimo.data.models.adapter

data class DrawerMenuModel(
    val title: String,
    val navigation: Int?,
    var count: Int? = null
)
