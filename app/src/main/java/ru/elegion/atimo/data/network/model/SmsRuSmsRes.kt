package ru.elegion.atimo.data.network.model

import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName

data class SmsRuSmsRes(
    val status: String,
    @SerializedName("status_code") val statusCode: Int,
    val sms: JsonObject?
)

data class SmsInfo(
    val status: String,
    @SerializedName("status_code") val statusCode: Int,
    @SerializedName("status_text") val statusText: String?,
)
