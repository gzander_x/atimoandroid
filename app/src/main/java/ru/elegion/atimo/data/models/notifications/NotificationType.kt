package ru.elegion.atimo.data.models.notifications

import androidx.annotation.StringRes
import ru.elegion.atimo.R

enum class NotificationType(
    val key: String,
    @StringRes val titleRes: Int
) {
    WAYBILL_END("WaybillEnd", R.string.notification_type_waybill_end),
    BALANCE_UPDATE("BalanceUpdate", R.string.notification_type_balance_update),
    DRIVER_BLOCKED("DriverBlocked", R.string.notification_type_driver_block);

    companion object {
        fun byKey(key: String?) = values().firstOrNull { it.key == key }
    }
}