package ru.elegion.atimo.data.models.notifications

data class NotificationRes(
    val success:Boolean,
    val notifications: List<Notification>
)
