package ru.elegion.atimo.data.network

import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query
import ru.elegion.atimo.data.models.setting.PushUpdateReq
import ru.elegion.atimo.data.models.setting.PushUpdateRes
import ru.elegion.atimo.data.models.auth.UserRes
import ru.elegion.atimo.data.models.company.CompanyCreateModifyRes
import ru.elegion.atimo.data.models.company.CompanyReq
import ru.elegion.atimo.data.models.drivers.DriverCreateReq
import ru.elegion.atimo.data.models.drivers.DriverMoRes
import ru.elegion.atimo.data.models.drivers.DriversRes
import ru.elegion.atimo.data.models.history.HistoryRes
import ru.elegion.atimo.data.models.points.PointsRes
import ru.elegion.atimo.data.models.vehicles.TechInspectionsRes
import ru.elegion.atimo.data.models.vehicles.VehicleUpdateReq
import ru.elegion.atimo.data.models.vehicles.VehiclesRes
import ru.elegion.atimo.data.models.waybills.WaybillsRes
import ru.elegion.atimo.data.network.base.*

interface AtimoApiService {

    @GET(UrlDriver.DRIVERS_LIST)
    suspend fun getDrivers(
        @Query("Telephone") telephone: String,
    ): Response<DriversRes>

    @POST(UrlDriver.CREATE_MODIFY_DRIVER_PHOTO)
    suspend fun uploadPhotoDriver(): Response<Unit> // TODO пока не понятно что возвращает метод

    @POST(UrlDriver.CREATE_MODIFY_DRIVER)
    suspend fun createModifyDriver(@Body driver: DriverCreateReq): Response<Unit> // TODO пока не понятно что возвращает метод

    @POST(UrlCompany.CREATE_MODIFY_COMPANY)
    suspend fun createModifyCompany(@Body company: CompanyReq): Response<CompanyCreateModifyRes>


    @GET(UrlVehicle.VEHICLES_LIST)
    suspend fun getVehicles(
        @Query("Telephone") telephone: String,
    ): Response<VehiclesRes>

    @GET(UrlVehicle.TECHNICAL_CHECKUP_LIST)
    suspend fun getTechInspections(
        @Query("Telephone") telephone: String,
        @Query("Passed") passed: String? = null,
        @Query("Date_Begin") dateBegin: String? = null,
        @Query("Date_End") dateEnd: String? = null,
        @Query("Page") page: Int? = null,
        @Query("PerPage") perPage: Int? = null,
        @Query("Vehicle_GRZ") grz: String = ""
    ): Response<TechInspectionsRes>

    @GET(UrlDriver.MEDICAL_CHECKUP_LIST)
    suspend fun getMedInspections(
        @Query("Telephone") telephone: String,
        @Query("Passed") passed: String? = null,
        @Query("Date_Begin") dateBegin: String? = null,
        @Query("Date_End") dateEnd: String? = null,
        @Query("Page") page: Int? = null,
        @Query("PerPage") perPage: Int? = null
    ): Response<DriverMoRes>

    @POST(UrlVehicle.CREATE_MODIFY_VEHICLE)
    suspend fun createModifyVehicle(
        @Body vehicle: VehicleUpdateReq
    ): Response<Unit>

    @GET(UrlWaybill.WAYBILL_LIST)
    suspend fun getWaybills(
        @Query("Telephone") phone: String,
        @Query("Passed") passed: String? = null,
        @Query("Date_Begin") dateBegin: String? = "",
        @Query("Date_End") dateEnd: String? = "",
        @Query("Page") page: Int? = 1,
        @Query("PerPage") perPage: Int? = 15
    ): Response<WaybillsRes>

    // TODO добавить вызов с сервера
    suspend fun getHistoryInfo(
        @Query("Telephone") phone: String,
    ): Response<HistoryRes>

    @GET(UrlPoints.TERMINAL_LIST)
    suspend fun getTerminals(
        @Query("Phone") phone: String,
        @Query("latitude") latitude: String = "0",
        @Query("longitude") longitude: String = "0",
    ): Response<PointsRes>

    @GET(UrlUser.USER)
    suspend fun getUser(
        @Query("Telephone") phone: String
    ): Response<UserRes>

    @POST(UrlUser.PUSH)
    suspend fun createModifyPush(
        @Body pushSettings: PushUpdateReq
    ): Response<PushUpdateRes>
}
