package ru.elegion.atimo.data.models.company

data class CompanyReq(
    val company: CompanyInfoReq
)
