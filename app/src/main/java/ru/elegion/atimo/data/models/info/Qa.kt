package ru.elegion.atimo.data.models.info

data class Qa(
    val qa_id:Int,
    val question:String,
    val answer:String,
    val created:String
)
