package ru.elegion.atimo.data.models.vehicles

import com.google.gson.annotations.SerializedName

data class VehicleUpdateReq(
    @SerializedName("vehicle") val vehicle: VehicleServer
)
