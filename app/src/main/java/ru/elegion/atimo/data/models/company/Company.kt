package ru.elegion.atimo.data.models.company

import com.google.gson.annotations.SerializedName

data class ompany(
    val phone:Int,
    @SerializedName("company_type") val companyType:Int,
    @SerializedName("company_name")val companyName:String,
    @SerializedName("company_inn")val companyInn:String,
    val email:String,
    @SerializedName("company_account")val companyAccount:String,
    @SerializedName("first_name")val firstName:String,
    @SerializedName("second_name")val secondName:String,
    @SerializedName("last_name")val lastName:String
)
