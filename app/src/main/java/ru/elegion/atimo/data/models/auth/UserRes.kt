package ru.elegion.atimo.data.models.auth

import com.google.gson.annotations.SerializedName
import ru.elegion.atimo.data.models.setting.PushProfileSettingRes

data class UserRes(
    @SerializedName("Success") val success: Boolean,
    @SerializedName("User") val user: User,
    @SerializedName("Push_List") val pushList: List<PushProfileSettingRes>
)
