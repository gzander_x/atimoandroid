package ru.elegion.atimo.data.models.rates

data class Rate(
    val rate_id:Int,
    val rate_price:String,
    val rate_description:String
)
