package ru.elegion.atimo.data.network.base

import android.content.SharedPreferences
import okhttp3.Interceptor
import okhttp3.Response
import ru.elegion.atimo.util.Constants.ENCRYPTED_SHARED_PREFERENCES
import ru.elegion.atimo.util.Constants.HEADER_AUTH_KEY
import ru.elegion.atimo.util.Constants.KEY_ACCESS_TOKEN
import java.io.IOException
import javax.inject.Inject
import javax.inject.Named

const val HEADER_AUTH_TOKEN_TYPE = "Bearer"

/**
 * Добавляет для каждого запроса заголовок с accessToken
 *
 * @param sharedPrefs хранилище зашифрованных данных
 * @param noAuthUrlStorage хранилище url-ов, не требующих заголовка авторизации для запроса
 */
class BearerAuthInterceptor @Inject constructor(
    @Named(ENCRYPTED_SHARED_PREFERENCES) private val sharedPrefs: SharedPreferences,
    private val noAuthUrlStorage: NoAuthUrlStorage
) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val url = originalRequest.url

        val headersBuilder = originalRequest.headers.newBuilder()

        if (noAuthUrlStorage.requiresAuthHeader("$url")) {
            sharedPrefs.getString(KEY_ACCESS_TOKEN, null)?.also { accessToken ->
                headersBuilder.add(HEADER_AUTH_KEY, "$HEADER_AUTH_TOKEN_TYPE $accessToken")
            }
        }

        val request = originalRequest.newBuilder()
            .headers(headersBuilder.build())
            .build()
        return chain.proceed(request)
    }
}
