package ru.elegion.atimo.data.models.points

data class PointsReq(
    val point: Point
)
