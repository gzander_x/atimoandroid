package ru.elegion.atimo.data.network.base

data class BaseParamsRequest(
    val phone: String,
    var passed: Boolean? = null,
    var dateBegin: String = "",
    var dateEnd: String = "",
    var page: Int? = null,
    var perPage: Int? = null
)