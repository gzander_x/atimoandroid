package ru.elegion.atimo.data.models.operations

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Operation(
    val numDoc: String,
    val dateDoc: String,
    val countViews: Int,
    val sum: Double
): Parcelable
