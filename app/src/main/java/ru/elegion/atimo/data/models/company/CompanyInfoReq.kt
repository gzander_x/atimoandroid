package ru.elegion.atimo.data.models.company

import com.google.gson.annotations.SerializedName

data class CompanyInfoReq(
    @SerializedName("company_inn") var companyInn: String = "",
    var phone: String = "" ,
    var email: String = "",
    @SerializedName("first_name") var firstName: String = "",
    @SerializedName("second_name") var secondName: String = "",
    @SerializedName("last_name") var lastName: String = "",
    @SerializedName("push_notification") var push: Boolean = true,

    )