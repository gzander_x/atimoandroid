package ru.elegion.atimo.data.models.vehicles

import com.google.gson.annotations.SerializedName

data class VehicleServer(
    @SerializedName("id") val id: String? = null,
    @SerializedName("company_id") val companyId: String? = null,
    @SerializedName("vehicle_type") val vehicleType: String? = null,
    @SerializedName("model") val model: String? = null,
    @SerializedName("mark") val mark: String? = "",
    @SerializedName("vehicle_number") val vehicleGrz: String? = null,
    @SerializedName("vehicle_year") val vehicleYear: String? = null,
    @SerializedName("vin") val vehicleVin: String? = null,
    @SerializedName("blocked") val vehicleStatus:Boolean? = null,
    @SerializedName("vehicle_license") val vehicleLicense: String? = null,
    @SerializedName("vehicle_license_date") val vehicleLicenseDate: String? = null
) {

    companion object {
        fun fromModel(model: Vehicle) = VehicleServer(
            id = model.id,
            companyId = model.companyId,
            vehicleType = model.vehicleType,
            model = model.model,
            mark = model.mark,
            vehicleGrz = model.vehicleGrz,
            vehicleYear = model.vehicleYear,
            vehicleVin = model.vehicleVin,
            vehicleStatus = model.vehicleStatus,
            vehicleLicense = model.vehicleLicense,
            vehicleLicenseDate = model.vehicleLicenseDate
        )
    }

    fun transform() = Vehicle(
        id = id ?: "",
        companyId = companyId ?: "",
        vehicleType = vehicleType ?: "",
        model = model ?: "",
        mark = mark ?: "",
        vehicleGrz = vehicleGrz ?: "",
        vehicleYear = vehicleYear ?: "",
        vehicleVin = vehicleVin ?: "",
        vehicleStatus = vehicleStatus ?: true,
        vehicleLicense = vehicleLicense ?: "",
        vehicleLicenseDate =vehicleLicenseDate ?: ""
    )
}

enum class VehicleType(
    val index: Int,
    val value: String
) {
    PASSENGER(0, "Легковой"),
    CARGO(1, "Грузовой");

    companion object {
        fun byValue(value: String?) = values().firstOrNull { it.value == value } ?: CARGO
    }
}
