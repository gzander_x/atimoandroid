package ru.elegion.atimo.data.models.adapter

import android.text.InputType
import ru.elegion.atimo.data.models.company.CompanyInfo

data class ProfileModel(
    val typeObject: CompanyInfo,
    val hint: String,
    var text: String,
    val inputType: Int = InputType.TYPE_CLASS_TEXT
)
