package ru.elegion.atimo.data.models.points

import com.google.gson.annotations.SerializedName

data class PointsRes(
    val success: Boolean,
    @SerializedName("TerminalsList") val points: List<Point>
)
