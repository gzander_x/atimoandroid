package ru.elegion.atimo.data.models.waybills

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import ru.elegion.atimo.R
import ru.elegion.atimo.data.models.BaseAdapterListModel

@Parcelize
data class Waybill(
    @SerializedName("date") val date: String? = null,
    @SerializedName("number") val number: String? = null,
    @SerializedName("date_begin") val dateBegin: String? = null,
    @SerializedName("date_end") val dateEnd: String? = null,
    @SerializedName("fleet_company_name") val fleetCompanyName: String? = null,
    @SerializedName("status") val status: String? = null,
    @SerializedName("number_KA") val numberKA: String? = null
) : Parcelable {
    companion object{
        private const val BAD_IMAGE = "Аннулирован, Закрыт"
    }

    fun toAdapterModel() = BaseAdapterListModel(
        tittle = "№$number",
        subTittle = status ?: "",
        image = if(BAD_IMAGE.contains(status?:"")){
            R.drawable.ic_icon_close
        }else{
            R.drawable.ic_waybill_icon
        },
        data = this
    )

}
