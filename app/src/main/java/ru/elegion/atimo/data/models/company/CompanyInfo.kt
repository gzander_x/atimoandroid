package ru.elegion.atimo.data.models.company

sealed class CompanyInfo{
    object CompanyName: CompanyInfo()
    object CompanyInn: CompanyInfo()
    object CompanyPhone: CompanyInfo()
    object CompanyEmail: CompanyInfo()
    object CompanyLastName: CompanyInfo()
    object CompanyFirstName: CompanyInfo()
    object CompanySecondName: CompanyInfo()
    object CompanyBik: CompanyInfo()
    object CompanyAccount: CompanyInfo()
}
