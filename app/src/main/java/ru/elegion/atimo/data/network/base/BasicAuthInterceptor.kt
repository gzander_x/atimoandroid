package ru.elegion.atimo.data.network.base

import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.Response
import ru.elegion.atimo.util.Constants.HEADER_AUTH_KEY
import java.io.IOException

/**
 * Добавляет для каждого запроса заголовок с basic auth
 *
 * @param username имя пользователя
 * @param password пароль
 * @param noAuthUrlStorage хранилище url-ов, не требующих заголовка авторизации для запроса
 */
class BasicAuthInterceptor(
    username: String,
    password: String,
    private val noAuthUrlStorage: NoAuthUrlStorage
): Interceptor {

    private val credentials = Credentials.basic(username, password)

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val url = originalRequest.url

        val headerBuilder = originalRequest.headers.newBuilder()

        if (noAuthUrlStorage.requiresAuthHeader("$url")) {
            headerBuilder.add(HEADER_AUTH_KEY, credentials)
        }

        val request = originalRequest.newBuilder()
            .headers(headerBuilder.build())
            .build()
        return chain.proceed(request)
    }
}