package ru.elegion.atimo.data.models.company

import com.google.gson.annotations.SerializedName

data class CompanyCreateModifyRes(
    @SerializedName("Success") val success: Boolean
)