package ru.elegion.atimo.data.network.base

import javax.inject.Inject

/**
 * Хранилище информации о запросах, не требующих добавления заголовка авторизации
 */
class NoAuthUrlStorage @Inject constructor() {

    private val noAuthUrls = listOf(
        UrlUser.USER,
        UrlSmsRu.CALL,
        UrlSmsRu.SMS
    )

    /**
     * Проверяет необходимость добавления заголовка аутентефикации к запросу
     */
    fun requiresAuthHeader(url: String) = noAuthUrls.none { url.contains(it) }
}
