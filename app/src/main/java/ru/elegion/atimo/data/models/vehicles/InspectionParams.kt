package ru.elegion.atimo.data.models.vehicles

data class InspectionParams(
    val grz: String? = null,
    val telephone: String,
    val passed: Boolean? = null,
    val dateBegin: String = "",
    val dateEnd: String = "",
    val page: Int? = null,
    val perPage: Int? = null
)
