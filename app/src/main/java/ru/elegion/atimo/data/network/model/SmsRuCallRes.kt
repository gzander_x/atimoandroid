package ru.elegion.atimo.data.network.model

import com.google.gson.annotations.SerializedName

data class SmsRuCallRes(
    val status: String?,
    val code: String?,
    @SerializedName("status_text")val statusText: String? = null
)
