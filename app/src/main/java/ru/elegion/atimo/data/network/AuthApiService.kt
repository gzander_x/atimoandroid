package ru.elegion.atimo.data.network

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import ru.elegion.atimo.data.network.base.SMS_RU_KEY
import ru.elegion.atimo.data.network.base.UrlSmsRu
import ru.elegion.atimo.data.network.model.SmsRuCallRes
import ru.elegion.atimo.data.network.model.SmsRuSmsRes

interface AuthApiService {

    @GET(UrlSmsRu.CALL)
    suspend fun sendCall(
        @Query("api_id") apiId: String = SMS_RU_KEY,
        @Query("phone") phoneNumber: String
    ): Response<SmsRuCallRes>

    @GET(UrlSmsRu.SMS)
    suspend fun sendSms(
        @Query("api_id") apiId: String = SMS_RU_KEY,
        @Query("to") to: String,
        @Query("msg") message: String
    ): Response<SmsRuSmsRes>
}
