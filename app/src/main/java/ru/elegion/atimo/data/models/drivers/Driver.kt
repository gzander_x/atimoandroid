package ru.elegion.atimo.data.models.drivers

import com.google.gson.annotations.SerializedName
import ru.elegion.atimo.util.extensions.changedDateToDisplay
import ru.elegion.atimo.util.extensions.changedDateToServer

data class Driver(
    @SerializedName("id") var id: Int? = null,
    @SerializedName("company_id") var companyId: String,
    @SerializedName("first_name") var firstName: String,
    @SerializedName("second_name") var secondName: String,
    @SerializedName("last_name") var lastName: String,
    @SerializedName("phone") var phone: String,
    @SerializedName("sex") var sex: Int,
    @SerializedName("driver_license") var driverLicense: String,
    @SerializedName("birthdate") var birthdate: String,
    @SerializedName("blocked") var driverStatus: Int,
) {
    companion object {
        const val GENDER_MALE = "Мужской"
        const val GENDER_FEMALE = "Женский"
        fun parserGender(gender: String): Int {
            return if (gender == GENDER_MALE) {
                1
            } else {
                0
            }
        }

        fun parserGender(gender: Int): String {
            return if (gender == 1) GENDER_MALE else GENDER_FEMALE
        }
    }

    constructor(driver: Driver) : this(
        driver.id,
        driver.companyId,
        driver.firstName,
        driver.secondName,
        driver.lastName,
        driver.phone,
        driver.sex,
        driver.driverLicense,
        driver.birthdate,
        driver.driverStatus
    )

    fun toCreateModify(): DriverCreateModify = DriverCreateModify(
        companyId = this.companyId.toInt(),
        driverStatus = this.driverStatus != 1, //Вот так криво работает бек....
        firstName = this.firstName,
        secondName = this.secondName,
        lastName = this.lastName,
        phone = this.phone,
        sex = this.sex,
        driverLicense = this.driverLicense,
        birthdate = this.birthdate
    )

    fun rollback(driver: Driver){
        this.id = driver.id
        this.companyId = driver.companyId
        this.firstName = driver.firstName
        this.secondName = driver.secondName
        this.lastName = driver.lastName
        this.phone = driver.phone
        this.sex = driver.sex
        this.driverLicense = driver.driverLicense
        this.birthdate = driver.birthdate
        this.driverStatus = driver.driverStatus
    }

    fun getFullName(): String{
        return "$lastName $firstName $secondName"
    }

    fun getSex(): String {
        return parserGender(sex)
    }

    fun setSex(gender: String) {
        sex = parserGender(gender)
    }

    fun getDate(): String {
        return birthdate.changedDateToDisplay()
    }

    fun setDate(date: String) {
        birthdate = date.changedDateToServer()
    }
    fun getPhoneForModify(): String{
        return if(phone.length == 12){
            formatPhone(phone).replace("+7", "").trim()
        }else{
            phone
        }
    }
    private fun formatPhone(phone: String): String{
        return "${phone.substring(0, 2)} ${phone.substring(2, 5)} ${phone.substring(5, 8)}-${phone.substring(8, 10)}-${phone.substring(10, 12)}"
    }
    fun getPhoneForList():String{
        return if(phone.length == 12){
            formatPhone(phone)
        }else{
            phone
        }
    }
}