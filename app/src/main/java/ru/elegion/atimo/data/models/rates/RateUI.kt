package ru.elegion.atimo.data.models.rates

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class RateUI(
    val count: Int = 0,
    val isPromo: Boolean = false,
    val code: String? = null
): Parcelable