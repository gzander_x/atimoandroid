package ru.elegion.atimo.data.models.vehicles

data class Vehicle(
    var id: String? = null,
    var companyId: String,
    var vehicleType: String,
    var model: String = "",
    var mark: String = "",
    var vehicleGrz: String,
    var vehicleYear: String = "",
    var vehicleVin: String,
    var vehicleStatus: Boolean,
    var vehicleLicense: String,
    var vehicleLicenseDate: String
) {
    companion object {
        const val VEHICLE_CARGO = "Грузовой"
        const val VEHICLE_PASSENGER = "Легковой"
    }

    constructor(car: Vehicle) : this(
        car.id,
        car.companyId,
        car.vehicleType,
        car.model,
        car.mark,
        car.vehicleGrz,
        car.vehicleYear,
        car.vehicleVin,
        car.vehicleStatus,
        car.vehicleLicense,
        car.vehicleLicenseDate
    )

    fun rollback(car: Vehicle) {
        id = car.id
        companyId = car.companyId
        vehicleType = car.vehicleType
        model = car.model
        mark = car.mark
        vehicleGrz = car.vehicleGrz
        vehicleYear = car.vehicleYear
        vehicleVin = car.vehicleVin
        vehicleStatus = car.vehicleStatus
        vehicleLicense = car.vehicleLicense
        vehicleLicenseDate = car.vehicleLicenseDate
    }
}
