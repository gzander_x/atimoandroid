package ru.elegion.atimo.data.models.points

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Point(
    val name: String,
    val latitude: String,
    val longitude: String,
    val work_time_begin: String,
    val work_time_end: String,
    val mo: Boolean,
    val to: Boolean,
    val status: Int
) : Parcelable
