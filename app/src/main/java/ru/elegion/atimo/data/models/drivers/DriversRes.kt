package ru.elegion.atimo.data.models.drivers

import com.google.gson.annotations.SerializedName

data class DriversRes(
    val success:Boolean,
    @SerializedName("DriverList") val drivers: List<Driver>
)
