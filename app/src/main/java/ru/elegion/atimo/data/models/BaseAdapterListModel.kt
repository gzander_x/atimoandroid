package ru.elegion.atimo.data.models

import androidx.annotation.DrawableRes

data class BaseAdapterListModel(
    @DrawableRes
    val image: Int,
    val tittle: String,
    val subTittle: String,
    val data: Any? = null
)