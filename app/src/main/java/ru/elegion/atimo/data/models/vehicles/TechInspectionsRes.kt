package ru.elegion.atimo.data.models.vehicles

import com.google.gson.annotations.SerializedName

class TechInspectionsRes(
    @SerializedName("Success") val success: Boolean? = null,
    @SerializedName("TechnicalCheckupList") val technicalCheckupList: List<TechInspectionRes>? = null
)
