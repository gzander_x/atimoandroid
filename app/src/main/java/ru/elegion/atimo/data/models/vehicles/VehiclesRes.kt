package ru.elegion.atimo.data.models.vehicles

import com.google.gson.annotations.SerializedName

data class VehiclesRes(
    @SerializedName("Success") val success: Boolean,
    @SerializedName("VehicleList") val vehicles: List<VehicleServer>
)
