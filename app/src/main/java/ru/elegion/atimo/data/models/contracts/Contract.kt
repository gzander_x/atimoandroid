package ru.elegion.atimo.data.models.contracts

data class Contract(
    val numDoc: String,
    val dateDoc: String,
    val formatFile: String,
    val size: String
)