package ru.elegion.atimo.data.models.drivers

import com.google.gson.annotations.SerializedName

data class DriverMoRes(
    @SerializedName("Success") val success: Boolean? = null,
    @SerializedName("MedicalCheckupList") val medicalCheckupList: List<DriverMo>? = null
)
