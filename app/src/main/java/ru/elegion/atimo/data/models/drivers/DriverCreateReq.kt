package ru.elegion.atimo.data.models.drivers

data class DriverCreateReq(
    val driver: DriverCreateModify
)
