package ru.elegion.atimo.data.models.notifications

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Notification(
    val id: Int? = null,
    val tittle: String? = "",
    val message: String? = "",
    val date: String? = "",
    val status: Int? = null
): Parcelable
