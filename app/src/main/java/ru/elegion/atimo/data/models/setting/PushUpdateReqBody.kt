package ru.elegion.atimo.data.models.setting

import com.google.gson.annotations.SerializedName

// TODO проверить, можно ли не передавать некоторые из полей, и если так, сделать их nullable
data class PushUpdateReqBody(
    @SerializedName("WaybillEnd") val waybillEnd: Boolean,
    @SerializedName("DriverBlocked") val driverBlocked: Boolean,
    @SerializedName("BalanceUpdate") val balanceUpdate: Boolean
)
