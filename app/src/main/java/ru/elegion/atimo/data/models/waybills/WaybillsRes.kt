package ru.elegion.atimo.data.models.waybills

import com.google.gson.annotations.SerializedName

data class WaybillsRes(
    @SerializedName("Success") val success: Boolean? = null,
    @SerializedName("WayBillList") val waybillsList: List<Waybill>? = null,
)