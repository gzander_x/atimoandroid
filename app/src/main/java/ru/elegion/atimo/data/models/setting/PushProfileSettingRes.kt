package ru.elegion.atimo.data.models.setting

import com.google.gson.annotations.SerializedName

data class PushProfileSettingRes(
    @SerializedName("Пуш") val name: String,
    @SerializedName("Активно") val status: Boolean
)
