package ru.elegion.atimo

import android.app.Application
import com.yandex.mapkit.MapKitFactory
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber

@HiltAndroidApp
class Application: Application() {

    override fun onCreate() {
        super.onCreate()
        MapKitFactory.setApiKey("3851dcdb-0c8b-499d-be0f-cd7010eb8eb8")
        Timber.plant(Timber.DebugTree())
    }
}